DIROWNER := $(shell ls -ld . | awk '{print $$3}')
CURRENTUSER := $(shell whoami)
GIT_UPDATE := $(shell if [ -f "/bin/nest/git_update.sh" ]; then echo "/bin/nest/git_update.sh"; else echo "$(HOME)/bin/nest/git_update.sh"; fi;)
SHELL := /bin/bash

install-all: vue-install composer-install

vue-dev:
	@npm --prefix client/ run dev;
vue-build:
	@if [ '$(DIROWNER)' = '$(CURRENTUSER)' ]; then\
		npm --prefix client/ run build;\
	else\
		sudo -u '$(DIROWNER)' npm --prefix client/ run build;\
	fi;
vue-install:
	@if [ '$(DIROWNER)' = '$(CURRENTUSER)' ]; then\
		npm --prefix client/ i;\
	else\
		sudo -u '$(DIROWNER)' npm --prefix client/ i;\
	fi;
composer-install:
	@if [ '$(DIROWNER)' = '$(CURRENTUSER)' ]; then\
		composer install;\
	else\
		sudo -u '$(DIROWNER)' composer install;\
	fi;
test-phpstan:
	@vendor/bin/phpstan analyze -v --configuration=./phpstan-config.neon;

# The tee redirection requires /bin/bash
git-pull-update:
	@exec 5>&1;\
	out=`$(GIT_UPDATE) . | tee >(cat - >&5)`;\
	if echo "$$out" | grep -q "Make sure to process composer install."; then\
		make composer-install;\
	fi;\
	if echo "$$out" | grep -q "Make sure to process vue install."; then\
		make vue-install;\
	fi;\
	if echo "$$out" | grep -q "Make sure to process vue build."; then\
		make vue-build;\
	fi;
