<?php

spl_autoload_register(function ($class) {
  if (strpos($class, 'LogViewer\\') !== 0) {
    return;
  }

  $fileName = strtr($class, ['LogViewer\\' => '', '\\' => DIRECTORY_SEPARATOR]);
  $baseDirectory = __DIR__.DIRECTORY_SEPARATOR;

  $file = $baseDirectory.$fileName.'.php';
  if (file_exists($file)) {
    require_once($file);
  }
});
