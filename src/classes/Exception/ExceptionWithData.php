<?php
namespace LogViewer\Exception;

class ExceptionWithData extends \Exception {
  /** @var array{log_message?: string, form_error?: array<string, mixed>} $data */
  private array $data = [];

  /**
   * @param array{log_message?: string, form_error?: array<string, mixed>} $data
   */
  public function __construct(string $message = "", int $code = 0, ?\Throwable $previous = null, array $data = []) {
    parent::__construct($message, $code, $previous);
    $this->data = $data;
  }

  /**
   * @return array{log_message?: string, form_error?: array<string, mixed>}
   */
  public function getData(): array {
    return $this->data;
  }

  public function getLogMessage(): string {
    return !empty($this->data['log_message']) ? $this->data['log_message'] : '';
  }

  /**
   * @return array<string, mixed>
   */
  public function getFormError(): array {
    return !empty($this->data['form_error']) ? $this->data['form_error'] : [];
  }
}
