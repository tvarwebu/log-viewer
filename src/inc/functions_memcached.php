<?php

function getMemcachedObject(): ?\Memcached {
  global $memcachedConfig;
  /** @var ?\Memcached $memcached */
  static $memcached = null;

  if (!empty($memcached)) {
    return $memcached;
  }

  if (!empty($memcachedConfig)) {
    if (!class_exists('Memcached', false)) {
      trigger_error('Missing PHP Memcached class', E_USER_WARNING);
      return null;
    }
    $memcached = new \Memcached();
    foreach ($memcachedConfig as $server) {
      $memcached->addServer($server['host'], isset($server['port']) ? $server['port'] : 11211);
    }
  }
  return $memcached;
}

function memcachedGet(string $key): mixed {
  $memcached = getMemcachedObject();
  if (empty($memcached)) {
    return false;
  }

  $value = $memcached->get($key);
  if ($value === false && !in_array($memcached->getResultCode(), [Memcached::RES_SUCCESS, Memcached::RES_NOTFOUND])) {
    trigger_error('Could not get value from memcached, result code: '.$memcached->getResultCode(), E_USER_WARNING);
  }
  return $value;
}

function memcachedSet(string $key, mixed $value, int $expiration = 0): bool {
  if ($expiration > 2592000) {
    $expiration = 2592000;
  }

  $memcached = getMemcachedObject();
  if (empty($memcached)) {
    return false;
  }

  $result = $memcached->set($key, $value, $expiration);
  if ($result === false && !in_array($memcached->getResultCode(), [Memcached::RES_SUCCESS, Memcached::RES_NOTFOUND])) {
    trigger_error('Could not set value to memcached, result code: '.$memcached->getResultCode(), E_USER_WARNING);
  }
  return $result;
}

function memcachedDelete(string $key): bool {
  $memcached = getMemcachedObject();
  if (empty($memcached)) {
    return false;
  }

  $result = $memcached->delete($key);
  if ($result === false && !in_array($memcached->getResultCode(), [Memcached::RES_SUCCESS, Memcached::RES_NOTFOUND])) {
    trigger_error('Could not delete value from memcached, result code: '.$memcached->getResultCode(), E_USER_WARNING);
  }
  return $result;
}

/**
 * @param string $prefix - prefix for the keys used in limits
 * @param array<string, int|array{count: int, filter: string, ttl?: int}> $limits
 */
function memcachedRateLimitExceeded(string $prefix, array $limits, int $ttlDefault): bool {
  foreach ($limits as $key => $limit) {
    $limitCount = is_array($limit) ? $limit['count'] : $limit;
    $ttl = is_array($limit) && !empty($limit['ttl']) ? $limit['ttl'] : $ttlDefault;

    $rateLimit = memcachedGet($prefix.'_'.$key);
    if (!is_array($rateLimit)) {
      continue;
    }

    foreach ($rateLimit as $index => $value) {
      if (!empty($value['time']) && $value['time'] + $ttl < time()) {
        unset($rateLimit[$index]);
      } elseif (is_array($limit) && !empty($limit['filter']) && (empty($value['filter']) || $limit['filter'] !== $value['filter'])) {
        unset($rateLimit[$index]);
      }
    }
    $rateLimit = array_values($rateLimit);
    memcachedSet($prefix.'_'.$key, $rateLimit, $ttl * 2);

    if (count($rateLimit) >= $limitCount) {
      return true;
    }
  }
  return false;
}

/**
 * @param string $prefix - prefix for the keys used in limits
 * @param array<string, int|array{count: int, filter: string, ttl?: int}> $limits
 */
function memcachedRateLimitIncrement(string $prefix, array $limits, int $ttlDefault): bool {
  foreach ($limits as $key => $limit) {
    $ttl = is_array($limit) && !empty($limit['ttl']) ? $limit['ttl'] : $ttlDefault;

    $rateLimit = memcachedGet($prefix.'_'.$key);
    if (!is_array($rateLimit)) {
      $rateLimit = [];
    }

    $rateLimit[] = ['time' => time(), 'filter' => is_array($limit) && !empty($limit['filter']) ? $limit['filter'] : null];
    memcachedSet($prefix.'_'.$key, $rateLimit, $ttl * 2);
  }
  return false;
}
