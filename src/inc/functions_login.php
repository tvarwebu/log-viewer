<?php

/**
 * @return array<string, string> - array of errors
 */
function userLogin(string $username, string $password): array {
  if (empty($username)) {
    return ['username' => 'Missing username.'];
  }

  if (empty($password)) {
    return ['password' => 'Missing password.'];
  }

  $database = getDatabase();
  $user = $database->users->findOne(['username' => $username]);
  if (empty($user)) {
    return ['username' => 'Unknown username supplied.'];
  }
  if (empty($user['active'])) {
    return ['global' => 'Your account was deactivated.'];
  }

  if (!passwordVerifyAndRehash($password, $user)) {
    return ['password' => 'Invalid password supplied.'];
  }

  sessionUserSet($user);
  return [];
}

/**
 * @return array{global?: string}
 */
function sessionLogin(string $loginHash): array {
  $database = getDatabase();
  $user = $database->users->findOne(['login_hash' => $loginHash]);
  if (empty($user)) {
    return ['global' => 'You have been logged out.'];
  }
  if (empty($user['active'])) {
    return ['global' => 'Your account was deactivated.'];
  }

  sessionUserSet($user);
  return [];
}

/**
 * @throws Exception if there is any problem with the token or user
 */
function tokenLogin(string $token): void {
  $key = getKeyForToken($token);
  if (empty($key)) {
    throw new Exception('Invalid token supplied.', 403);
  }
  if (empty($key['user_id'])) {
    trigger_error('Supplied token does not have user specified: '.var_export($key, true), E_USER_WARNING);
    throw new Exception('Token does not have user specified.', 400);
  }

  $database = getDatabase();
  $user = $database->users->findOne(['_id' => new MongoDB\BSON\ObjectID($key['user_id'])]);
  if (empty($user)) {
    throw new Exception('User does not exist.', 403);
  }
  if (empty($user['active'])) {
    throw new Exception('Your account was deactivated.', 403);
  }

  sessionUserSet($user);
}

/**
 * @throws Exception if there is any problem with the token or user
 */
function tokenLoginOrException(): void {
  $token = getBearerToken();
  if (empty($token)) {
    throw new Exception('Missing or invalid token in Authorization Header.', 400);
  }
  tokenLogin($token);
}

function sessionUserSet(MongoDB\Model\BSONDocument $user): void {
  $userArray = bsonDocumentToArray($user);
  unset($userArray['password'], $userArray['login_hash']);
  $_SESSION['LVuser'] = $userArray;
}

function passwordVerifyAndRehash(string $password, MongoDB\Model\BSONDocument $user): bool {
  $database = getDatabase();

  if (!is_string($user['password'])) {
    return false;
  }
  if (!password_verify($password, $user['password'])) {
    return false;
  }
  if (password_needs_rehash($user['password'], PASSWORD_DEFAULT)) {
    $database->users->updateOne(['_id' => $user['_id']], ['$set' => ['password' => password_hash($password, PASSWORD_DEFAULT)]]);
  }
  return true;
}

function loginHashUpdate(bool $keepMeLoggedIn): void {
  global $keepLoggedInTime, $urlPrefix;

  $sessionId = (string)session_id();

  $database = getDatabase();
  $database->users->updateOne(['_id' => new MongoDB\BSON\ObjectID($_SESSION['LVuser']['_id'])], ['$set' => ['login_hash' => $sessionId]]);

  if ($keepMeLoggedIn || isset($_COOKIE['loginHash'])) {
    if (!isset($_COOKIE['loginHash']) || $_COOKIE['loginHash'] !== $sessionId) {
      setcookie('loginHash', $sessionId, time() + $keepLoggedInTime, $urlPrefix);
    }
  } else {
    setcookie('loginHash', '', time() - 365 * 24 * 60 * 60, $urlPrefix);
  }
}

function sessionDestroyCookieUnset(): void {
  global $urlPrefix;
  setcookie('loginHash', '', time() - 365 * 24 * 60 * 60, $urlPrefix);
  session_destroy();
}

/**
 * @return array<string, int|array{count: int, filter: string}>
 */
function loginRateLimits(string $username): array {
  global $trustedIpAddresses;
  $limits = array(
    'global' => ['count' => 75, 'filter' => 'ip_'.$_SERVER['REMOTE_ADDR']],
    'ip_'.$_SERVER['REMOTE_ADDR'] => 15
  );

  if (!empty($username)) {
    // memcache key size limit is 250 bytes, the prefix has 22 bytes so we keep the extra 3 as a spare
    $limits['username_'.substr((string)preg_replace('/[^a-zA-Z0-9@.-]/', '', $username), 0, 225)] = 20;
  }

  if (!empty($trustedIpAddresses) && in_array($_SERVER['REMOTE_ADDR'], $trustedIpAddresses)) {
    unset($limits['global']);
  }
  return $limits;
}
