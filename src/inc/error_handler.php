<?php

$currentRequestErrorCount = 0;

function logViewerErrorHandler(int $errorNumber, string $errorString, string $errorFile, int $errorLine): bool {
  global $currentRequestHash, $currentRequestErrorCount, $errorHandleMaxLineLength, $logToFile;
  static $postDataShown = false;

  if (empty($currentRequestHash)) {
    $currentRequestHash = md5(microtime());
  }

  if (!error_reporting() || $errorNumber === 2048) {
    return false;
  }

  $data = array(
    'timestamp' => microtime(true) * 1000,
    'date' => (new DateTime())->format('Y-m-d H:i:s.v O').' GMT',
  );

  $dataToExport = array(
    'REQUEST_URI' => 'url',
    'REQUEST_METHOD' => 'method',
    'HTTP_REFERER' => 'referer',
    'HTTP_X_REQUESTED_WITH' => 'X-Requested-With',
    'REMOTE_ADDR' => 'IP',
    'HTTP_USER_AGENT' => 'Agent',
    'HTTP_AUTHORIZATION' => 'Authorization'
  );
  if (empty($_SERVER['REQUEST_URI'])) {
    $dataToExport['PHP_SELF'] = 'php-self';
    $dataToExport['argv'] = 'argv';
    $dataToExport['argc'] = 'argc';
  }
  foreach ($dataToExport as $key => $label) {
    if (isset($_SERVER[$key])) {
      $data[$label] = $_SERVER[$key];
    }
  }

  $data['pid'] = getmypid();
  $data['request_hash'] = $currentRequestHash;
  $data['error_count'] = (++$currentRequestErrorCount);
  if (session_id() !== false) {
    $data['session'] = substr(session_id(), 0, 6).'***';
  }
  $data['errno'] = $errorNumber;
  $data['line'] = $errorLine;
  $data['filename'] = $errorFile;
  $data['trace'] = explode("\n", (new \Exception)->getTraceAsString());
  $data['message'] = preg_replace("/(\r)?\n\r?\n/", "$1\n", $errorString);

  if (!empty($_SERVER['REQUEST_METHOD']) && in_array($_SERVER['REQUEST_METHOD'], ['POST', 'PUT'], true) && !$postDataShown) {
    $data['post'] = $_POST;
    $postDataShown = true;
  }

  $data = dataPrivateAnonymize($data);

  if (!$logToFile) {
    try {
      $database = getDatabase();
      $database->command(['ping' => 1]); // Send a ping to confirm a successful connection

      $result = $database->errors_log_viewer->insertOne($data);
      if ($result->getInsertedCount() === 0) {
        throw new Exception('Log was not inserted into DB.');
      }
      return false;
    } catch (Exception $exception) {
      // we could not save the logs to DB save to file instead
    }
  }

  $stringToLog = '';
  $line = '';
  foreach ($data as $key => $value) {
    $line .= (!empty($line) ? ' | ' : '').$key .": ".print_r($value, true);
    if (strlen($line) >= $errorHandleMaxLineLength) {
      $stringToLog .= $line.END_LINE;
      $line = '';
    }
  }
  if (!empty($line)) {
    $stringToLog .= $line.END_LINE;
    $line = '';
  }
  file_put_contents(dirname(__DIR__).'/logs/errors_log_viewer.log', $stringToLog.END_LINE.END_LINE, FILE_APPEND);
  return false;
}

function logViewerExceptionHandler(Throwable $exception = null): void {
  if (!is_null($exception)) {
    trigger_error('Uncaught Exception - '.$exception->getMessage().' in '.$exception->getFile().':'.$exception->getLine().', trace: '."\n".$exception->getTraceAsString(), E_USER_WARNING);
    http_response_code(500);
  }
}

set_error_handler('logViewerErrorHandler');
set_exception_handler('logViewerExceptionHandler');
