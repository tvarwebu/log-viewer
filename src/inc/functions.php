<?php

require_once(__DIR__.'/error_handler.php');
require_once(__DIR__.'/functions_html.php');
require_once(__DIR__.'/functions_login.php');
require_once(__DIR__.'/functions_permissions.php');
require_once(__DIR__.'/functions_users.php');
require_once(__DIR__.'/functions_memcached.php');
require_once(dirname(__DIR__, 2).'/vendor/autoload.php');
require_once(dirname(__DIR__).'/classes/autoload.php');

function getCorrectUrl(string $url): string {
  $domain = getDomainWithPrefix();

  if (strpos($url, 'http') !== 0) {
    $url = $domain.'/'.ltrim($url, '/');
  }
  $url = preg_replace('/([^:])\/\//', '\1/', $url);
  return is_string($url) ? $url : '';
}

function getDomainName(): string {
  $domain = '';
  if (!empty($_SERVER['HTTP_X_FORWARDED_HOST'])) {
    $domain = $_SERVER['HTTP_X_FORWARDED_HOST'];
  }
  if (empty($domain) && !empty($_SERVER['HTTP_HOST'])) {
    $domain = $_SERVER['HTTP_HOST'];
  }
  return $domain;
}

function getDomainWithPrefix(): string {
  global $urlPrefix;
  static $prefix = null;
  if (is_null($prefix)) {
    $domain = getDomainName();

    $prefix = isset($urlPrefix) ? '/'.trim($urlPrefix, '/') : '';
    if ($prefix == '/') {
      $prefix = '';
    }
    if (substr($prefix, 0, 2) == '//') {
      $prefix = substr($prefix, 0, 1);
    }

    $protocol = 'http';
    if (isset($_SERVER['HTTP_X_FORWARDED_HOST']) && isset($_SERVER['HTTP_X_FORWARDED_HTTPS']) && $_SERVER['HTTP_X_FORWARDED_HTTPS'] === 'on') {
      $protocol = 'https';
    } elseif (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
      $protocol = 'https';
    }

    if (!empty($domain)) {
      $prefix = $protocol.'://'.$domain.$prefix;
    }
    $prefix = str_replace(DIRECTORY_SEPARATOR, '/', $prefix);
  }
  return $prefix;
}

function sanitizeStringInput(string $input): string {
  return trim(strip_tags($input));
}

/**
 * @param array<string|int, mixed> $array
 * @return array<string|int, mixed>
 */
function sanitizeArray(array $array): array {
  foreach ($array as $key => $value) {
    if (is_array($value)) {
      $array[$key] = sanitizeArray($value);
    } elseif (is_string($value)) {
      $array[$key] = sanitizeStringInput($value);
    } else {
      $array[$key] = $value;
    }
  }

  return $array;
}

function includePageAndDie(string $page, int $httpCode = 0): never {
  $_REQUEST['page'] = $page;
  foreach ($GLOBALS as $key => $value) { // This is needed for the include
    global $$key;
  }

  if (!empty($httpCode)) {
    http_response_code($httpCode);
  }
  include(dirname(__DIR__).'/index.php');
  die();
}

function getDatabase(): MongoDB\Database {
  global $mongoDbUri, $mongoDbParams;
  static $database;

  if (empty($database)) {
    $database = (new MongoDB\Client($mongoDbUri, $mongoDbParams))->logViewer;
  }

  return $database;
}

function getDatabaseOrDie(bool $isApi = false, string $logFile = ''): MongoDB\Database {
  try {
    $database = getDatabase();
    $database->command(['ping' => 1]); // Send a ping to confirm a successful connection
    return $database;
  } catch (Exception $exception) {
    if ($isApi) {
      apiErrorResponse($exception->getMessage(), $logFile, 'mongo DB error: '.$exception->getMessage(), 500);
    } else {
      htmlError($exception->getMessage());
    }
  }
}

/**
 * @param array<string, mixed> $additionalData
 * @param array<string, mixed> $dataRoot
 */
function requestStoreToFile(string $filename, array $additionalData = [], array $dataRoot = []): void {
  $data = requestStoreToFileData($additionalData, $dataRoot);
  file_put_contents($filename, print_r($data, true).'----------'.END_LINE.END_LINE, FILE_APPEND);
}

/**
 * @param array<string, mixed> $additionalData
 * @param array<string, mixed> $dataRoot
 * @return array<string, mixed>
 */
function requestStoreToFileData(array $additionalData = [], array $dataRoot = []): array {
  $data = $dataRoot;
  $data['datetime'] = (new DateTime())->format('Y-m-d H:i:s.v O');
  $data['headers'] = array(
    'Accept' => isset($_SERVER['HTTP_ACCEPT']) ? $_SERVER['HTTP_ACCEPT'] : '',
    'User-Agent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
    'Host' => isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '',
    'Connection' => isset($_SERVER['HTTP_CONNECTION']) ? $_SERVER['HTTP_CONNECTION'] : '',
    'Cache-Control' => isset($_SERVER['HTTP_CACHE_CONTROL']) ? $_SERVER['HTTP_CACHE_CONTROL'] : '',
    'Cookie' => isset($_SERVER['HTTP_COOKIE']) ? preg_replace('#PHPSESSID=([0-9a-zA-Z]{6})([0-9a-zA-Z]+)(;|"|$)#', 'PHPSESSID=\1***\3', $_SERVER['HTTP_COOKIE']) : '',
    'Authorization' => isset($_SERVER['HTTP_AUTHORIZATION']) ? tokenAnonymize($_SERVER['HTTP_AUTHORIZATION']) : '',
    'Method' => !empty($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'N/A',
  );
  $data['IP info'] = [];
  if (!empty($_SERVER['REQUEST_URI'])) {
    $data['IP info']['script'] = $_SERVER['REQUEST_URI'];
  }
  if (!empty($_SERVER['REMOTE_ADDR'])) {
    $data['IP info']['IP'] = $_SERVER['REMOTE_ADDR'];
  }
  if (session_id() !== false) {
    $data['IP info']['Session'] = substr(session_id(), 0, 6).'***';
  }
  $data['Memory Peak[MB]'] = round(memory_get_peak_usage() / 1024 / 1024);
  $data['GET'] = !empty($_GET) ? dataPrivateAnonymize($_GET) : [];
  $data['POST'] = !empty($_POST) ? dataPrivateAnonymize($_POST) : [];

  if (isset($_SERVER['REQUEST_METHOD']) && in_array($_SERVER['REQUEST_METHOD'], ['PUT', 'DELETE'])) {
    $data['REQUEST'] = !empty($_REQUEST) ? dataPrivateAnonymize($_REQUEST) : [];
  }

  $data['FILES'] = !empty($_FILES) ? $_FILES : [];
  $data['Additional info'] = dataPrivateAnonymize($additionalData);

  return $data;
}

/**
 * @param array<string, mixed> $data
 * @return array<string, mixed>
 */
function dataPrivateAnonymize(array $data): array {
  $dataHidden = [];
  foreach ($data as $key => $value) {
    if (is_array($value)) {
      $dataHidden[$key] = dataPrivateAnonymize($value);
      continue;
    }

    if (in_array($key, ['pass', 'passwd', 'password', 'fpassword'])) {
      $value = '***';
    }
    if ($key === 'client_secret') {
      $value = is_string($value) ? substr((string)$value, 0, 4).'***' : '***';
    }
    if ($key === 'data') {
      $value = is_string($value) ? preg_replace('#([?|&]+)(pass|password)=([^&]+)($|[&]+)#', '\1\2=***\4', $value) : $value;
      if (is_null($value)) {
        $value = '';
      }
    }
    if (in_array($key, ['refresh_token', 'access_token', 'authorization'])) {
      $value = is_string($value) ? tokenAnonymize($value) : '***';
    }
    if (in_array($key, ['Cookie'])) {
      $value = is_string($value) ? preg_replace('#PHPSESSID=([0-9a-zA-Z]{6})([0-9a-zA-Z]+)(;|"|$)#', 'PHPSESSID=\1***\3', $value) : '***';
    }
    $dataHidden[$key] = $value;
  }

  return $dataHidden;
}

function tokenAnonymize(string $token): string {
  $tokenLength = strlen($token);
  if ($tokenLength === 0) {
    return '';
  } elseif ($tokenLength < 10) {
    return '***';
  }
  $token = preg_replace('#^(Bearer )?(.{'.(min(round($tokenLength / 3), 10)).'})(.*)$#', '\1\2***', $token);
  return is_string($token) ? $token : '';
}

function messageAdd(string $content, string $type, string $identifier = null): void {
  static $messageCounter = 0;
  if (!isset($_SESSION['_display_messages_'])) {
    $_SESSION['_display_messages_'] = [];
  }
  $messageId = $identifier;
  if (empty($messageId)) {
    while (isset($_SESSION['_display_messages_']['message_php_'.$messageCounter])) {
      $messageCounter++;
    }
    $messageId = 'message_php_'.($messageCounter++);
  }
  $_SESSION['_display_messages_'][$messageId] = ['content' => $content, 'type' => $type];
}

function messagesDisplay(): string {
  if (empty($_SESSION['_display_messages_'])) {
    return '';
  }
  $messages = $_SESSION['_display_messages_'];
  $_SESSION['_display_messages_'] = [];

  $html = '';
  foreach ($messages as $messageId => $message) {
    $messageContent = strip_tags($message['content'], '<div><span><b><strong><i><br><a><img><p><script><br><ul><ol><li><label><sub><sup>');
    $type = !empty($message['type']) && in_array($message['type'], ['info', 'warning', 'error', 'success']) ? : 'info';
    $timeout = !empty($message['timeout']) ? (int)$message['timeout'] : 5000;
    $html .= 'messageShow(\''.htmlspecialchars($messageContent, ENT_QUOTES).'\', \''.$type.'\', \''.$messageId.'\', '.$timeout.');'.END_LINE;
  }

  return !empty($html) ? '<script>'.$html.'</script>' : '';
}

function apiErrorResponse(string $message, string $logFile = '', string $logMessage = '', int $httpCode = 400): never {
  $logMessageAdditional = array(
    'user id: '.(!empty($_SESSION['LVuser']['_id']) ? $_SESSION['LVuser']['_id'] : '-- no user --'),
    'ip: '.(!empty($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '-- no ip --')
  );
  $logMessage .= (!empty($logMessage) ? ', ' : '').implode(', ', $logMessageAdditional);
  apiResponse($message, [], 'error', $logFile, $logMessage, $httpCode);
}

/**
 * @param array<string, mixed> $data
 */
function apiResponse(string $message, array $data = [], string $type = 'success', string $logFile = '', string $logMessage = '', int $httpCode = 200): never {
  global $logToFile;

  if (!empty($logMessage) && !empty($logFile)) {
    if ($logToFile) {
      file_put_contents(dirname(__DIR__).'/logs/'.$logFile.'.log', (new DateTime())->format('Y-m-d H:i:s.v O').' - '.$logMessage.END_LINE, FILE_APPEND);
    } else {
      logViewerLogsSave($logFile, $logMessage, requestStoreToFileData());
    }
  }
  if ($httpCode !== 200) {
    http_response_code($httpCode);
  }
  header('Content-type: application/json');
  die(json_encode(array_merge(['result' => $type, 'message' => $message], $data)));
}

function isDeveloper(): bool {
  return !empty($_SESSION['developer']);
}

function developerSet(): void {
  if (isset($_REQUEST['developer'])) {
    if (!empty($_REQUEST['developer'])) {
      $_SESSION['developer'] = true;
    } else {
      unset($_SESSION['developer']);
    }
  }
}

/**
 * @return ?array{
 *   type: 'file',
 *   handle: string,
 *   class: string,
 *   bsIcon: string,
 *   url: string,
 *   lastModified: int|false,
 *   fileSize: int|false,
 *   count: int|false,
 *   permissionsReal: string|false
 * }
 */
function errorsLogsFilesData(string $errorHandle): ?array {
  $logFilename = dirname(__DIR__).'/logs/'.$errorHandle.'.log';
  if (!file_exists($logFilename)) {
    return null;
  }

  $fileSize = filesize($logFilename);
  $lastModified = filemtime($logFilename);

  $filePerms = fileperms($logFilename);
  if ($filePerms !== false) {
    $permissions = decoct($filePerms);
    $permissionsReal = substr($permissions, strlen($permissions) - 3);
  } else {
    $permissionsReal = 'Could not get file permissions';
  }

  $class = '';
  $bsIcon = '';
  if ($lastModified && $fileSize > 0 && time() - $lastModified < 60 * 60) {
    $class = 'text-danger';
    $bsIcon = 'bi-exclamation-circle-fill';
  } elseif ($lastModified && $fileSize > 0 && time() - $lastModified < 60 * 60 * 24) {
    $class = 'text-warning';
    $bsIcon = 'bi-exclamation-circle-fill';
  }

  return array(
    'type' => 'file',
    'handle' => $errorHandle,
    'class' => $class,
    'bsIcon' => $bsIcon,
    'url' => getCorrectUrl('error-logs/'.$errorHandle.'/file/view'),
    'lastModified' => $lastModified,
    'fileSize' => $fileSize,
    'count' => false,
    'permissionsReal' => $permissionsReal,
  );
}

/**
 * @return ?array{
 *   type: 'collection',
 *   handle: string,
 *   class: string,
 *   bsIcon: string,
 *   url: string,
 *   lastModified: int|false,
 *   fileSize: int|false,
 *   count: int|false,
 *   permissionsReal: string|false
 * }
 */
function errorsLogsCollectionData(string $errorHandle): ?array {
  $database = getDatabase();
  $collection = $database->{$errorHandle};

  $count = $collection->countDocuments();
  if ($count === 0) {
    return null;
  }

  $logLast = $collection->findOne([], ['sort' => ['_id' => -1], 'limit' => 1]);
  $logLastTimestamp = !empty($logLast) ? $logLast['_id']->getTimestamp() : false;

  $class = '';
  $bsIcon = '';
  if ($logLastTimestamp && $count > 0 && time() - $logLastTimestamp < 60 * 60) {
    $class = 'text-danger';
    $bsIcon = 'bi-exclamation-circle-fill';
  } elseif ($logLastTimestamp && $count > 0 && time() - $logLastTimestamp < 60 * 60 * 24) {
    $class = 'text-warning';
    $bsIcon = 'bi-exclamation-circle-fill';
  }

  return array(
    'type' => 'collection',
    'handle' => $errorHandle,
    'class' => $class,
    'bsIcon' => $bsIcon,
    'url' => getCorrectUrl('error-logs/'.$errorHandle.'/collection/view'),
    'lastModified' => $logLastTimestamp,
    'fileSize' => false,
    'count' => $count,
    'permissionsReal' => false,
  );
}

function sizeUnitsFormat(int $filesize): string {
  if ($filesize / 1024 > 1) {
    $filesize /= 1024;
    if ($filesize / 1024 > 1) {
      $filesize /= 1024;
      if ($filesize / 1024 > 1) {
        $filesize /= 1024;
        if ($filesize / 1024 > 1) {
          $filesize /= 1024;
          if ($filesize / 1024 > 1) {
            return (round($filesize * 100) / 100) .'PB';
          }
          return (round($filesize * 100) / 100) .'TB';
        }
        return (round($filesize * 100) / 100) .'GB';
      }
      return (round($filesize * 100) / 100) .'MB';
    }
    return (round($filesize * 100) / 100) .'kB';
  }
  return $filesize .'B';
}

function logErrorMessage(string $filename, string $message): void {
  global $logToFile;

  if ($logToFile) {
    file_put_contents(dirname(__DIR__).'/logs/'.$filename.'.log', (new DateTime())->format('Y-m-d H:i:s.v O').' - '.$message.END_LINE, FILE_APPEND);
  } else {
    logViewerLogsSave($filename, $message);
  }
}

/**
 * @param array<string, mixed> $logData
 */
function logViewerLogsSave(string $logTable, string $logMessage = '', array $logData = []): void {
  try {
    $database = getDatabase();
    $database->command(['ping' => 1]); // Send a ping to confirm a successful connection
    $dataToInsert = [];
    if (!empty($logMessage)) {
      $dataToInsert['message'] = $logMessage;
    }
    if (!empty($logData)) {
      $dataToInsert = array_merge($dataToInsert, $logData);
    }
    $dataToInsert['datetime'] = (new DateTime())->format('Y-m-d H:i:s.v O');

    $result = $database->{$logTable}->insertOne($dataToInsert);
    if ($result->getInsertedCount() === 0) {
      throw new Exception('Log was not inserted into DB.');
    }
  } catch (Exception $exception) {
    if (!empty($logMessage)) {
      file_put_contents(dirname(__DIR__).'/logs/'.$logTable.'.log', (new DateTime())->format('Y-m-d H:i:s.v O').' - could not save log to DB error: '.$exception->getMessage().' - original message: '.$logMessage."\n", FILE_APPEND);
    } elseif (!empty($logData)) {
      file_put_contents(dirname(__DIR__).'/logs/'.$logTable.'.log', (new DateTime())->format('Y-m-d H:i:s.v O').' - could not save log to DB error: '.$exception->getMessage().' - original data: '.print_r($logData)."\n", FILE_APPEND);
    }
  }
}

/**
 * @return array<int, mixed>
 */
function cursorToArray(MongoDB\Driver\Cursor $cursor): array {
  $array = [];
  /** @var array<string, mixed> $item */
  foreach ($cursor as $item) {
    $data = [];
    foreach ($item as $key => $value) {
      if ($value instanceof MongoDB\BSON\ObjectID) {
        $data[$key] = (string)$value;
      } elseif ($value instanceof MongoDB\Model\BSONArray) {
        $data[$key] = iterator_to_array($value);
      } elseif ($value instanceof MongoDB\Model\BSONDocument) {
        $data[$key] = bsonDocumentToArray($value);
      } else {
        $data[$key] = $value;
      }
    }
    $array[] = $data;
  }
  return $array;
}

/**
 * @return array<string, mixed>
 */
function bsonDocumentToArray(MongoDB\Model\BSONDocument $document): array {
  $data = [];
  foreach ($document as $key => $item) {
    if ($item instanceof MongoDB\BSON\ObjectID) {
      $data[$key] = (string)$item;
    } elseif ($item instanceof MongoDB\Model\BSONArray) {
      $data[$key] = iterator_to_array($item);
    } elseif ($item instanceof MongoDB\Model\BSONDocument) {
      $data[$key] = bsonDocumentToArray($item);
    } else {
      $data[$key] = $item;
    }
  }
  return $data;
}

/**
 * @param array{field: string, order: string} $sort
 */
function arraySort(array $sort): callable {
  return function ($a, $b) use ($sort) {
    $order = $sort['order'] === 'asc' ? 1 : -1;

    if (isset($a[$sort['field']]) && isset($b[$sort['field']])) {
      if (is_string($a[$sort['field']]) && is_string($b[$sort['field']])) {
        return strcmp($a[$sort['field']], $b[$sort['field']]) * $order;
      }
      return ($a[$sort['field']] - $b[$sort['field']]) * $order;
    } elseif (isset($a[$sort['field']])) {
      return 1 * $order;
    }
    return -1 * $order;
  };
}

/**
 * @return array<int, array{name: string, type: string, required: bool}>
 */
function logsFields(string $projectId): array {
  $fields = [];
  $database = getDatabase();
  $collectionsInfo = $database->listCollections(['filter' => ['name' => 'logs_'.$projectId]]);
  $collectionsInfo->rewind();
  $collectionInfo = $collectionsInfo->current();

  $collectionOptions = $collectionInfo->getOptions();
  if (!empty($collectionOptions) && !empty($collectionOptions['validator'])) {
    foreach ($collectionOptions['validator']['$jsonSchema']['properties'] as $key => $value) {
      $fields[] = ['name' => $key, 'type' => $value['bsonType'], 'required' => !empty($collectionOptions['validator']['$jsonSchema']['required']) && in_array($key, $collectionOptions['validator']['$jsonSchema']['required'])];
    }
  }

  return $fields;
}

/**
 * @param array<string|int, mixed> $logsFilterSetting
 * @return array<string, string|int|array<string, mixed>>
 */
function mongoDBLogsFilter(array $logsFilterSetting): array {
  $logsFilter = [];
  if (empty($logsFilterSetting)) {
    return $logsFilter;
  }

  /**
   * @var array<string|int, string|int|array<string, mixed>> $setting
   */
  foreach ($logsFilterSetting as $key => $setting) {
    $field = $key;
    if (is_int($field)) {
      if (!empty($setting['field']) && is_string($setting['field'])) {
        $field = $setting['field'];
      } else {
        trigger_error('Invalid field supplied for filter $key: '.$key.', filter: '.print_r($logsFilterSetting, true), E_USER_WARNING);
        continue;
      }
    }
    if (empty($setting['value'])) {
      continue;
    }
    $filter = null;
    if ($setting['type'] === 'number') {
      if ($setting['filter'] === 'between' && isset($setting['value']['min']) && isset($setting['value']['max'])) {
        $filter = ['$lt' => $setting['value']['max'], '$gt' => $setting['value']['min']];
      } elseif ($setting['filter'] === 'less') {
        $filter = ['$lt' => $setting['value']];
      } elseif ($setting['filter'] === 'greater') {
        $filter = ['$gt' => $setting['value']];
      } else {
        $filter = $setting['value'];
      }
    } elseif ($setting['type'] === 'array') {
      if (is_array($setting['value'])) {
        $filter = ['$in' => $setting['value']];
      } else {
        $filter = $setting['value'];
      }
    } elseif ($field === '_id' && is_string($setting['value'])) {
      if ($setting['filter'] === 'timestamp') {
        $filter = timestampToMongoDbId((int)$setting['value']);
      } elseif ($setting['filter'] === 'timestamp__less') {
        $filter = ['$lt' => timestampToMongoDbId((int)$setting['value'])];
      } else {
        $filter = new MongoDB\BSON\ObjectID($setting['value']);
      }
    } else {
      if (is_array($setting['value'])) {
        $filter = ['$in' => $setting['value']];
      } elseif ($setting['filter'] === 'regex') {
        $filter = ['$regex' => $setting['value']];
      } elseif ($setting['filter'] === 'starts') {
        $filter = ['$regex' => '^'.$setting['value']];
      } elseif ($setting['filter'] === 'contains') {
        $filter = ['$regex' => $setting['value']];
      } elseif ($setting['filter'] === 'ends') {
        $filter = ['$regex' => $setting['value'].'$'];
      } else {
        $filter = $setting['value'];
      }
    }
    if (!empty($setting['negative'])) {
      if (is_array($filter)) {
        $filter = ['$not' => $filter];
      } else {
        $filter = ['$ne' => $filter];
      }
    }
    $logsFilter[$field] = $filter;
  }

  return $logsFilter;
}

/**
 * @param array<string, mixed> $dataJson
 */
function logInsertData(string $data, array $dataJson, \MongoDB\InsertOneResult $result = null): void {
  global $insertDataLog, $logToFile, $insertDataLogSuccess;

  if (empty($insertDataLog)) {
    return;
  }

  if (empty($insertDataLogSuccess) && !is_null($result) && $result->getInsertedCount() > 0) {
    return;
  }

  $token = getBearerToken();
  $token = !empty($token) ? tokenAnonymize($token) : '-- no token --';

  $resultString = 'added';
  if (is_null($result)) {
    $resultString = 'error';
  } elseif ($result->getInsertedCount() === 0) {
    $resultString = 'skipped';
  }
  if ($logToFile) {
    if ($insertDataLog === 'simple') {
      file_put_contents(dirname(__DIR__).'/logs/insert_data.log', (new DateTime())->format('Y-m-d H:i:s.v O').' - '.$token.' '.sizeUnitsFormat(strlen($data)).' - '.$resultString."\n", FILE_APPEND);
    } elseif ($insertDataLog === 'full') {
      requestStoreToFile(dirname(__DIR__).'/logs/insert_data.log', $dataJson, ['result' => $resultString]);
    }
  } else {
    if ($insertDataLog === 'simple') {
      logViewerLogsSave('insert_data', $token.' '.sizeUnitsFormat(strlen($data)).' - '.$resultString);
    } elseif ($insertDataLog === 'full') {
      logViewerLogsSave('insert_data', '', requestStoreToFileData($dataJson, ['result' => $resultString]));
    }
  }
}

function timestampToMongoDbId(int $timestamp): \MongoDB\BSON\ObjectID {
  $hexTimestamp = dechex($timestamp);
  $hexTimestamp = str_pad($hexTimestamp, 8, '0', STR_PAD_LEFT);
  return new MongoDB\BSON\ObjectID($hexTimestamp.'0000000000000000');
}

function collectionExists(string $name): bool {
  $database = getDatabase();
  $collections = $database->listCollections(['filter' => ['name' => $name]]);
  return iterator_count($collections) > 0;
}

function compareValues(mixed $a, mixed $b): bool {
  $aType = gettype($a);
  $bType = gettype($b);
  if ($aType !== $bType) {
    if (in_array($aType, ['integer', 'double']) && in_array($bType, ['integer', 'double']) && $a == $b) {
      return true;
    }
    return false;
  }

  if (is_array($a) && is_array($b)) {
    if (count($a) !== count($b)) {
      return false;
    }
    if (empty($a) && empty($b)) {
      return true;
    }

    foreach ($a as $key => $value) {
      if (!isset($b[$key]) || !compareValues($value, $b[$key])) {
        return false;
      }
    }
    return true;
  }
  if (is_object($a) && is_object($b)) {
    if (get_class($a) !== get_class($b)) {
      return false;
    }
    return $a == $b;
  }
  if (is_double($a)) {
    if ($a === $b) {
      return true;
    }
    return abs($a - $b) < 0.0001;
  }

  return $a === $b;
}

function getIdForPage(string $page): string {
  switch ($page) {
    case 'dashboard_edit':
    case 'dashboard_save':
    case 'dashboard_view':
    case 'dashboard_deactivate':
      return !empty($_REQUEST['dashboard_id']) ? $_REQUEST['dashboard_id'] : '';
    case 'user_edit':
    case 'user_save':
    case 'user_deactivate':
    case 'user_password_edit':
    case 'user_password_save':
      if (!empty($_REQUEST['user_id'])) {
        return $_REQUEST['user_id'] === 'me' ? $_SESSION['LVuser']['_id'] : $_REQUEST['user_id'];
      }
      return '';
    case 'project_save':
    case 'project_edit':
    case 'project_delete':
    case 'project_logs':
      return !empty($_REQUEST['project_id']) ? $_REQUEST['project_id'] : '';
    case 'key_edit':
    case 'key_save':
      return !empty($_REQUEST['key_id']) ? $_REQUEST['key_id'] : '';
    case 'tag_edit':
    case 'tag_save':
    case 'tag_deactivate':
      return !empty($_REQUEST['tag_id']) ? $_REQUEST['tag_id'] : '';
  }

  return '';
}

/**
 * @param string[] $projectHandles
 * @return array{
 *   row: array<string, array{
 *     '_id': string,
 *     'active': int,
 *     'favourite': int,
 *     'handle': string,
 *     'settings': array{
 *       'name'?: string,
 *       'project_handles'?: string[],
 *       'project_handle'?: string
 *      }
 *   }>,
 *   pie: array<int, array{
 *     '_id': string,
 *     'active': int,
 *     'favourite': int,
 *     'handle': string,
 *     'settings': array{
 *       'name'?: string,
 *       'project_handles'?: string[],
 *       'project_handle'?: string
 *     }
 *   }>
 * }
 */
function projectListGraphSettings(array $projectHandles): array {
  $database = getDatabase();
  $projectListGraphSettings = array(
    'row' => [],
    'pie' => [],
  );

  $graphSettings = cursorToArray($database->log_viewer_settings->find(['handle' => ['$in' => ['projectLogsGraph', 'logsRowGraph']], 'active' => 1, 'favourite' => 1]));

  /** @var array{'_id': string, 'active': int, 'favourite': int, 'handle': string, 'settings': array{'name'?: string, 'project_handles'?: string[], 'project_handle'?: string}} $graphSetting */
  foreach ($graphSettings as $graphSetting) {
    if ($graphSetting['handle'] === 'projectLogsGraph') {
      if (!empty($graphSetting['settings']['project_handles']) && !empty(array_intersect($graphSetting['settings']['project_handles'], $projectHandles))) {
        $projectListGraphSettings['pie'][] = $graphSetting;
      }
    } elseif (!empty($graphSetting['settings']['project_handle'])) {
      $projectListGraphSettings['row'][$graphSetting['settings']['project_handle']] = $graphSetting;
    }
  }

  return $projectListGraphSettings;
}

/**
 * @param array<MongoDB\Model\BSONDocument> $projects
 * @return array<array{'_id': string, 'handle': string, 'priority': int|null, 'name': string, 'count': int, 'logToDeleteCount': int, 'logLastTimestamp': int|bool, 'class': string, userCanDelete: bool, userCanMark: bool}>
 */
function projectListData(array $projects) {
  $database = getDatabase();
  $projectListData = [];

  /** @var array{_id: MongoDB\BSON\ObjectID, handle: string, priority?: int, name: string} $project */
  foreach ($projects as $project) {
    $class = '';
    $logs = $database->{'logs_'.$project['_id']};
    $logCount = $logs->countDocuments(['__ignored' => ['$ne' => true]]);
    $logToDeleteCount = $logs->countDocuments(['__ignored' => ['$ne' => true], 'to_be_deleted' => true]);
    $logLast = $logs->findOne(['__ignored' => ['$ne' => true]], ['sort' => ['_id' => -1], 'limit' => 1]);
    $logLastTimestamp = false;
    if (!empty($logLast)) {
      $logLastTimestamp = $logLast['_id']->getTimestamp();
      if ($logLastTimestamp && $logCount > 0 && time() - $logLastTimestamp < 60 * 60) {
        $class = 'text-danger';
      } elseif ($logLastTimestamp && $logCount > 0 && time() - $logLastTimestamp < 60 * 60 * 24) {
        $class = 'text-warning';
      }
    }

    $logInfo = array(
      '_id' => (string)$project['_id'],
      'handle' => $project['handle'],
      'priority' => isset($project['priority']) ? $project['priority'] : null,
      'name' => $project['name'],
      'count' => $logCount,
      'logToDeleteCount' => $logToDeleteCount,
      'logLastTimestamp' => $logLastTimestamp,
      'class' => $class,
    );

    $logInfo['userCanDelete'] = permissionsCheckProject((string)$project['_id'], ['admin']);
    $logInfo['userCanMark'] = $logInfo['userCanDelete'] || permissionsCheckProject((string)$project['_id'], ['maintainer', 'admin']);

    $projectListData[] = $logInfo;
  }

  return $projectListData;
}

/**
 * @param array<array{
 *   _id: string,
 *   handle: string,
 *   priority: int|null,
 *   name: string,
 *   count: int,
 *   logToDeleteCount: int,
 *   logLastTimestamp: int|bool,
 *   class: string,
 *   userCanDelete: bool,
 *   userCanMark: bool
 * }> $projectListData
 * @return array<int, array{
 *   id?: string,
 *   url?: string,
 *   value: string,
 *   class?: string,
 *   title?: string,
 *   onclick?: string,
 *   data?: array<string, string>
 * }>
 */
function projectListSubmenu(array $projectListData, string $dashboardId = '') {
  $links = [];
  if (isSuperuser()) {
    $links[] =['url' => 'projects/new/edit', 'value' => '<i class="bi bi-plus buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Add Project'];
  }
  if (count($projectListData) > 0) {
    $links[] = array(
      'url' => '#',
      'value' => '<i class="bi bi-graph-up-arrow buttonIcon"></i>',
      'class' => 'btn-outline-secondary',
      'title' => 'Show graph for Logs distribution between projects.',
      'data' => ['bs-toggle' => 'modal', 'bs-target' => '#projectLogsGraphModal']
    );
  }

  $links[] = array(
    'url' => '#',
    'value' => '<i class="bi bi-search buttonIcon"></i>',
    'class' => 'btn-outline-secondary',
    'title' => 'Search projects logs for specific key value.',
    'data' => ['bs-toggle' => 'modal', 'bs-target' => '#projectLogsSearchModal']
  );

  if (!empty($dashboardId) && permissionsCheck('dashboard_edit', $dashboardId)) {
    $links[] = array(
      'url' => 'dashboards/'.$dashboardId.'/edit',
      'value' => '<i class="bi bi-pencil buttonIcon"></i>',
      'class' => 'btn-outline-secondary',
      'title' => 'Edit Dashboard.',
    );
  }

  if (!empty(array_filter(array_column($projectListData, 'userCanMark')))) {
    $links[] = array(
      'url' => '#',
      'value' => '<i class="bi bi-clock-history buttonIcon"></i>',
      'class' => 'btn-outline-danger',
      'title' => 'Mark logs older then date specified to be deleted.',
      'data' => ['bs-toggle' => 'modal', 'bs-target' => '#logsMarkModal']
    );
  }

  $logToDeleteCount = 0;
  $logToDeleteHandles = [];
  foreach ($projectListData as $projectLogsInfo) {
    if (!empty($projectLogsInfo['logToDeleteCount']) && permissionsCheckProject($projectLogsInfo['_id'], ['admin'])) {
      $logToDeleteCount += $projectLogsInfo['logToDeleteCount'];
      $logToDeleteHandles[] = $projectLogsInfo['handle'];
    }
  }
  if ($logToDeleteCount > 0) {
    $links[] = array(
      'id' => 'button___all_logs_delete',
      'url' => '#',
      'value' => '<i class="bi bi-trash3 buttonIcon"></i>',
      'class' => 'btn-outline-danger',
      'onclick' => 'logMarkedDelete('.htmlspecialchars((string)json_encode($logToDeleteHandles), ENT_QUOTES).', \'all Projects\', \''.getCorrectUrl('').'\'); return false;',
      'title' => 'Delete '.$logToDeleteCount.' marked logs.',
    );
  }
  return $links;
}

/**
 * @return array<int, array{
 *   type: string,
 *   handle: string,
 *   class: string,
 *   bsIcon: string,
 *   url: string,
 *   lastModified: int|false,
 *   fileSize: int|false,
 *   count: int|false,
 *   permissionsReal: string|false
 * }>
 */
function errorLogsInfo(): array {
  $errorHandles = errorLogsHandles();

  $errorsInfo = [];

  foreach ($errorHandles as $errorHandle) {
    $fileErrors = errorsLogsFilesData($errorHandle);
    if (!empty($fileErrors)) {
      $errorsInfo[] = $fileErrors;
    }

    $collectionErrors = errorsLogsCollectionData($errorHandle);
    if (!empty($collectionErrors)) {
      $errorsInfo[] = $collectionErrors;
    }
  }

  return $errorsInfo;
}

/**
 * @return string[]
 */
function errorLogsHandles(): array {
  return array(
    'errors_logs_insert',
    'errors_error_logs_delete',
    'errors_log_viewer_settings_save',
    'errors_logs_clear',
    'errors_logs_delete',
    'errors_parser_settings_save',
    'errors_rate_limit_delete',
    'errors_user_key_create',
    'errors_user_key_deactivate',
    'errors_log_viewer',
  );
}

function isColorLight(string $color): bool {
  $colorArray = str_split(strtr($color, ['#' => '']));
  $r = hexdec($colorArray[0].$colorArray[1]);
  $g = hexdec($colorArray[2].$colorArray[3]);
  $b = hexdec($colorArray[4].$colorArray[5]);

  return ((max($r, $g, $b) + min($r, $g, $b)) / 510.0) > 0.6;
}

function attemptRefreshToLoginIfFromExternalSite(): void {
  if (!empty($_REQUEST['did_refresh']) || !empty($_COOKIE['did_refresh'])) {
    return;
  }
  if (isUser()) {
    return;
  }
  if ($_SERVER['REQUEST_METHOD'] !== 'GET') {
    return;
  }
  if (!empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], getCorrectUrl('')) !== false) {
    return;
  }
  attemptRefreshToLogin();
}

function attemptRefreshToLogin(): void {
  global $preferHttps;
  $rateLimits = [(string)$_SERVER['REMOTE_ADDR'] => 2];
  // $_REQUEST['did_refresh'] is required for .htaccess entries
  if (empty($_REQUEST['did_refresh']) && empty($_COOKIE['did_refresh']) && !memcachedRateLimitExceeded('did_refresh', $rateLimits, 10)) {
    header('Set-Cookie: dummy=1', true); // Reset existing cookies (eg. PHPSESSIONID)
    setcookie('did_refresh', '1', array(
      'expires' => time() + 10,
      'path' => '/',
      'domain' => '',
      'secure' => $preferHttps,
      'httponly' => true,
      'samesite' => 'Strict'
    ));
    memcachedRateLimitIncrement('did_refresh', $rateLimits, 10);
    die('<html><head><meta http-equiv="refresh" content="0" /></head></html>');
  }
}

function underscoresToSpaceCapital(string $input): string {
  return ucwords(strtr($input, ['_' => ' ']));
}

/**
 * @param array<string, mixed> $dataJson
 * @return array{
 *   name: string,
 *   active: int,
 *   active_until: int,
 *   action: string,
 *   project_id: string,
 *   rules: array<int, array{field: string, type: string, value: string}>
 * }
 * @throws \LogViewer\Exception\ExceptionWithData
 */
function tagPostValidateAndSanitize(array $dataJson): array {
  $database = getDatabaseOrDie();

  if (!isset($dataJson['project_id']) || !is_string($dataJson['project_id'])) {
    $data = array(
      'log_message' => 'missing project id, keys: '.implode(', ', array_keys($dataJson)),
      'form_error' => array(
        'tag' => ['project_id' => 'Project is required.']
      )
    );
    throw new \LogViewer\Exception\ExceptionWithData('Missing Project ID.', data: $data);
  }
  $project = $database->projects->findOne(['_id' => new MongoDB\BSON\ObjectID($dataJson['project_id'])]);
  if (empty($project)) {
    $data = array(
      'log_message' => 'invalid project id: '.$dataJson['project_id'],
      'form_error' => array(
        'tag' => ['project_id' => 'Invalid Project.']
      )
    );
    throw new \LogViewer\Exception\ExceptionWithData('Invalid Project ID.', data: $data);
  }

  if (!permissionsCheckProject((string)$project['_id'], ['maintainer', 'admin'])) {
    $data = array(
      'log_message' => 'project '.$project['_id'].' access denied',
      'form_error' => array(
        'tag' => ['project_id' => 'Access denied.'],
      )
    );
    throw new \LogViewer\Exception\ExceptionWithData('Access denied.', 403, data: $data);
  }

  $name = !empty($dataJson['name']) && is_string($dataJson['name']) ? sanitizeStringInput($dataJson['name']) : '';
  if (strlen($name) === 0) {
    $data = array(
      'log_message' => 'invalid value supplied for name, value: '.$dataJson['name'],
      'form_error' => array(
        'tag' => ['name' => 'Invalid name supplied.']
      )
      );
    throw new \LogViewer\Exception\ExceptionWithData('Invalid name supplied.', data: $data);
  }

  $activeUntil = !empty($dataJson['active_until']) && is_string($dataJson['active_until']) ? strtr($dataJson['active_until'], ['T' => ' ']) : '';
  if (empty($activeUntil)) {
    $data = array(
      'log_message' => 'supplied invalid value for active_until, value: -- no value --',
      'form_error' => array(
        'tag' => ['active_until' => 'Active until must be set.']
      )
    );
    throw new \LogViewer\Exception\ExceptionWithData('Active until must be set.', data: $data);
  }
  $date = DateTime::createFromFormat('Y-m-d H:i', $activeUntil);
  if (!$date || $date->format('Y-m-d H:i') !== $activeUntil) {
    $data = array(
      'log_message' => 'supplied invalid value for active_until, value: '.print_r($dataJson['active_until'], true),
      'form_error' => array(
        'tag' => ['active_until' => 'Invalid value supplied.']
      )
    );
    throw new \LogViewer\Exception\ExceptionWithData('Invalid date supplied for active until.', data: $data);
  }
  if ($date < (new DateTime())) {
    $data = array(
      'log_message' => 'supplied invalid value for active_until, value: '.print_r($dataJson['active_until'], true),
      'form_error' => array(
        'tag' => ['active_until' => 'Date cannot be in the past.']
      )
    );
    throw new \LogViewer\Exception\ExceptionWithData('Active until cannot be in the past.', data: $data);
  }

  $action = !empty($dataJson['action']) && is_string($dataJson['action']) ? sanitizeStringInput($dataJson['action']) : '';
  if (strlen($action) === 0 || !in_array($action, ['ignore'])) {
    $data = array(
      'log_message' => 'invalid value supplied for action, value: '.$dataJson['action'],
      'form_error' => array(
        'tag' => ['action' => 'Invalid action supplied.']
      )
      );
    throw new \LogViewer\Exception\ExceptionWithData('Invalid action supplied.', data: $data);
  }

  $rules = !empty($dataJson['rules']) && is_array($dataJson['rules']) ? array_values(array_filter($dataJson['rules'])) : [];
  if (empty($rules)) {
    $data = array(
      'log_message' => 'tag has to have at least one rule, value: '.(!empty($dataJson['rules']) ? print_r($dataJson['rules'], true) : '-- no value --'),
      'form_error' => array(
        'tag' => ['rules' => ['global' => 'Tag has to have at least one Rule.']]
      )
    );
    throw new \LogViewer\Exception\ExceptionWithData('Tag has to have at least one Rule.', data: $data);
  }

  $fields = logsFields((string)$project['_id']);
  $fieldsTypes = [];
  foreach ($fields as $field) {
    $fieldsTypes[$field['name']] = $field['type'];
  }

  foreach ($rules as $index => $rule) {
    $rules[$index]['field'] = sanitizeStringInput($rule['field']);
    if (empty($rules[$index]['field']) || empty($fieldsTypes[$rules[$index]['field']])) {
      $data = array(
        'log_message' => 'supplied invalid field for rule, value: '.(!empty($rule['field']) ? print_r($rule['field'], true) : '-- no field --'),
        'form_error' => array(
          'tag' => ['rules' => [$index => 'Field is required.']]
        )
      );
      throw new \LogViewer\Exception\ExceptionWithData('Rule '.($index + 1).' must have field.', data: $data);
    }

    if (empty($rules[$index]['filter'])) {
      $data = array(
        'log_message' => 'supplied invalid filter for rule, value: -- no filter --',
        'form_error' => array(
          'tag' => ['rules' => [$index => 'Filter is required.']]
        )
      );
      throw new \LogViewer\Exception\ExceptionWithData('Rule '.($index + 1).' must have filter.', data: $data);
    } elseif (!in_array($rules[$index]['filter'], ['equals', 'not_equals', 'less', 'greater', 'contains', 'not_contains', 'version_compare'])) {
      $data = array(
        'log_message' => 'supplied invalid filter for rule, value: '.print_r($rule['type'], true),
        'form_error' => array(
          'tag' => ['rules' => [$index => 'Invalid filter supplied.']]
        )
      );
      throw new \LogViewer\Exception\ExceptionWithData('Rule '.($index + 1).' must have type.', data: $data);
    }

    $rules[$index]['type'] = $fieldsTypes[$rules[$index]['field']];

    switch ($fieldsTypes[$rules[$index]['field']]) {
      case 'string':
        $rules[$index]['value'] = sanitizeStringInput($rule['value']);
        break;
      case 'int':
        $rules[$index]['value'] = (int)$rule['value'];
        break;
      case 'double':
        $rules[$index]['value'] = (float)$rule['value'];
        break;
      case 'array':
      default:
        $rules[$index]['value'] = (array)$rule['value'];
        break;
    }

    if (empty($rules[$index]['value'])) {
      $data = array(
        'log_message' => 'supplied invalid value for rule, value: '.(!empty($rule['value']) ? print_r($rule['value'], true) : '-- no value --'),
        'form_error' => array(
          'tag' => ['rules' => [$index => 'Value is required.']]
        )
      );
      throw new \LogViewer\Exception\ExceptionWithData('Rule '.($index + 1).' must have value.', data: $data);
    }
  }

  return array(
    'name' => $name,
    'active' => 1,
    'active_until' => $date->getTimestamp(),
    'action' => $action,
    'project_id' => $dataJson['project_id'],
    'rules' => $rules,
  );
}

/**
 * @param array<string, mixed> $dataJson
 * @return ?array{
 *   _id: string,
 *   name: string,
 *   active: int,
 *   active_until: int,
 *   action: string,
 *   project_id: string,
 *   rules: array<int, array{field: string, filter: string, type: string, value: string}>
 * }
 */
function logInsertTagFind(string $projectId, array $dataJson): ?array {
  $database = getDatabase();

  $tags = cursorToArray($database->tags->find(['project_id' => $projectId, 'active' => 1, 'active_until' => ['$gt' => time()]]));
  /** @var array{_id: string, name: string, active: int, active_until: int, action: string, project_id: string, rules: array<int, array{field: string, filter: string, type: string, value: string}>} $tag */
  foreach ($tags as $tag) {
    if ($tag['action'] === 'ignore') {
      $ruleMatch = true;
      foreach ($tag['rules'] as $rule) {
        if (!isset($dataJson[$rule['field']])) {
          $ruleMatch = false;
          break;
        }

        switch ($rule['filter']) {
          case 'equals':
            $ruleMatch = compareValues($dataJson[$rule['field']], $rule['value']);
            break;
          case 'not_equals':
            $ruleMatch = !compareValues($dataJson[$rule['field']], $rule['value']);
            break;
          case 'less':
            $ruleMatch = $dataJson[$rule['field']] < $rule['value'];
            break;
          case 'greater':
            $ruleMatch = $dataJson[$rule['field']] > $rule['value'];
            break;
          case 'contains':
            $ruleMatch = is_string($dataJson[$rule['field']]) && preg_match('/'.$rule['value'].'/', $dataJson[$rule['field']]) === 1;
            break;
          case 'not_contains':
            $ruleMatch = is_string($dataJson[$rule['field']]) && preg_match('/'.$rule['value'], '/', $dataJson[$rule['field']]) !== 1;
            break;
          case 'version_compare':
            if (is_string($dataJson[$rule['field']])) {
              foreach (explode(',', $rule['value']) as $value) {
                if (version_compare($dataJson[$rule['field']], $value) === 0) {
                  break 2;
                }
              }
            }
            $ruleMatch = false;
            break;
          default:
            trigger_error('Unknown rule comparison type supplied: '.$rule['filter'].', tag id: '.$tag['_id'], E_USER_WARNING);
            break;
        }

        if (!$ruleMatch) {
          break;
        }
      }
      if ($ruleMatch) {
        return $tag;
      }
    }
  }
  return null;
}

/**
 * @param array<int|string, array{name: string, type: string, required?: bool}> $fields
 * @return array<string, array{value: string|int|double|array<string, mixed>, filter: string, type: string, negative: int}>
 */
function requestFilterSettingsParse(array $fields, string $filterDefault = 'equals', string $requestKey = 'filter'): array {
  if (empty($_REQUEST[$requestKey])) {
    return [];
  }

  $fieldsTypes = [];
  foreach ($fields as $field) {
    $fieldsTypes[$field['name']] = $field['type'];
  }

  $filterSettings = [];
  foreach ($_REQUEST[$requestKey] as $key => $value) {
    if (!is_string($key)) {
      trigger_error('Filter has numeric key: '.print_r($_REQUEST[$requestKey], true), E_USER_WARNING);
      continue;
    }
    $pos = strpos($key, '__');
    $filter = $filterDefault;
    $negative = 0;
    if ($pos !== false && !isset($fieldsTypes[$key])) {
      $filter = substr($key, $pos + 2);
      $key = substr($key, 0, $pos);

      if (strpos($filter, 'not_') === 0) {
        $negative = 1;
        $filter = strtr($filter, ['not_' => '']);
      }
    }

    $type = !empty($fieldsTypes[$key]) ? $fieldsTypes[$key] : 'string';
    if ($filter === 'version_compare') {
      $filter = 'equals';
      $type = 'array';
      $value = explode(',', $value);
    }

    if ($type === 'int') {
      $type = 'number';
      if (is_array($value)) {
        $value = array_map('intval', $value);
      } else {
        $value = (int)$value;
      }
    } elseif ($type === 'double') {
      $type = 'number';
      if (is_array($value)) {
        $value = array_map('floatval', $value);
      } else {
        $value = (float)$value;
      }
    }

    $filterSettings[$key] = array(
      'value' => $value,
      'filter' => $filter,
      'type' => $type,
      'negative' => $negative
    );
  }

  return $filterSettings;
}

/**
 * @param array{
 *   _id: string,
 *   name: string,
 *   active_until: int,
 *   action: string,
 *   project_id: string,
 *   rules: array<int, array{field: string, type: string, value: string}>
 * } $tag
 */
function tagProcess(array $tag): int {
  $database = getDatabase();

  $logsFilter = mongoDBLogsFilter($tag['rules']);
  $logsCollection = $database->{'logs_'.$tag['project_id']};
  return $logsCollection->updateMany($logsFilter, ['$set' => ['__tag_id' => $tag['_id'], '__ignored' => true]])->getModifiedCount();
}
