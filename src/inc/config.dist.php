<?php

if (!defined('END_LINE')) {
  define('END_LINE', "\r\n");
}

$mongoDbUri = ''; // Uri for you DB e.g. `mongodb://localhost:27017`.
$mongoDbParams = array( // Options passed to the `new MongoDB\Client()`.
);

$urlPrefix = ''; // String to append before links URL.
$vueDevServerPort = 8084; // port for the Log Parser app dev server, has to match the port specified in client/vite.config.js

/**
 * @var string $insertDataLog - one of ['', 'simple', 'full']
 *   - '' to not log data when inserting logs
 *   - 'simple' for single line log with only date, access token, data length
 *   - 'full' to store full request data
 */
$insertDataLog = '';
$insertDataLogSuccess = false; // if inserted data should be logged if the insert was success, by default only errors are logged
$logToFile = false; // log app errors and insert data to file instead of mongo DB

$errorHandleMaxLineLength = 120; // max length of line when saving log viewer errors to file before break is inserted
$keepLoggedInTime = 14 * 24 * 60 * 60; // how long should the user be logged in without asking a password

$trustedIpAddresses = []; // array of IP addresses that are not subject to rate limits

$memcachedConfig = array( // array of memcached servers to connect to, format: ['host' => <string>, 'port' => <int>]. 'port' can be omitted and default 11211 will be used.
);

$preferHttps = true; // false on localhost

/**
 * Allows adding headers into the <header>.
 * @param array<int, array{
 *   id?: string,
 *   url?: string,
 *   value: string,
 *   class?: string,
 *   title?: string,
 *   onclick?: string,
 *   data?: array<string, string>
 * }> $linksAdditional
 */
function pageHeaderCustom(string $title, string $activeLink = '', array $linksAdditional = [], string $containerClass = 'container'): string {
  return '';
}

/**
 * Allows adding html before </body>.
 */
function pageFooterCustom(bool $addVue = false): string {
  return '';
}
