<?php

function permissionsCheck(string $page, string $id = ''): bool {
  $unauthorizedPages = array(
    'login',
    'logout',
  );
  if (in_array($page, $unauthorizedPages)) {
    return true;
  }

  if (isUser()) {
    return permissionsCheckUser($page, $id);
  }

  return false;
}

function permissionsCheckUser(string $page, string $id = ''): bool {
  switch ($page) {
    case 'project_list':
      return true;
    case 'dashboard_list':
    case 'user_list':
    case 'key_list':
    case 'key_edit':
    case 'key_save':
    case 'rate_limit_list':
    case 'rate_limit_delete':
      return isSuperuser();
    case 'dashboard_view':
      return permissionsCheckDashboard($id, 'read');
    case 'dashboard_edit':
    case 'dashboard_save':
    case 'dashboard_deactivate':
      return permissionsCheckDashboard($id, 'edit');
    case 'user_edit':
    case 'user_save':
    case 'user_password_edit':
    case 'user_password_save':
    case 'user_key_create':
      return permissionsCheckUserEdit($id);
    case 'user_deactivate':
      return isSuperuser() && permissionsCheckUserEdit($id);
    case 'project_save':
    case 'project_edit':
    case 'project_delete':
    case 'logs_delete':
    case 'logs_insert':
      return permissionsCheckProject($id, ['admin']);
    case 'logs_clear':
    case 'log_viewer_settings_save':
      return permissionsCheckProject($id, ['maintainer', 'admin']);
    case 'tag_list':
      return permissionTypeCheck(['maintainer', 'admin']);
    case 'project_logs':
    case 'logs_info':
    case 'logs_search':
    case 'parser_settings_save':
      return permissionsCheckProject($id, ['standard', 'maintainer', 'admin']);
    case 'home':
      return isUser();
    case 'errors_logs_file_raw':
    case 'errors_logs_collection_raw':
    case 'error_logs_delete':
      return permissionsCheckAdditional('error_logs_manage');
    case 'user_key_deactivate':
      return permissionsCheckKey($id);
    case 'tag_edit':
    case 'tag_save':
    case 'tag_deactivate':
      return permissionsCheckTag($id);
  }

  trigger_error('Unknown page supplied: '.$page, E_USER_WARNING);
  return false;
}

/**
 * @param string[] $types - ['standard', 'maintainer', 'admin']
 */
function permissionsCheckProject(string $projectId, array $types): bool {
  if (isSuperuser()) {
    return true;
  }

  if (empty($_SESSION['LVuser']['permissions']['projects'][$projectId])) {
    return false;
  }

  return in_array($_SESSION['LVuser']['permissions']['projects'][$projectId], $types);
}

/**
 * @param string $type - ['read', 'edit']
 */
function permissionsCheckDashboard(string $dashboardId, string $type): bool {
  if (isSuperuser()) {
    return true;
  }

  if ($type === 'read') {
    return !empty($_SESSION['LVuser']['permissions']['dashboards']) && in_array($dashboardId, $_SESSION['LVuser']['permissions']['dashboards']);
  }
  return false;
}

function permissionsCheckUserEdit(string $userId): bool {
  if ($userId === $_SESSION['LVuser']['_id']) {
    return true;
  }
  if ($userId === 'new') {
    return isSuperuser();
  }
  if (isSuperuser()) {
    $database = getDatabase();
    $user = $database->users->findOne(['_id' => new MongoDB\BSON\ObjectID($userId)]);
    if (!empty($user) && $user['type'] !== 'superuser') {
      return true;
    }
  }
  return false;
}

function permissionsCheckAdditional(string $permissionHandle): bool {
  if (isSuperuser()) {
    return true;
  }
  if (!isUser()) {
    return false;
  }
  return !empty($_SESSION['LVuser']['permissions']['additional']) && in_array($permissionHandle, $_SESSION['LVuser']['permissions']['additional']);
}

function permissionsCheckKey(string $keyId): bool {
  $database = getDatabase();

  $key = $database->keys->findOne(['_id' => new MongoDB\BSON\ObjectID($keyId)]);
  if (empty($key) || empty($key['user_id'])) {
    return false;
  }
  if (!permissionsCheckUserEdit($key['user_id'])) {
    return false;
  }
  return true;
}

function permissionsCheckTag(string $tagId): bool {
  if (isSuperuser()) {
    return true;
  }

  $database = getDatabase();
  if ($tagId === 'new') {
    return true;
  }

  $tag = $database->tags->findOne(['_id' => new MongoDB\BSON\ObjectID($tagId)]);
  if (empty($tag) || empty($tag['project_id'])) {
    return false;
  }
  if (empty($_SESSION['LVuser']['permissions']['projects'][$tag['project_id']])) {
    return false;
  }

  return in_array($_SESSION['LVuser']['permissions']['projects'][$tag['project_id']], ['maintainer', 'admin']);
}

function getAuthorizationHeader(): ?string {
  $authorizationHeader = null;
  if (isset($_SERVER['Authorization'])) {
    $authorizationHeader = trim($_SERVER['Authorization']);
  } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) { // Nginx or fast CGI
    $authorizationHeader = trim($_SERVER['HTTP_AUTHORIZATION']);
  } elseif (function_exists('apache_request_headers')) {
    $requestHeaders = apache_request_headers();
    if (isset($requestHeaders['Authorization'])) {
      $authorizationHeader = trim($requestHeaders['Authorization']);
    }
  }
  return $authorizationHeader;
}

function getBearerToken(): ?string {
  $authorizationHeader = getAuthorizationHeader();
  if (!empty($authorizationHeader)) {
    if (preg_match('/Bearer\s(\S+)/', $authorizationHeader, $matches)) {
      return $matches[1];
    }
  }
  return null;
}

/**
 * @return ?array{
 *   _id: string,
 *   user_id: string,
 *   name: string,
 *   active: int,
 *   token: string,
 *   last_used: float
 * }
 */
function getKeyForToken(string $token): ?array {
  static $keys = [];
  if (isset($keys[$token])) {
    return $keys[$token];
  }

  $database = getDatabase();
  $key = $database->keys->findOne(['token' => $token]);
  if (!empty($key)) {
    $timestamp = microtime(true) * 1000;
    if ($timestamp - (int)$timestamp === 0.0) {
      $timestamp += 0.001;
    }
    $database->keys->updateOne(['_id' => $key['_id']], ['$set' => ['last_used' => $timestamp]]);
    $key = bsonDocumentToArray($key);
    /** @var array{_id: string, user_id: string, name: string, active: int, token: string, last_used: float} $key */
    $keys[$token] = $key;
  } else {
    $keys[$token] = null;
  }
  return $keys[$token];
}

/**
 * @param string|string[] $types
 */
function permissionTypeCheck(string|array $types): bool {
  if (!isUser()) {
    return false;
  }
  if (isSuperuser()) {
    return true;
  }

  return !empty($_SESSION['LVuser']['permissions']['projects']) && !empty(array_intersect($_SESSION['LVuser']['permissions']['projects'], (array)$types));
}

function serviceAccessOrDie(): void {
  if (php_sapi_name() !== 'cli' && !isSuperuser()) {
    trigger_error('Missing permissions for service script, session: '.print_r($_SESSION, true), E_USER_WARNING);
    http_response_code(404);
    die('');
  }
}
