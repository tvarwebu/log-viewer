<?php

/**
 * @param array<int, array{
 *   id?: string,
 *   url?: string,
 *   value: string,
 *   class?: string,
 *   title?: string,
 *   onclick?: string,
 *   data?: array<string, string>,
 *   items?: array<int, array{id?: string, url?: string, value: string, class?: string, title?: string, onclick?: string, data?: array<string, string>}>
 * }> $linksAdditional
 */
function pageHeader(string $title, string $activeLink = '', array $linksAdditional = [], string $containerClass = 'container'): string {
  $errorLogsInfo = permissionsCheck('error_logs_delete') ? errorLogsInfo() : [];

  $links = [];
  if (!empty($errorLogsInfo)) {
    $class = 'btn-outline-secondary';
    foreach ($errorLogsInfo as $value) {
      if ($value['class'] === 'text-danger') {
        $class = 'btn-outline-danger';
      } elseif ($value['class'] === 'text-warning' && $class !== 'btn-outline-danger') {
        $class = 'btn-outline-warning';
      }
    }
    $links[] = array(
      'id' => 'errorLogsInfoButton',
      'url' => '#',
      'value' => '<i class="bi bi-exclamation-triangle buttonIcon"></i>',
      'class' => $class,
      'title' => 'View Logs insert errors',
      'data' => ['bs-toggle' => 'modal', 'bs-target' => '#errorLogsInfoModal']
    );
  }

  if (isSuperuser()) {
    $links[] = ['url' => 'dashboards', 'value' => '<i class="bi bi-speedometer2 buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Dashboards'];
    $links[] = ['url' => 'users', 'value' => '<i class="bi bi-people buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Users'];
    $links[] = ['url' => 'rate-limits', 'value' => '<i class="bi bi-clipboard2-pulse buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Rate Limits'];
  }

  if (isUser()) {
    if (permissionTypeCheck(['maintainer', 'admin'])) {
      $links[] = ['url' => 'tags', 'value' => '<i class="bi bi-tags buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Tags'];
    }
    $links[] = ['url' => 'projects', 'value' => '<i class="bi bi-file-earmark-medical buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Projects'];

    $linksGroup = ['value' => '<i class="bi bi-person buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Profile options', 'items' => []];
    $linksGroup['items'][] = ['url' => 'users/me/edit', 'value' => '<i class="bi bi-person buttonIcon"></i> Profile', 'class' => 'btn-outline-primary', 'title' => 'Profile'];
    $linksGroup['items'][] = ['url' => '#', 'onclick' => 'themeSet(\'dark\'); return false', 'value' => '<i class="bi bi-lightbulb buttonIcon"></i> Switch theme', 'id' => 'themeSwitcherDark', 'class' => 'btn-outline-dark', 'title' => 'Switch to Dark Mode'];
    $linksGroup['items'][] = ['url' => '#', 'onclick' => 'themeSet(\'light\'); return false', 'value' => '<i class="bi bi-lightbulb buttonIcon"></i> Switch theme', 'id' => 'themeSwitcherLight', 'class' => 'btn-outline-light', 'title' => 'Switch to Light Mode'];
    $links[] = $linksGroup;

    $links[] = ['url' => 'logout', 'value' => '<i class="bi bi-box-arrow-right buttonIcon"></i>', 'class' => 'btn-outline-secondary', 'title' => 'Logout'];
  } else {
    $links[] = ['url' => '#', 'onclick' => 'themeSet(\'dark\'); return false', 'value' => '<i class="bi bi-lightbulb buttonIcon"></i>', 'id' => 'themeSwitcherDark', 'class' => 'btn-outline-dark', 'title' => 'Switch to Dark Mode'];
    $links[] = ['url' => '#', 'onclick' => 'themeSet(\'light\'); return false', 'value' => '<i class="bi bi-lightbulb buttonIcon"></i>', 'id' => 'themeSwitcherLight', 'class' => 'btn-outline-light', 'title' => 'Switch to Light Mode'];
  }

  $html = '<html>'.END_LINE;
  $html .= '  <header>'.END_LINE;
  $html .= '    <title>'.htmlspecialchars($title).' - Log Viewer</title>'.END_LINE;
  $html .= '    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />'.END_LINE;
  $html .= '    <meta name="author" content="Nest Design development@nestdesign.com, Tvar Webu development@tvarwebu.cz" />'.END_LINE;
  $html .= '    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">'.END_LINE;
  $html .= '    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">'.END_LINE;
  $html .= '    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>'.END_LINE;
  $html .= '    <link href="'.getCorrectUrl('styles/default.scss?'.filemtime(dirname(__DIR__).'/styles/default.scss').'=1').'" rel="stylesheet" type="text/css" />'.END_LINE;
  $html .= '    <link href="'.getCorrectUrl('styles/override.scss?'.filemtime(dirname(__DIR__).'/styles/override.scss').'=1').'" rel="stylesheet" type="text/css" />'.END_LINE;
  $html .= '    <link href="'.getCorrectUrl('styles/responsive.scss?'.filemtime(dirname(__DIR__).'/styles/responsive.scss').'=1').'" rel="stylesheet" type="text/css" />'.END_LINE;
  $html .= '    <script src="'.getCorrectUrl('js/functions.js?'.filemtime(dirname(__DIR__).'/js/functions.js').'=1').'"></script>'.END_LINE;
  if (function_exists('pageHeaderCustom')) {
    $html .= pageHeaderCustom($title, $activeLink, $linksAdditional, $containerClass).END_LINE;
  }
  $html .= '    <script>'.END_LINE;
  $html .= '      var correctUrl = \''.getCorrectUrl('').'\';'.END_LINE;
  $html .= '    </script>'.END_LINE;
  $html .= '  </header>'.END_LINE;
  $html .= '  <body>'.END_LINE;
  $html .= '    <div class="'.htmlspecialchars($containerClass, ENT_QUOTES).' mt-3">'.END_LINE;
  $html .= '      <div class="row align-items-center">';
  $html .= '        <div class="col"><h1><a href="'.getCorrectUrl('').'"><img class="logo" src="'.getCorrectUrl('/images/logo.svg').'"></a>Log Viewer</h1></div>';
  $html .= '        <div class="col text-end col-md-auto">'.END_LINE;
  $html .= pageSubmenu($links, $activeLink);
  $html .= '        </div>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="row align-items-center">';
  $html .= '        <div class="col"><h1>'.htmlspecialchars($title).'</h1></div>';
  $html .= '        <div class="col text-end col-md-auto">'.END_LINE;
  $html .= pageSubmenu($linksAdditional, $activeLink);
  $html .= '        </div>'.END_LINE;
  $html .= '      </div>'.END_LINE;

  if (!empty($errorLogsInfo)) {
    $html .= errorLogsInfoModal($errorLogsInfo);
  }

  return $html;
}

function pageFooter(bool $addVue = false): string {
  global $vueDevServerPort;

  $html = '';
  if (function_exists('pageFooterCustom')) {
    $html .= pageFooterCustom($addVue).END_LINE;
  }
  $html .= '    </div>'.END_LINE;

  if ($addVue) {
    if (isDeveloper()) {
      $httpsOn = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on';
      $html .= '    <script type="module" src="http'.($httpsOn ? 's' : '').'://'.getDomainName().':'.$vueDevServerPort.'/@vite/client"></script>'.END_LINE;
      $html .= '    <script type="module" src="http'.($httpsOn ? 's' : '').'://'.getDomainName().':'.$vueDevServerPort.'/src/main.js"></script>'.END_LINE;
    } else {
      $html .= '    <link href="'.getCorrectUrl('js/vuejs/css/app.css?'.filemtime(dirname(__DIR__).'/js/vuejs/css/app.css').'=1').'" rel="stylesheet" type="text/css" />'.END_LINE;
      $html .= '    <script src="'.getCorrectUrl('js/vuejs/app.js?'.filemtime(dirname(__DIR__).'/js/vuejs/app.js').'=1').'"></script>'.END_LINE;
    }
  }
  $html .= '    <div id="toastContainerMain" class="toast-container p-3 top-0 end-0 position-fixed" id="toastPlacement">'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '    <script>'.END_LINE;
  $html .= '      themeSet(getPreferredTheme());'.END_LINE;
  $html .= '    </script>'.END_LINE;
  $html .= messagesDisplay();
  $html .= '  </body>'.END_LINE;
  $html .= '</html>'.END_LINE;
  return $html;
}

/**
 * @param array<int, array{
 *   id?: string,
 *   url?: string,
 *   value: string,
 *   class?: string,
 *   title?: string,
 *   onclick?: string,
 *   data?: array<string, string>,
 *   items?: array<int, array{id?: string, url?: string, value: string, class?: string, title?: string, onclick?: string, data?: array<string, string>}>
 * }> $links
 */
function pageSubmenu(array $links, string $activeLink = ''): string {
  $html = '';
  foreach ($links as $link) {
    if (!empty($link['items'])) {
      $class = 'btn dropdown-toggle mt-1 '.(!empty($link['class']) ? ' '.$link['class'] : '').(!empty($link['url']) && $link['url'] === $activeLink ? ' active' : '');
      $html .= '<div class="dropdown submenuDropdown">'.END_LINE;
      $html .= '  <button class="'.$class.'" type="button" data-bs-toggle="dropdown" aria-expanded="false">'.$link['value'].'</button>'.END_LINE;
      $html .= '  <ul class="dropdown-menu">'.END_LINE;
      foreach ($link['items'] as $linkItem) {
        if (empty($linkItem['class'])) {
          $linkItem['class'] = '';
        }
        if (strpos($linkItem['class'], 'dropdown-item') === false) {
          $linkItem['class'] = trim($linkItem['class'].' dropdown-item');
        }
        $html .= '    <li>'.submenuLinkHtml($linkItem, $activeLink).'</li>'.END_LINE;
      }
      $html .= '   </ul>'.END_LINE;
      $html .= '</div>'.END_LINE;
    } else {
      $html .= submenuLinkHtml($link, $activeLink);
    }
  }

  return $html;
}

/**
 * @param array{
 *   id?: string,
 *   url?: string,
 *   value: string,
 *   class?: string,
 *   title?: string,
 *   onclick?: string,
 *   data?: array<string, string>,
 *   items?: array<int, array{id?: string, url?: string, value: string, class?: string, title?: string, onclick?: string, data?: array<string, string>}>
 * } $link
 */
function submenuLinkHtml(array $link, string $activeLink = ''): string {
  $id = !empty($link['id']) ? ' id="'.$link['id'].'"' : '';
  $class = 'btn me-1 mt-1'.(!empty($link['class']) ? ' '.$link['class'] : '').(!empty($link['url']) && $link['url'] === $activeLink ? ' active' : '');
  $title = !empty($link['title']) ? ' title="'.$link['title'].'"' : '';
  $onclick = !empty($link['onclick']) ? ' onclick="'.$link['onclick'].'"' : '';
  $url = !empty($link['url']) && $link['url'] !== '#' ? getCorrectUrl($link['url']) : '#';
  $data = '';
  if (!empty($link['data'])) {
    foreach ($link['data'] as $key => $value) {
      $data .= ' data-'.$key.'="'.$value.'"';
    }
  }
  return '<a'.$id.' class="'.$class.'" href="'.$url.'"'.$title.$onclick.$data.'>'.$link['value'].'</a>';
}

function htmlError(string $error, string $title = 'Log Viewer', int $httpResponseCode = 400): never {
  if (!defined('END_LINE')) {
    define('END_LINE', "\r\n");
  }

  if (!empty($httpResponseCode)) {
    http_response_code($httpResponseCode);
  }

  $html = pageHeader($title, 'error');
  $html .= '<div class="alert alert-danger text-center mt-5">'.$error.'</div>'.END_LINE;
  $html .= pageFooter();
  die($html);
}

/**
 * @param array<int, array{
 *   type: string,
 *   handle: string,
 *   class: string,
 *   bsIcon: string,
 *   url: string,
 *   lastModified: int|false,
 *   fileSize: int|false,
 *   count: int|false,
 *   permissionsReal: string|false
 * }> $logsInfo
 */
function errorLogsInfoModal(array $logsInfo): string {
  $showType = count(array_unique(array_column($logsInfo, 'type'))) > 1;

  $html = '<div class="modal" tabindex="-1" id="errorLogsInfoModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">Logs insert errors</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;
  $html .= '        <table id="tableLogsInsertErrors" class="table table-hover align-middle">'.END_LINE;
  $html .= '          <thead>'.END_LINE;
  $html .= '            <tr>'.END_LINE;
  $html .= '              <th>Name</th>'.END_LINE;
  if ($showType) {
    $html .= '              <th>Type</th>'.END_LINE;
  }
  $html .= '              <th>Last Error</th>'.END_LINE;
  $html .= '              <th>Count/Size</th>'.END_LINE;
  $html .= '            </tr>'.END_LINE;
  $html .= '          </thead>'.END_LINE;
  $html .= '          <tbody>'.END_LINE;
  foreach ($logsInfo as $logInfo) {
    $html .= '            <tr id="rowLogsInsertErrors_'.$logInfo['type'].'_'.$logInfo['handle'].'">'.END_LINE;
    $html .= '              <td'.(!empty($logInfo['class']) ? ' class="'.$logInfo['class'].'"' : '').'><i class="bi '.$logInfo['bsIcon'].'"></i> '.underscoresToSpaceCapital($logInfo['handle']).'</td>'.END_LINE;
    if ($showType) {
      $html .= '              <td'.(!empty($logInfo['class']) ? ' class="'.$logInfo['class'].'"' : '').'>'.ucfirst($logInfo['type']).'</td>'.END_LINE;
    }
    $html .= '              <td'.(!empty($logInfo['class']) ? ' class="'.$logInfo['class'].'"' : '').'>'.(is_int($logInfo['lastModified']) ? gmdate('d.m.Y H:i:s', $logInfo['lastModified']) : 'Could not get file modification time').'</td>'.END_LINE;
    $html .= '              <td class="actionsCover '.(!empty($logInfo['class']) ? ' '.$logInfo['class'] : '').'">'.END_LINE;
    $html .= '                <div class="actions">'.END_LINE;
    $html .= '                  <a class="btn btn-outline-primary" href="'.$logInfo['url'].'" title="View insert error logs" target="_blank"><i class="bi bi-eye buttonIcon"></i></a>'.END_LINE;
    $html .= '                  <a class="btn btn-outline-danger" href="#" title="Delete error logs." onclick="errorLogsDelete(\''.$logInfo['type'].'\', \''.$logInfo['handle'].'\'); return false;"><i class="bi bi-trash3 buttonIcon"></i></a>'.END_LINE;
    $html .= '                </div>'.END_LINE;
    if (is_int($logInfo['fileSize'])) {
      $html .= '                <span title="'.number_format($logInfo['fileSize'], 0, '.', ' ').'B">'.sizeUnitsFormat($logInfo['fileSize']).'</span>'.END_LINE;
    }
    if ($logInfo['count'] !== false) {
      $html .= '                <span title="Error count">'.$logInfo['count'].'</span>'.END_LINE;
    }
    if ($logInfo['permissionsReal'] !== false && $logInfo['permissionsReal'] !== '666') {
      $html .= '                <span class="textSmall text-danger" title="Current file permissions. Expected 666.">'.$logInfo['permissionsReal'].'</span>'.END_LINE;
    }
    $html .= '              </td>'.END_LINE;
    $html .= '            </tr>'.END_LINE;
  }
  $html .= '         </tbody>'.END_LINE;
  $html .= '        </table>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;

  return $html;
}

/**
 * @param array{field: string, order: string} $sort
 */
function tableHeaderSortable(string $field, string $name, array $sort, string $url = '', string $class = ''): string {
  $sortNew = array(
    'sort' => array(
      'field' => $field,
      'order' => $sort['field'] === $field && $sort['order'] !== 'desc' ? 'desc' : 'asc'
    )
  );

  $request = $_REQUEST;
  if (isset($request['page']) && $request['page'] === 'dashboard') {
    unset($request['page']);
  }

  $html = '<th'.(!empty($class) ? ' class="'.$class.'"' : '').'>'.END_LINE;
  $html .= '  <a class="sort'.($sort['field'] === $field ? ' '.$sort['order'] : '').'" href="'.getCorrectUrl($url).'?'.http_build_query(array_merge($request, $sortNew)).'">'.$name.'</a>'.END_LINE;
  $html .= '</th>'.END_LINE;
  return $html;
}

function floatingSaveButtonHtml(string $formId): string {
  $html = '<div class="floating-save-button">'.END_LINE;
  $html .= '  <span class="save-button bg-success handler-form-submit" data-title="Save changes">'.END_LINE;
  $html .= '    <i class="bi bi-save"></i>'.END_LINE;
  $html .= '  </span>'.END_LINE;

  $html .= '  <span class="save-spinner bg-success d-none">'.END_LINE;
  $html .= '    <div class="spinner-border" role="status">'.END_LINE;
  $html .= '      <span class="visually-hidden">Loading...</span>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </span>'.END_LINE;


  $html .= '</div>'.END_LINE;
  $html .= '<script type="text/javascript">floatingSaveButtonInit(\''.$formId.'\');</script>'.END_LINE;
  return $html;
}

/**
 * @param string[] $handles
 */
function logsMarkModal(array $handles): string {
  $html = '<div class="modal" tabindex="-1" id="logsMarkModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">Mark logs to be deleted</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;
  $html .= '        <label for="logsMarkDateInput" class="form-label">Date</label>'.END_LINE;
  $html .= '        <input type="date" id="logsMarkDateInput" name="date" value="'.date('Y-m-d', strtotime('sunday last week')).'" class="form-control" />'.END_LINE;
  $html .= '        <div id="logsMarkError" class="alert alert-danger d-none mt-2">Invalid date supplied</div>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-footer">'.END_LINE;
  $html .= '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>'.END_LINE;
  $html .= '        <button type="button" class="btn btn-danger" onclick="logsMarkToBeDeleted('.htmlspecialchars((string)json_encode($handles), ENT_QUOTES).')">Mark older to be deleted</button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;
  return $html;
}

/**
 * @param array<int, array{
 *   _id: string,
 *   handle: string,
 *   priority: int|null,
 *   name: string,
 *   count: int,
 *   logToDeleteCount: int,
 *   logLastTimestamp: int|bool,
 *   class: string,
 *   userCanDelete: bool,
 *   userCanMark: bool
 * }> $projectsList
 */
function projectLogsGraphModal(array $projectsList): string {
  $html = '<div class="modal" tabindex="-1" id="projectLogsGraphModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">Projects Logs Graphs</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;
  $html .= '        <p>Select at least two Projects to show graph for Logs distribution between Projects</p>'.END_LINE;
  $html .= '        <select id="logsGraphSelect" class="form-select" size="'.min(15, count($projectsList)).'" multiple>'.END_LINE;
  foreach ($projectsList as $project) {
    $html .= '          <option value="'.$project['handle'].'">'.$project['name'].'</option>'.END_LINE;
  }
  $html .= '        </select>'.END_LINE;
  $html .= '        <div id="projectLogsGraphError" class="alert alert-danger d-none mt-2">You have to select at least two Projects to create graph.</div>'.END_LINE;

  $database = getDatabase();
  /** @var array<int, array{_id: string, active: int, favourite: int, handle: string, settings: array{name: string, project_handles: string[]}}> $projectLogsGraphs */
  $projectLogsGraphs = cursorToArray($database->log_viewer_settings->find(['handle' => 'projectLogsGraph', 'active' => 1]));
  $projectHandlesAvailable = array_column($projectsList, 'handle');
  $projectLogsGraphs = array_filter($projectLogsGraphs, fn ($graph) => !empty(array_intersect($graph['settings']['project_handles'], $projectHandlesAvailable)));
  if (!empty($projectLogsGraphs)) {
    $html .= '        <div class="mt-2">'.END_LINE;
    $html .= '          <h5>Saved Charts</h5>'.END_LINE;
    $html .= '          <div class="list-group list-group-flush">'.END_LINE;
    foreach ($projectLogsGraphs as $graph) {
      $projectsForGraph = array_values(array_filter($projectsList, function ($project) use ($graph) { return in_array($project['handle'], $graph['settings']['project_handles']); }));
      $projectsForGraphString = json_encode($projectsForGraph);
      $graphString = json_encode($graph);
      if (is_string($projectsForGraphString) && is_string($graphString)) {
        $html .= '            <a id="graph_link_'.$graph['_id'].'" href="#" class="list-group-item list-group-item-action" onclick="projectsGraphCreate('.htmlspecialchars($projectsForGraphString, ENT_QUOTES).', '.htmlspecialchars($graphString, ENT_QUOTES).'); bootstrap.Modal.getOrCreateInstance(\'#projectLogsGraphModal\').hide(); return false;">'.$graph['settings']['name'].'</a>'.END_LINE;
      }
    }
    $html .= '          </div>'.END_LINE;
    $html .= '        </div>'.END_LINE;
  }

  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-footer">'.END_LINE;
  $html .= '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>'.END_LINE;
  $html .= '        <button type="button" class="btn btn-primary" onclick="projectsGraphShow()">Create Graph</button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;
  return $html;
}

function projectLogsGraphSaveModal(): string {
  $html = '<div class="modal" tabindex="-1" id="projectLogsGraphSaveModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">Projects Logs Graph save</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;
  $html .= '        <label for="graphNameInput" class="form-label">Graph name</label>'.END_LINE;
  $html .= '        <input id="graphNameInput" class="form-control">'.END_LINE;
  $html .= '        <div id="projectLogsGraphSaveError" class="alert alert-danger d-none mt-2">Invalid Graph name supplied.</div>'.END_LINE;
  $html .= '        <input id="graphNameHandles" type="hidden">'.END_LINE;
  $html .= '        <input id="graphWrapperId" type="hidden">'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-footer">'.END_LINE;
  $html .= '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>'.END_LINE;
  $html .= '        <button type="button" class="btn btn-success" onclick="projectLogsGraphSave()">Save Graph</button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;
  return $html;
}

/**
 * @param array<int, array{
 *   _id: string,
 *   handle: string,
 *   priority: int|null,
 *   name: string,
 *   count: int,
 *   logToDeleteCount: int,
 *   logLastTimestamp: int|bool,
 *   class: string,
 *   userCanDelete: bool,
 *   userCanMark: bool
 * }> $projectListData
 * @param array<array{
 *   _id: string,
 *   active: int,
 *   favourite: int,
 *   handle: string,
 *   settings: array{
 *     name?: string,
 *     project_handles?: string[],
 *     project_handle?: string
 *   }
 * }> $pieGraphSettings
 */
function projectListPieGraphsHtml(array $projectListData, array $pieGraphSettings = []): string {
  $html = '<div id="projectLogsGraph">'.END_LINE;

  if (!empty($pieGraphSettings)) {
    $html .= '<script>'.END_LINE;
    foreach ($pieGraphSettings as $graphSettings) {
      if (!empty($graphSettings['settings']['project_handles'])) {
        $projectsForGraph = array_values(array_filter($projectListData, function ($project) use ($graphSettings) { return in_array($project['handle'], $graphSettings['settings']['project_handles']); }));
        $html .= '  projectsGraphCreate('.json_encode($projectsForGraph).', '.json_encode($graphSettings).');'.END_LINE;
      }
    }
    $html .= '</script>'.END_LINE;
  }

  $html .= '</div>'.END_LINE;
  return $html;
}

/**
 * @param array<int, array{
 *   _id: string,
 *   handle: string,
 *   priority: int|null,
 *   name: string,
 *   count: int,
 *   logToDeleteCount: int,
 *   logLastTimestamp: int|bool,
 *   class: string,
 *   userCanDelete: bool,
 *   userCanMark: bool
 * }> $projectListData
 * @param array{field: string, order: string} $sort
 * @param array<string, array{
 *   _id: string,
 *   active: int,
 *   favourite: int,
 *   handle: string,
 *   settings: array{
 *     name?: string,
 *     project_handles?: string[],
 *     project_handle?: string
 *   }
 * }> $rowGraphSettings
 * @param array<int, array{
 *   _id: string,
 *   project_handle: string,
 *   handle: string,
 *   key: string,
 *   active: int,
 *   favourite: int,
 *   settings: array<string, array<string, mixed>>
 * }> $projectLogsFilters
 */
function projectListTableHtml(array $projectListData, array $sort, array $rowGraphSettings = [], array $projectLogsFilters = []): string {
  $html = '<table class="table table-hover align-middle">'.END_LINE;
  $html .= '  <thead class="table-sortable-header">'.END_LINE;
  $html .= '    <tr>'.END_LINE;
  $html .= tableHeaderSortable('priority', 'Priority', $sort);
  $html .= tableHeaderSortable('name', 'Name', $sort);
  $html .= tableHeaderSortable('logLastTimestamp', 'Last', $sort);
  $html .= tableHeaderSortable('count', 'Count', $sort);
  $html .= '      <th class="logsGraph d-none">Graphs</th>'.END_LINE;
  $html .= '    </tr>'.END_LINE;
  $html .= '  </thead>'.END_LINE;
  $html .= '  <tbody>'.END_LINE;
  if (!empty($projectListData)) {
    foreach ($projectListData as $logsInfo) {
      $html .= '    <tr id="row_'.$logsInfo['handle'].'">'.END_LINE;
      $html .= '      <td'.(!empty($logsInfo['class']) ? ' class="'.$logsInfo['class'].'"' : '').'>'.(!is_null($logsInfo['priority']) ? intval($logsInfo['priority']) : '').'</td>'.END_LINE;
      $html .= '      <td class="actionsCover '.(!empty($logsInfo['class']) ? ' '.$logsInfo['class'] : '').'">'.END_LINE;
      if (permissionsCheckProject($logsInfo['_id'], ['admin'])) {
        $html .= '        <div class="actions">'.END_LINE;
        $html .= '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('projects/'.$logsInfo['_id'].'/edit').'" title="Edit Project"><i class="bi bi-pencil buttonIcon"></i></a>'.END_LINE;
        $html .= '        </div>'.END_LINE;
      }
      $html .= htmlspecialchars($logsInfo['name']);
      $html .= '      </td>'.END_LINE;
      $html .= '      <td'.(!empty($logsInfo['class']) ? ' class="'.$logsInfo['class'].'"' : '').'>'.(is_int($logsInfo['logLastTimestamp']) ? gmdate('Y-m-d H:i:s', $logsInfo['logLastTimestamp']) : '').'</td>'.END_LINE;
      $html .= '      <td class="actionsCover '.(!empty($logsInfo['class']) ? ' '.$logsInfo['class'] : '').'">'.END_LINE;
      $html .= '        <div class="actions">'.END_LINE;
      $html .= '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('projects/'.$logsInfo['_id'].'/logs').'" title="Show Logs"><i class="bi bi-list-columns buttonIcon"></i></a>'.END_LINE;
      $html .= '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('projects/'.$logsInfo['_id'].'/logs?limit=1000').'" title="Show last 1000 Logs"><i class="bi bi-funnel buttonIcon"></i></a>'.END_LINE;
      $html .= '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('api/logs/load?project_handle='.$logsInfo['handle'].'&limit=1000').'" title="Show raw data for last 1000 Logs"><i class="bi bi-filetype-raw buttonIcon"></i></a>'.END_LINE;
      $projectFilters = array_filter($projectLogsFilters, function ($filter) use ($logsInfo) { return $filter['project_handle'] === $logsInfo['handle']; });
      foreach ($projectFilters as $logsFilter) {
        $html .= '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('projects/'.$logsInfo['_id'].'/logs?logs_filter_id='.$logsFilter['_id']).'" title="Show Logs with filter '.$logsFilter['key'].'"><i class="bi bi-search buttonIcon"></i></a>'.END_LINE;
      }
      if (empty($rowGraphSettings[$logsInfo['handle']])) {
        $html .= '          <a class="btn btn-outline-secondary buttonShowGraph" href="#" title="Show graph for number of new Logs per day" onclick="logsRowGraphShow(\''.$logsInfo['handle'].'\'); this.remove(); return false;"><i class="bi bi-graph-up-arrow buttonIcon"></i></a>'.END_LINE;
      }
      if (!empty($logsInfo['logToDeleteCount']) && !empty($logsInfo['userCanDelete'])) {
        $html .= '          <a id="button_'.htmlspecialchars($logsInfo['handle'], ENT_QUOTES).'_logs_delete" class="btn btn-outline-danger logs-delete" title="Delete '.$logsInfo['logToDeleteCount'].' marked logs." onclick="logMarkedDelete(\''.htmlspecialchars($logsInfo['handle'], ENT_QUOTES).'\',  \''.htmlspecialchars($logsInfo['name'], ENT_QUOTES).'\', \''.getCorrectUrl('').'\'); return false"><i class="bi bi-trash3 buttonIcon"></i></a>'.END_LINE;
      }
      $html .= '        </div>'.END_LINE;
      $html .= $logsInfo['count'] - $logsInfo['logToDeleteCount'];
      $html .= '      </td>'.END_LINE;
      if (!empty($rowGraphSettings[$logsInfo['handle']])) {
        $html .= '      <td class="logsGraph actionsCover">'.END_LINE;
        $html .= '        <script>logsRowGraphShow(\''.$logsInfo['handle'].'\', '.json_encode($rowGraphSettings[$logsInfo['handle']]).');</script>'.END_LINE;
        $html .= '      </td>'.END_LINE;
      } else {
        $html .= '      <td class="logsGraph actionsCover d-none"></td>'.END_LINE;
      }
      $html .= '    </tr>'.END_LINE;
    }
  } else {
    $html .= '    <tr>'.END_LINE;
    $html .= '      <td colspan="2">There are no projects.</td>'.END_LINE;
    $html .= '    </tr>'.END_LINE;
  }
  $html .= '  </tbody>'.END_LINE;
  $html .= '</table>'.END_LINE;
  return $html;
}

function apiKeyCreateModal(string $userId, bool $columnActiveShow): string {
  $html = '<div class="modal" tabindex="-1" id="apiKeyCreateModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">API Key create</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;
  $html .= '        <label for="apiKeyNameInput" class="form-label">API Key name</label>'.END_LINE;
  $html .= '        <input id="apiKeyNameInput" class="form-control">'.END_LINE;
  $html .= '        <div id="apiKeyCreateError" class="alert alert-danger d-none mt-2">Invalid API Key name supplied.</div>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-footer">'.END_LINE;
  $html .= '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>'.END_LINE;
  $html .= '        <button type="button" class="btn btn-success" onclick="apiKeyCreate(\''.htmlspecialchars($userId, ENT_QUOTES).'\', '.$columnActiveShow.')">Create</button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;
  return $html;
}

/**
 * @param array<int|string, string|array{value: string|int|float, name: string}> $options
 * @param array<string, string> $selectAttributes
 * @param array<string, mixed> $selectOptions
 */
function selectHtml(string $name, array $options, string|int|float $selected = null, array $selectAttributes = [], array $selectOptions = []): string {
  $selectAttributes['class'] = (!empty($selectAttributes['class']) ? $selectAttributes['class'].' ' : '').'form-select';
  array_walk($selectAttributes, fn (&$value, $attribute) => $value = $attribute.'="'.$value.'"');

  $html = '<select name="'.$name.'" '.implode(' ', $selectAttributes).'>'.END_LINE;
  if (!empty($selectOptions['zero_select'])) {
    $html .= '<option>'.$selectOptions['zero_select'].'</option>'.END_LINE;
  }
  foreach ($options as $key => $option) {
    $keyValue = !empty($selectOptions['key_value']) ? $selectOptions['key_value'] : 'value';
    if ($keyValue === '__value') {
      if (is_array($option)) {
        trigger_error('Key value __value cannot be used if option is array. Select: '.$name.', option: '.print_r($option, true), E_USER_WARNING);
        $value = $option['value'];
      } else {
        $value = $option;
      }
    } elseif ($keyValue === '__key') {
      $value = (string)$key;
    } else {
      $value = (string)$option[$keyValue];
    }

    $keyName = !empty($selectOptions['key_name']) ? $selectOptions['key_name'] : 'name';
    if ($keyName === '__value') {
      if (is_array($option)) {
        trigger_error('Key name __value cannot be used if option is array. Select: '.$name.', option: '.print_r($option, true), E_USER_WARNING);
        $name = $option['name'];
      } else {
        $name = $option;
      }
    } elseif ($keyName === '__key') {
      $name = (string)$key;
    } else {
      $name = (string)$option[$keyName];
    }

    $html .= '  <option value="'.$value.'"'.($value === $selected ? ' selected="selected"' : '').'>'.$name.'</option>'.END_LINE;
  }
  $html .= '</select>'.END_LINE;
  return $html;
}

/**
 * @param array<int, array{
 *   _id: string,
 *   handle: string,
 *   priority: int|null,
 *   name: string,
 *   count: int,
 *   logToDeleteCount: int,
 *   logLastTimestamp: int|bool,
 *   class: string,
 *   userCanDelete: bool,
 *   userCanMark: bool
 * }> $projectsList
 */
function projectLogsSearchModal(array $projectsList): string {
  $fields = [];
  $database = getDatabase();

  $logsCollections = array_map(fn ($project) => 'logs_'.$project['_id'], $projectsList);
  $collectionsInfo = $database->listCollections(['filter' => ['name' => ['$in' => $logsCollections]]]);
  $collectionsInfo->rewind();
  foreach ($collectionsInfo as $collectionInfo) {
    $collectionOptions = $collectionInfo->getOptions();
    if (!empty($collectionOptions) && !empty($collectionOptions['validator'])) {
      foreach ($collectionOptions['validator']['$jsonSchema']['properties'] as $key => $value) {
        if (isset($fields[$key])) {
          $fields[$key]['count']++;
        } else {
          $fields[$key] = ['name' => $key, 'type' => $value['bsonType'], 'count' => 1];
        }
      }
    }
  }

  ksort($fields, SORT_STRING | SORT_FLAG_CASE);

  $projectHandlesString = json_encode(array_column($projectsList, 'handle'));
  /** @var array<string, array{name: string, type: string, count: int}> $fields */
  $search = requestFilterSettingsParse($fields, 'contains', 'search');

  $html = '<div class="modal" tabindex="-1" id="projectLogsSearchModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">Project Logs Search</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;

  $html .= '        <form id="formProjectLogsSearch" class="filter" onsubmit="projectsLogsSearch('.htmlspecialchars((string)$projectHandlesString, ENT_QUOTES).'); return false;">'.END_LINE;
  $html .= '          <ul>'.END_LINE;
  foreach ($fields as $key => $settings) {
    $filterType = !empty($search[$key]) ? $search[$key]['filter'] : 'contains';
    $filterNegative = !empty($search[$key]) ? $search[$key]['negative'] : 0;
    $filterValue = isset($search[$key]) ? $search[$key]['value'] : '';

    $html .= '             <li>'.END_LINE;
    $html .= '               <div class="filter-item">'.END_LINE;
    $html .= '                 <div class="hover-items">'.END_LINE;
    if (in_array($settings['type'], ['int', 'double'])) {
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'equals' ? ' bg-secondary' : '').'" href="#" title="Match value exactly" onclick="searchFilterNumberChangeHandle(this); return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'equals\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="icon-equals"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'less' ? ' bg-secondary' : '').'" href="#" title="Filter values that are less then" onclick="searchFilterNumberChangeHandle(this); return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'less\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="icon-less"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'between' ? ' bg-secondary' : '').'" href="#" title="Filter values between" onclick="searchFilterNumberChangeHandle(this, true); return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'between\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="icon-between"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'greater' ? ' bg-secondary' : '').'" href="#" title="Filter values that are more then" onclick="searchFilterNumberChangeHandle(this); return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'greater\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="icon-more"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
    } else {
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'equals' ? ' bg-secondary' : '').'" href="#" title="Match value exactly" onclick="return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'equals\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="icon-equals"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'starts' ? ' bg-secondary' : '').'" href="#" title="Filter values that start with" onclick="return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'starts\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="bi bi-align-start"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'contains' ? ' bg-secondary' : '').'" href="#" title="Filter values that contain" onclick="return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'contains\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="bi bi-align-center"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'ends' ? ' bg-secondary' : '').'" href="#" title="Filter values that end with" onclick="return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'ends\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="bi bi-align-end"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
      $html .= '                   <a class="btn btn-outline-secondary filterTypeButton'.($filterType === 'regex' ? ' bg-secondary' : '').'" href="#" title="Use value as regex" onclick="return searchFilterClickedHandle(this, \''.$key.'\', \'filter\', \'regex\', \'.filterTypeButton.bg-secondary\');">'.END_LINE;
      $html .= '                     <i class="bi bi-regex"></i>'.END_LINE;
      $html .= '                   </a>'.END_LINE;
    }
    $html .= '                   <a class="btn btn-outline-secondary filterNegativeButton'.($filterNegative === 0 ? ' bg-secondary' : '').'" href="#" title="Filter items matching the query" onclick="return searchFilterClickedHandle(this, \''.$key.'\', \'negative\', 0, \'.filterNegativeButton.bg-secondary\');">'.END_LINE;
    $html .= '                     <i class="bi bi-check-lg"></i>'.END_LINE;
    $html .= '                   </a>'.END_LINE;
    $html .= '                   <a class="btn btn-outline-secondary filterNegativeButton'.($filterNegative === 1 ? ' bg-secondary' : '').'" href="#" title="Filter items not matching the query" onclick="return searchFilterClickedHandle(this, \''.$key.'\', \'negative\', 1, \'.filterNegativeButton.bg-secondary\');">'.END_LINE;
    $html .= '                     <i class="bi bi-ban"></i>'.END_LINE;
    $html .= '                   </a>'.END_LINE;
    $html .= '                 </div>'.END_LINE;
    $html .= '                 <label for="input_'.$key.'" class="filter-item-title help" title="Field is in '.$settings['count'].' projects."><strong>'.$key.'</strong> ('.$settings['count'].')</label>'.END_LINE;
    if (in_array($settings['type'], ['int', 'double'])) {
      $isArray = is_array($filterValue);
      $html .= '                 <span class="filterNumberDefault'.($isArray ? ' d-none' : '').'">'.END_LINE;
      $html .= '                   <input id="input_'.$key.'" name="['.$key.'][value]" value="'.(!$isArray ? $filterValue : '').'" class="form-control" type="number"'.($isArray ? ' disabled' : '').' />'.END_LINE;
      $html .= '                 </span>'.END_LINE;
      $html .= '                 <span class="filterNumberBetween numberBetweenWrapper'.(!$isArray ? ' d-none' : '').'">'.END_LINE;
      $html .= '                   <label :for="input_'.$key.'InputMin" class="filter-item-title">From:</label>'.END_LINE;
      $html .= '                   <input id="input_'.$key.'InputMin" name="['.$key.'][value][min]" value="'.($isArray && isset($filterValue['min']) ? $filterValue['min'] : '').'" class="form-control" type="number"'.(!$isArray ? ' disabled' : '').' />'.END_LINE;
      $html .= '                   <label :for="input_'.$key.'InputMax" class="filter-item-title">To:</label>'.END_LINE;
      $html .= '                   <input id="input_'.$key.'InputMax" name="['.$key.'][value][max]" value="'.($isArray && isset($filterValue['max']) ? $filterValue['max'] : '').'" class="form-control" type="number"'.(!$isArray ? ' disabled' : '').' />'.END_LINE;
      $html .= '                 </span>'.END_LINE;
    } else {
      $html .= '                 <input id="input_'.$key.'" name="['.$key.'][value]" value="'.(!is_array($filterValue) ? $filterValue : '').'" class="form-control" />'.END_LINE;
    }
    $html .= '                 <input name="['.$key.'][filter]" value="'.$filterType.'" type="hidden" />'.END_LINE;
    $html .= '                 <input name="['.$key.'][type]" value="'.$settings['type'].'" type="hidden" />'.END_LINE;
    $html .= '                 <input name="['.$key.'][negative]" value="'.$filterNegative.'" type="hidden" />'.END_LINE;
    $html .= '               </div>'.END_LINE;
    $html .= '             </li>'.END_LINE;
  }
  $html .= '          </ul>'.END_LINE;
  $html .= '          <input type="submit" value="Search" class="btn btn-primary float-end" />'.END_LINE;
  $html .= '          <button type="button" class="btn btn-primary float-end me-2" onclick="projectsLogsSearchCopy()">Copy search URL</button>'.END_LINE;
  $html .= '        </form>'.END_LINE;
  $html .= '        <table id="projectLogsSearchResult" class="table table-hover align-middle d-none">'.END_LINE;
  $html .= '          <thead>'.END_LINE;
  $html .= '            <tr>'.END_LINE;
  $html .= '              <th>Project</th>'.END_LINE;
  $html .= '              <th>Last</th>'.END_LINE;
  $html .= '              <th>Count</th>'.END_LINE;
  $html .= '            </tr>'.END_LINE;
  $html .= '          </thead>'.END_LINE;
  $html .= '          <tbody>'.END_LINE;
  $html .= '          </tbody>'.END_LINE;
  $html .= '        </table>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;

  if (!empty($search)) {
    $html .= '<script>'.END_LINE;
    $html .= '  bootstrap.Modal.getOrCreateInstance(\'#projectLogsSearchModal\').show();'.END_LINE;
    $html .= '  projectsLogsSearch('.$projectHandlesString.');'.END_LINE;
    $html .= '</script>'.END_LINE;
  }

  return $html;
}

function fieldsImportModal(): string {
  $html = '<div class="modal" tabindex="-1" id="fieldsImportModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">Import Fields</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;
  $html .= '        <input id="fieldsImportInput" class="form-control" type="file" id="formFile">'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-footer">'.END_LINE;
  $html .= '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>'.END_LINE;
  $html .= '        <button type="button" class="btn btn-primary" onclick="projectFieldsImport()">Import</button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;
  return $html;
}

/**
 * @param array<int, array{_id: string, name: string, active: int, project_ids: string[]}> $dashboards
 */
function projectDashboardAssignmentModal(array $dashboards, string $projectId): string {
  $html = '<div class="modal" tabindex="-1" id="projectDashboardAssignmentModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">Dashboard assignment</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;

  $html .= '      <select id="selectDashboardIds" class="form-select" name="dashboard_ids[]" size="10" multiple>'.END_LINE;
  foreach ($dashboards as $dashboard) {
    $html .= '        <option value="'.$dashboard['_id'].'"'.(in_array($projectId, $dashboard['project_ids']) ? ' selected="selected"' : '').'>'.$dashboard['name'].'</option>'.END_LINE;
  }
  $html .= '      </select>'.END_LINE;

  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-footer">'.END_LINE;
  $html .= '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>'.END_LINE;
  $html .= '        <button type="button" class="btn btn-primary" onclick="projectDashboardAssignmentSave(\''.$projectId.'\')">Save</button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;
  return $html;
}

/**
 * @param array<int, array{
 *   _id: string,
 *   forename: string,
 *   surname: string,
 *   active: int,
 *   type: string,
 *   permissions: array{dashboards: string[], projects: array<string, string>, additional: string[]}
 * }> $users
 */
function projectUserPermissionsModal(array $users, string $projectId): string {
  $html = '<div class="modal" tabindex="-1" id="projectUserPermissionsModal">'.END_LINE;
  $html .= '  <div class="modal-dialog">'.END_LINE;
  $html .= '    <div class="modal-content">'.END_LINE;
  $html .= '      <div class="modal-header">'.END_LINE;
  $html .= '        <h5 class="modal-title">Projects permissions</h5>'.END_LINE;
  $html .= '        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-body">'.END_LINE;

  foreach ($users as $user) {
    if ($user['type'] === 'superuser') {
      continue;
    }
    $projectPermission = !empty($user['permissions']['projects'][$projectId]) ? $user['permissions']['projects'][$projectId] : '';
    $html .= '        <div class="mb-3 row hoverBackground">'.END_LINE;
    $html .= '          <label class="col col-form-label">'.$user['forename'].' '.$user['surname'].'</label>'.END_LINE;
    $html .= '          <div class="col text-end">'.END_LINE;
    $html .= '            <div class="btn-group" role="group" aria-label="Project permissions select">'.END_LINE;
    $html .= '              <input type="radio" class="btn-check" name="'.$user['_id'].'" value="admin" id="checkboxPermissionsAdmin_'.$user['_id'].'" autocomplete="off"'.($projectPermission === 'admin' ? ' checked' : '').'>'.END_LINE;
    $html .= '              <label class="btn btn-outline-primary" for="checkboxPermissionsAdmin_'.$user['_id'].'" title="User can view, clear and delete logs. Additionally they can edit the project settings.">Admin</label>'.END_LINE;
    $html .= '              <input type="radio" class="btn-check" name="'.$user['_id'].'" value="maintainer" id="checkboxPermissionsMaintainer_'.$user['_id'].'" autocomplete="off"'.($projectPermission === 'maintainer' ? ' checked' : '').'>'.END_LINE;
    $html .= '              <label class="btn btn-outline-primary" for="checkboxPermissionsMaintainer_'.$user['_id'].'" title="User can view and clear logs.">Maintainer</label>'.END_LINE;
    $html .= '              <input type="radio" class="btn-check" name="'.$user['_id'].'" value="standard" id="checkboxPermissionsStandard_'.$user['_id'].'" autocomplete="off"'.($projectPermission === 'standard' ? ' checked' : '').'>'.END_LINE;
    $html .= '              <label class="btn btn-outline-primary" for="checkboxPermissionsStandard_'.$user['_id'].'" title="User can view logs.">Standard</label>'.END_LINE;
    $html .= '              <input type="radio" class="btn-check" name="'.$user['_id'].'" value="none" id="checkboxPermissionsNone_'.$user['_id'].'" autocomplete="off"'.(empty($projectPermission) ? ' checked' : '').'>'.END_LINE;
    $html .= '              <label class="btn btn-outline-primary" for="checkboxPermissionsNone_'.$user['_id'].'" title="No access.">None</label>'.END_LINE;
    $html .= '            </div>'.END_LINE;
    $html .= '          </div>'.END_LINE;
    $html .= '        </div>'.END_LINE;
  }

  $html .= '      </div>'.END_LINE;
  $html .= '      <div class="modal-footer">'.END_LINE;
  $html .= '        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>'.END_LINE;
  $html .= '        <button type="button" class="btn btn-primary" onclick="projectUserPermissionsSave(\''.$projectId.'\')">Save</button>'.END_LINE;
  $html .= '      </div>'.END_LINE;
  $html .= '    </div>'.END_LINE;
  $html .= '  </div>'.END_LINE;
  $html .= '</div>'.END_LINE;
  return $html;
}
