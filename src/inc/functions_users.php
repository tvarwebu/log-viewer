<?php

/**
 * @param array<string, mixed> $post
 * @return array<string, string> - array of errors
 */
function usersCollectionInit(array $post): array {
  if (collectionExists('users')) {
    return ['global' => 'Users Collection already exists.'];
  }
  $userPost = array(
    'type' => 'superuser',
    'active' => 1,
    'permissions' => array(
      'projects' => [],
      'dashboards' => [],
      'additional' => [],
    )
  );

  $keysValidate = ['forename', 'surname', 'username', 'password'];
  foreach ($keysValidate as $key) {
    $value = !empty($post[$key]) && is_string($post[$key]) ? $post[$key] : '';
    if ($key === 'password') {
      $error = passwordValidate($value);
      if (!empty($error)) {
        return [$key => $error];
      }
    } else {
      $value = sanitizeStringInput($value);
    }
    if (empty($value)) {
      return [$key => 'Missing '.ucfirst($key).'.'];
    }
    $userPost[$key] = $value;
  }

  if (empty($post['passwordCheck']) || !is_string($post['passwordCheck'])) {
    return ['passwordCheck' => 'Missing passwords confirmation.'];
  }
  if ($userPost['password'] !== $post['passwordCheck']) {
    return ['passwordCheck' => 'Passwords do not match.'];
  }

  $userPost['password'] = password_hash($userPost['password'], PASSWORD_DEFAULT);

  $validator = array(
    '$jsonSchema' => array(
      'bsonType' => 'object',
      'title' => 'Users Object Validation',
      'required' => ['forename', 'surname', 'username', 'password', 'type', 'active', 'permissions'],
      'properties' => array(
        'forename' => ['bsonType' => 'string'],
        'surname' => ['bsonType' => 'string'],
        'username' => ['bsonType' => 'string'],
        'password' => ['bsonType' => 'string'],
        'type' => ['enum' => ['standard', 'superuser']],
        'active' => ['bsonType' => 'int'],
        'login_hash' => ['bsonType' => 'string'],
        'permissions' => ['bsonType' => 'object'],
      ),
    )
  );

  $database = getDatabase();
  $resultCollectionCreate = $database->createCollection('users', ['validator' => $validator]);
  if (empty($resultCollectionCreate['ok'])) {
    return ['global' => 'Could not create Users Collection.'];
  }

  $insertOneResult = $database->users->insertOne($userPost);
  if ($insertOneResult->getInsertedCount() === 0) {
    return ['global' => 'Could not create user.'];
  }

  $userPost['_id'] = (string)$insertOneResult->getInsertedId();
  unset($userPost['password']);
  $_SESSION['LVuser'] = $userPost;
  return [];
}

function isUser(): bool {
  return !empty($_SESSION['LVuser']);
}

function isSuperuser(): bool {
  return !empty($_SESSION['LVuser']['type']) && $_SESSION['LVuser']['type'] === 'superuser';
}

function userOrTokenOrDie(string $logfile): bool {
  if (!isUser()) {
    if (!empty($_COOKIE['loginHash'])) {
      $errors = sessionLogin($_COOKIE['loginHash']);
      if (!empty($errors)) {
        apiErrorResponse($errors['global'], $logfile, $errors['global'], 403);
      }
      return true;
    }

    try {
      tokenLoginOrException();
    } catch (\Exception $ex) {
      apiErrorResponse($ex->getMessage(), $logfile, $ex->getMessage(), !empty($ex->getCode()) ? $ex->getCode() : 403);
    }
  }
  return true;
}

function sessionRefresh(): void {
  if (!isUser()) {
    return;
  }
  $database = getDatabase();
  $user = $database->users->findOne(['_id' => new MongoDB\BSON\ObjectID($_SESSION['LVuser']['_id'])]);
  if (empty($user)) {
    sessionDestroyCookieUnset();
    throw new Exception('User in session does not exist.');
  }
  if (empty($user['active'])) {
    sessionDestroyCookieUnset();
    throw new Exception('Your account was deactivated.');
  }

  $userArray = bsonDocumentToArray($user);
  unset($userArray['password'], $userArray['login_hash']);
  $_SESSION['LVuser'] = $userArray;
}

function passwordValidate(string $password): string {
  if (strlen($password) < 6) {
    return 'Password is too short, it must be at least 6 characters ';
  }
  if (!preg_match('/[a-zA-Z]/', $password) || !preg_match('/[0-9]/', $password)) {
    return 'Password must contain a mix of letters and numbers.';
  }
  if (strlen($password) > 100) {
    return 'Password has to have less then 100 characters';
  }
  if (preg_match('#([\"\\\']+)#', $password, $regs)) {
    return 'Password contain characters that is not allowed';
  }
  return '';
}

function userKeyTokenGenerate(): string {
  $database = getDatabase();

  for ($i = 0; $i < 10; $i++) {
    $token = base64_encode(random_bytes(64));
    if (empty($database->keys->findOne(['token' => $token]))) {
      return $token;
    }
  }
  return '';
}
