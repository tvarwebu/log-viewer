<?php

$database = getDatabaseOrDie();
$deletedShow = !empty($_REQUEST['deleted_show']);
$users = cursorToArray($database->users->find(!$deletedShow ? ['active' => 1] : []));
$sort = !empty($_REQUEST['sort']) ? $_REQUEST['sort'] : ['field' => 'username', 'order' => 'asc'];
if (!empty($users)) {
  usort($users, arraySort($sort));
}

$links = array(
  ['url' => 'users/new/edit', 'value' => '<i class="bi bi-plus buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Add User'],
  ['url' => 'users?deleted_show=1', 'value' => 'Show Deleted', 'class' => 'btn-outline-secondary', 'title' => 'Show Deleted'],
);
echo pageHeader('Users', 'users', $links);

echo '<table class="table table-hover align-middle">'.END_LINE;
echo '  <thead class="table-sortable-header">'.END_LINE;
echo '    <tr>'.END_LINE;
echo tableHeaderSortable('forename', 'Forename', $sort);
echo tableHeaderSortable('surname', 'Surname', $sort);
if ($deletedShow) {
  echo tableHeaderSortable('active', 'Active', $sort);
}
echo tableHeaderSortable('type', 'Type', $sort);
echo '    </tr>'.END_LINE;
echo '  </thead>'.END_LINE;
echo '  <tbody>'.END_LINE;
if (!empty($users)) {
  /** @var array{_id: string, forename: string, surname: string, username: string, password: string, active: int, type: string} $user*/
  foreach ($users as $user) {
    echo '    <tr'.(empty($user['active']) ? ' class="opacity-50"' : '').'>'.END_LINE;
    echo '      <td>'.$user['forename'].'</td>'.END_LINE;
    echo '      <td>'.$user['surname'].'</td>'.END_LINE;
    if ($deletedShow) {
      echo '      <td>'.(!empty($user['active']) ? 'Active' : 'Inactive').'</td>'.END_LINE;
    }
    echo '      <td class="actionsCover">'.END_LINE;
    if ($user['type'] !== 'superuser' || $user['_id'] === $_SESSION['LVuser']['_id']) {
      echo '        <div class="actions">'.END_LINE;
      echo '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('users/'.$user['_id'].'/edit').'" title="Edit User"><i class="bi bi-pencil buttonIcon"></i></a>'.END_LINE;
      echo '        </div>'.END_LINE;
    }
    echo ucfirst($user['type']);
    echo '      </td>'.END_LINE;
    echo '    </tr>'.END_LINE;
  }
} else {
  trigger_error('Someone accessed user list without any users existing: '.var_export($_SESSION), E_USER_WARNING);
  echo '    <tr>'.END_LINE;
  echo '      <td colspan="'.(3 + ($deletedShow ? 1 : 0)).'" class="text-center">There are no users. How did you get here?</td>'.END_LINE;
  echo '    </tr>'.END_LINE;
}
echo '  </tbody>'.END_LINE;
echo '</table>'.END_LINE;

echo pageFooter();
