<?php

$database = getDatabaseOrDie();
$tagId = 'new';
if (!empty($_REQUEST['tag_id'])) {
  $tagId = sanitizeStringInput($_REQUEST['tag_id']);
}

$tag = array(
  'name' => '',
  'active' => 1,
  'active_until' => (new DateTime('now + 14 days'))->getTimestamp(),
  'action' => 'ignore',
  'project_id' => '',
  'rules' => [],
);

if (!empty($_POST)) {
  $tag = array_merge($tag, $_POST);
} elseif ($tagId !== 'new') {
  $tag = $database->tags->findOne(['_id' => new MongoDB\BSON\ObjectID($tagId)]);
  if (empty($tag)) {
    htmlError('Could not find Tag with ID: '.$tagId, 'Tag Edit');
  }
}

$ruleFilterOptions = array(
  ['value' => 'equals', 'name' => 'Equals'],
  ['value' => 'not_equals', 'name' => 'Not Equals'],
  ['value' => 'less', 'name' => 'Less'],
  ['value' => 'greater', 'name' => 'Greater'],
  ['value' => 'contains', 'name' => 'Contains'],
  ['value' => 'not_contains', 'name' => 'Not Contains'],
  ['value' => 'version_compare', 'name' => 'Version Compare'],
);

$projects = $database->projects->find()->toArray();
usort($projects, arraySort(['field' => 'priority', 'order' => 'asc']));

$projectsFields = [];
$collectionsInfo = $database->listCollections(['filter' => ['name' => ['$in' => array_map(fn ($project) => 'logs_'.$project['_id'], $projects)]]]);
$collectionsInfo->rewind();
foreach ($collectionsInfo as $collectionInfo) {
  $projectId = strtr($collectionInfo->getName(), ['logs_' => '']);
  if (!isset($projectsFields[$projectId])) {
    $projectsFields[$projectId] = [];
  }
  $collectionOptions = $collectionInfo->getOptions();
  if (!empty($collectionOptions) && !empty($collectionOptions['validator'])) {
    foreach ($collectionOptions['validator']['$jsonSchema']['properties'] as $key => $value) {
      $projectsFields[$projectId][$key] = $key;
    }
  }
}

foreach (array_keys($projectsFields) as $projectId) {
  ksort($projectsFields[$projectId]);
}

$links = [];
if ($tagId !== 'new') {
  $projectCurrent = null;
  foreach ($projects as $project) {
    if ((string)$project['_id'] === $tag['project_id']) {
      $projectCurrent = $project;
      break;
    }
  }

  $links[] = array(
    'url' => 'projects/'.$tag['project_id'].'/edit',
    'value' => '<i class="bi bi-file-earmark-medical buttonIcon"></i>',
    'class' => 'btn-outline-secondary',
    'title' => 'Edit Project',
  );
  $urlFilter = ['filter' => []];
  foreach ($tag['rules'] as $rule) {
    $ruleFilter = !empty($rule['filter']) ? $rule['filter'] : '';
    if ($ruleFilter === 'contains') {
      $ruleFilter = 'regex';
    }
    $urlFilter['filter'][$rule['field'].(!empty($ruleFilter) ? '__'.$ruleFilter : '')] = $rule['value'];
  }
  $links[] = array(
    'url' => 'projects/'.$tag['project_id'].'/logs?'.http_build_query($urlFilter),
    'value' => '<i class="bi bi-search buttonIcon"></i>',
    'class' => 'btn-outline-secondary',
    'title' => 'Show Logs with filter',
  );
  if (!empty($projectCurrent)) {
    $links[] = array(
      'url' => 'api/logs/load?project_handle='.$projectCurrent['handle'].'&limit=1000&'.http_build_query($urlFilter),
      'value' => '<i class="bi bi-filetype-raw buttonIcon"></i>',
      'class' => 'btn-outline-secondary',
      'title' => 'Show raw data for last 1000 Logs',
    );
  }
  if (!empty($tag['active'])) {
    $links[] = array(
      'url' => 'tags/'.$tagId.'/deactivate',
      'value' => '<i class="bi bi-trash3 buttonIcon"></i>',
      'class' => 'btn-outline-danger',
      'onclick' => 'return confirm(\'Are you sure you want to deactivate '.htmlspecialchars($tag['name'], ENT_QUOTES).'?\')',
      'title' => 'Deactivate Tag',
    );
  }
}

$pageName = '';
if ($tagId !== 'new') {
  $pageName .= $tag['name'];
  if (empty($tag['active'])) {
    $pageName .= ' - inactive';
  }
} else {
  $pageName .= 'New';
}
$pageName .= ' - Tag Edit';

echo pageHeader($pageName, 'tag/'.$tagId.'/edit', $links);

if (!empty($errors['global'])) {
  echo '<div class="alert alert-danger text-center">'.implode('<br />'.END_LINE, $errors['global']).'</div>'.END_LINE;
}

$formClass = !empty($errors) && !empty(array_sum(array_map('count', $errors))) ? 'was-validated' : 'needs-validation';
echo '<form id="formTagEdit" method="post" action="'.getCorrectUrl('tags/'.($tagId !== 'new' ? $tagId : 'new').'/save').'" class="'.$formClass.'" novalidate>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="inputName" class="col-sm-2 col-form-label">Name</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="inputName" name="name" class="form-control'.(!empty($errors['tag']['name']) ? ' is-invalid' : '').'" value="'.htmlspecialchars($tag['name'], ENT_QUOTES).'" required />'.END_LINE;
if (!empty($errors['tag']['name'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['tag']['name']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;

echo '  <div class="mb-3 row'.($tag['action'] !== 'ignore' ? ' d-none' : '').'">'.END_LINE;
echo '    <label for="inputActiveUntil" class="col-sm-2 col-form-label">Active until</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="inputActiveUntil" name="active_until" type="datetime-local" class="form-control'.(!empty($errors['tag']['active_until']) ? ' is-invalid' : '').'" value="'.gmdate('Y-m-d H:i', is_string($tag['active_until']) ? strtotime($tag['active_until']) : $tag['active_until']).'" />'.END_LINE;
if (!empty($errors['tag']['active_until'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['tag']['active_until']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;

echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="selectAction" class="col-sm-2 col-form-label">Action</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
$actionOptions = array(
  ['value' => 'ignore', 'name' => 'Ignore'],
);
echo selectHtml('action', $actionOptions, $tag['action'], ['id' => 'selectAction', 'required' => 'required']);

if (!empty($errors['tag']['action'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['tag']['action']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;

echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="selectProjectId" class="col-sm-2 col-form-label">Projects</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo selectHtml('project_id', array_map(fn ($project) => ['value' => (string)$project['_id'], 'name' => $project['name']], $projects), $tag['project_id'], ['id' => 'selectProjectId', 'required' => 'required', 'onchange' => 'tagProjectIdChanged(this)'], ['zero_select' => '-- select --']);
if (!empty($errors['tag']['project_id'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['tag']['project_id']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;

echo '  <div class="row align-items-center">'.END_LINE;
echo '    <div class="col"><h2>Rules</h2></div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <table id="tableRules" class="table">'.END_LINE;
echo '    <thead>'.END_LINE;
echo '     <tr>'.END_LINE;
echo '       <th>Field</th>'.END_LINE;
echo '       <th>Filter</th>'.END_LINE;
echo '       <th>Value</th>'.END_LINE;
echo '       <th>Actions</th>'.END_LINE;
if (!empty($errors['tag']['rules'])) {
  echo '       <th>Errors</th>'.END_LINE;
}
echo '     </tr>'.END_LINE;
echo '    </thead>'.END_LINE;
echo '    <tbody>'.END_LINE;
if (!empty($errors['tag']['rules']['global'])) {
  echo '      <tr>'.END_LINE;
  echo '        <td class="text-danger" colspan="4">'.$errors['tag']['rules']['global'].'</td>'.END_LINE;
  echo '      </tr>'.END_LINE;
}
foreach ($tag['rules'] as $key => $rule) {
  echo '      <tr id="rule'.$key.'">'.END_LINE;
  echo '        <td>'.END_LINE;
  if (!empty($projectsFields[$tag['project_id']])) {
    if (!in_array($rule['field'], $projectsFields[$tag['project_id']])) {
      $projectsFields[$tag['project_id']][$rule['field']] = $rule['field'].' - unknown';
    }
    echo selectHtml('rules['.$key.'][field]', $projectsFields[$tag['project_id']], $rule['field'], ['class' => 'ruleFieldSelect'], ['key_value' => '__key', 'key_name' => '__value', 'zero_select' => '-- select --']);
  } else {
    echo '        <select name="rules['.$key.'][field]" class="ruleFieldSelect form-control">'.END_LINE;
    echo '          <option>-- select --</option>'.END_LINE;
    echo '          <option value="'.htmlspecialchars($rule['field'], ENT_QUOTES).'" selected="selected">'.htmlspecialchars($rule['field'], ENT_QUOTES).' - unknown</option>'.END_LINE;
    echo '        </select>'.END_LINE;
  }
  echo '        </td>'.END_LINE;
  echo '        <td>'.selectHtml('rules['.$key.'][filter]', $ruleFilterOptions, $rule['filter']).'</td>'.END_LINE;
  echo '        <td><input name="rules['.$key.'][value]" value="'.htmlspecialchars($rule['value'], ENT_QUOTES).'" class="form-control" /></td>'.END_LINE;
  echo '        <td class="rowActions">'.END_LINE;
  echo '          <a href="#" onclick="tableRowRemove(\'rule'.$key.'\', \'Rule\'); return false;" class="btn btn-outline-danger"  title="Remove Rule" role="button"><i class="bi bi-trash3 buttonIcon"></i></a></td>'.END_LINE;
  echo '        </td>'.END_LINE;
  if (!empty($errors['tag']['rules'])) {
    echo '        <td class="text-danger">'.(!empty($errors['tag']['rules'][$key]) ? $errors['tag']['rules'][$key] : '').'</td>'.END_LINE;
  }
  echo '      </tr>'.END_LINE;
}
echo '    </tbody>'.END_LINE;
echo '  </table>'.END_LINE;

echo '<template id="rowRuleTemplate">'.END_LINE;
echo '  <tr id="rowRuleNew">'.END_LINE;
echo '    <td>'.END_LINE;
if (!empty($projectsFields[$tag['project_id']])) {
  echo selectHtml('rules[__replace__][field]', $projectsFields[$tag['project_id']], '', ['class' => 'ruleFieldSelect'], ['key_value' => '__key', 'key_name' => '__value', 'zero_select' => '-- select --']);
} else {
  echo '      <select name="rules[__replace__][field]" class="ruleFieldSelect form-control">'.END_LINE;
  echo '        <option>-- select --</option>'.END_LINE;
  echo '      </select>'.END_LINE;
}
echo '    </td>'.END_LINE;
echo '    <td>'.selectHtml('rules[__replace__][filter]', $ruleFilterOptions).'</td>'.END_LINE;
echo '    <td><input name="rules[__replace__][value]" value="" class="form-control" /></td>'.END_LINE;
echo '    <td class="rowActions">'.END_LINE;
echo '      <a href="#" onclick="tableRowRemove(\'__replace__\', \'Rule\'); return false;" class="btn btn-outline-danger"  title="Remove Rule" role="button"><i class="bi bi-trash3 buttonIcon"></i></a></td>'.END_LINE;
echo '    </td>'.END_LINE;
echo '  </tr>'.END_LINE;
echo '</template>'.END_LINE;

echo '  <div class="row align-items-center">'.END_LINE;
echo '    <div class="col"><a href="#" onclick="ruleRowAdd(); return false" class="btn btn-outline-primary" role="button">Add Rule</a></div>'.END_LINE;
echo '    <div class="col text-end">'.END_LINE;
echo '      <input type="submit" class="btn btn-success" value="Save'.(empty($tag['active']) ? ' and reactivate' : '').'">'.END_LINE;
if (!empty($tag['active'])) {
  echo '      <input type="submit" class="btn btn-primary" name="save_and_process" value="Save and process">'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '</form>'.END_LINE;

echo '<script>'.END_LINE;
echo 'var projectsFields = '.json_encode($projectsFields).';'.END_LINE;
echo '</script>'.END_LINE;

echo floatingSaveButtonHtml('formTagEdit');

echo pageFooter();
