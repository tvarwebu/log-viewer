<?php

$database = getDatabaseOrDie();

if (empty($_POST)) {
  htmlError('Missing data to save.', 'Project Edit');
}

$projectId = !empty($_REQUEST['project_id']) && $_REQUEST['project_id'] !== 'new' ? sanitizeStringInput($_REQUEST['project_id']) : '';

$errors = ['global' => [], 'project' => [], 'fields' => []];

$projectName = !empty($_POST['name']) ? sanitizeStringInput($_POST['name']) : '';
if (empty($projectName)) {
  $errors['project']['name'] = 'Missing Project name.';
}

$projectHandle = !empty($_POST['handle']) ? sanitizeStringInput($_POST['handle']) : '';
if (empty($projectHandle)) {
  $errors['project']['handle'] = 'Missing Project handle.';
}

$projectPriority = !empty($_POST['priority']) ? (int)$_POST['priority'] : 100;
if (!is_int($projectPriority)) {
  $errors['project']['priority'] = 'Invalid value supplied for Project priority.';
}

$filter = ['handle' => $projectHandle];
if (!empty($projectId)) {
  $filter['_id'] = ['$ne' => new MongoDB\BSON\ObjectID($projectId)];
}

$projectsCurrent = $database->projects->find($filter);
if (count($projectsCurrent->toArray()) > 0) {
  $errors['project']['handle'] = 'Handle must be unique.';
}

$validator = array(
  '$jsonSchema' => array(
    'bsonType' => 'object',
    'title' => $projectName.' Object Validation',
    'required' => [],
    'properties' => [],
  )
);
$fields = [];
if (!empty($_POST['fields'])) {
  foreach ($_POST['fields'] as $key => $field) {
    $fieldName = !empty($field['name']) ? sanitizeStringInput($field['name']) : '';
    if (empty($fieldName)) {
      continue;
    }

    $fieldType = !empty($field['type']) && in_array($field['type'], ['string', 'int', 'double', 'array']) ? $field['type'] : '';
    if (empty($fieldType)) {
      $errors['fields'][$key] = 'Invalid type of Field.';
      continue;
    }

    $validator['$jsonSchema']['properties'][$fieldName] = ['bsonType' => $fieldType];
    if (!empty($field['required'])) {
      $validator['$jsonSchema']['required'][] = $fieldName;
    }
  }
}

if (!empty(array_sum(array_map('count', $errors)))) {
  includePageAndDie('project_edit', 400);
}

if (empty($validator['$jsonSchema']['required'])) {
  unset($validator['$jsonSchema']['required']);
}

if (!empty($projectId)) {
  $project = $database->projects->findOne(['_id' => new MongoDB\BSON\ObjectID($projectId)]);
  if (empty($project)) {
    $errors['global'][] = 'Could not find Project with ID: '.$projectId;
    includePageAndDie('project_edit', 400);
  }

  $projectUpdate = [];
  if ($project['name'] !== $projectName) {
    $projectUpdate['name'] = $projectName;
  }
  if ($project['handle'] !== $projectHandle) {
    $projectUpdate['handle'] = $projectHandle;
  }
  if (!isset($project['priority']) || $project['priority'] !== $projectPriority) {
    $projectUpdate['priority'] = $projectPriority;
  }

  if (!empty($projectUpdate)) {
    $resultProjectUpdate = $database->projects->updateOne(['_id' => new MongoDB\BSON\ObjectID($projectId)], ['$set' => $projectUpdate]);
    if ($resultProjectUpdate->getMatchedCount() === 0) {
      $errors['global'][] = 'Could not update Project.';
      includePageAndDie('project_edit', 400);
    }
  }

  if (!empty($validator['$jsonSchema']['properties'])) {
    $resultCollectionModify = $database->modifyCollection('logs_'.$projectId, ['validator' => $validator]);
    if (empty($resultCollectionModify['ok'])) {
      $errors['global'][] = 'Could not update Collection.';
      includePageAndDie('project_edit', 400);
    }
  }
} else {
  $resultProjectCreate = $database->projects->insertOne(['name' => $projectName, 'handle' => $projectHandle, 'priority' => $projectPriority]);
  if ($resultProjectCreate->getInsertedCount() === 0) {
    $errors['global'][] = 'Could not add Project to DB.';
    includePageAndDie('project_edit', 400);
  }
  $projectId = $resultProjectCreate->getInsertedId();
  $resultCollectionCreate = $database->createCollection('logs_'.$projectId, !empty($validator['$jsonSchema']['properties']) ? ['validator' => $validator] : []);
  if (empty($resultCollectionCreate['ok'])) {
    $errors['global'][] = 'Could not create Collection.';
    includePageAndDie('project_edit', 400);
  }
}

messageAdd('Project '.(!empty($projectId) ? ' updated' : 'created'), 'success', 'project_save');
Header('Location: '.getCorrectUrl('projects'));
die();
