<?php

$database = getDatabaseOrDie();

$projectId = !empty($_REQUEST['project_id']) ? sanitizeStringInput($_REQUEST['project_id']) : '';
if (empty($projectId)) {
  htmlError('Missing Project ID to delete.', 'Project delete');
}
$project = $database->projects->findOne(['_id' => new MongoDB\BSON\ObjectID($projectId)]);
if (empty($project)) {
  htmlError('Could not find Project with ID: '.$projectId, 'Project delete');
}
$collection = $database->{'logs_'.$project['_id']};
if (!empty($collection)) {
  $result = $collection->drop();
  if (empty($result['ok'])) {
    htmlError('Could not delete Logs Collection for project ID: '.$projectId, 'Project delete');
  }
}
$database->projects->deleteOne(['_id' => new MongoDB\BSON\ObjectID($projectId)]);

messageAdd('Project '.$project['name'].' and all related Logs were deleted', 'success', 'project_delete');
Header('Location: '.getCorrectUrl(''));
die();
