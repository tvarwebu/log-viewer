<?php

$database = getDatabaseOrDie();

$project = null;
$projectId = !empty($_REQUEST['project_id']) ? sanitizeStringInput($_REQUEST['project_id']) : (!empty($_POST['project_id']) ? sanitizeStringInput($_POST['project_id']) : '');
$fields = !empty($_POST['fields']) ? $_POST['fields'] : [];
if (!empty($projectId) && $projectId !== 'new') {
  $projects = $database->projects;
  $project = $projects->findOne(['_id' => new MongoDB\BSON\ObjectID($projectId)]);
  if (empty($project)) {
    htmlError('Could not find Project with ID: '.$_REQUEST['project_id'], 'Project Edit');
  }

  if (empty($fields)) {
    $fields = logsFields((string)$project['_id']);
  }
}

$projectName = !is_null($project) ? $project['name'] : (!empty($_POST['name']) ? $_POST['name'] : '');
$projectHandle = !is_null($project) ? $project['handle'] : (!empty($_POST['handle']) ? $_POST['handle'] : '');
$projectPriority = !is_null($project) && !empty($project['priority']) ? $project['priority'] : (!empty($_POST['priority']) ? $_POST['priority'] : 100);
if (empty($fields)) {
  $fields[] = ['name' => '', 'type' => 'string', 'required' => false];
}

$links = [];
if (!is_null($project)) {
  $links[] = array(
    'url' => 'projects/'.$projectId.'/logs',
    'value' => '<i class="bi bi-list-columns buttonIcon"></i>',
    'class' => 'btn-outline-secondary',
    'title' => 'Show Logs',
  );
  $links[] = array(
    'url' => 'api/logs/load?project_handle='.$project['handle'].'&limit=1000',
    'value' => '<i class="bi bi-filetype-raw buttonIcon"></i>',
    'class' => 'btn-outline-secondary',
    'title' => 'Show raw data for last 1000 Logs',
  );
  $links[] = array(
    'url' => 'projects/'.$projectId.'/delete',
    'value' => '<i class="bi bi-trash3 buttonIcon"></i>',
    'class' => 'btn-outline-danger',
    'onclick' => 'return confirm(\'Are you sure you want to delete '.htmlspecialchars($project['name'], ENT_QUOTES).' and all related Logs?\')',
    'title' => 'Delete Project and all related Logs',
  );
}

echo pageHeader((!is_null($project) ? $project['name'] : 'New').' - Project Edit', 'projects/'.$projectId.'/edit', $links);

if (!empty($errors['global'])) {
  echo '<div class="alert alert-danger text-center">'.implode('<br />'.END_LINE, $errors['global']).'</div>'.END_LINE;
}

echo fieldsImportModal();
if ($projectId !== 'new') {
  /** @var array<int, array{_id: string, name: string, active: int, project_ids: string[]}> $dashboards */
  $dashboards = cursorToArray($database->dashboards->find(['active' => 1]));
  /** @var array<int, array{_id: string, forename: string, surname: string, active: int, type: string, permissions: array{dashboards: string[], projects: array<string, string>, additional: string[]}}> $users */
  $users = cursorToArray($database->users->find(['active' => 1], ['sort' => ['forename' => 1, 'surname' => 1]]));

  if (isSuperuser()) {
    echo projectDashboardAssignmentModal($dashboards, $projectId);
    echo projectUserPermissionsModal($users, $projectId);
  }
}

$formClass = !empty($errors) && !empty(array_sum(array_map('count', $errors))) ? 'was-validated' : 'needs-validation';
echo '<form id="formProjectEdit" method="post" action="'.getCorrectUrl('projects/'.(!is_null($project) ? $project['_id'] : 'new').'/save').'" class="'.$formClass.'" novalidate>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="name" class="col-sm-2 col-form-label">Name</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="name" invalid="invalid" name="name" class="form-control'.(!empty($errors['project']['name']) ? ' is-invalid' : '').'" value="'.htmlspecialchars($projectName, ENT_QUOTES).'" required />'.END_LINE;
echo '      <span class="invalid-feedback">'.(!empty($errors['project']['name']) ? htmlspecialchars($errors['project']['name']) : 'Missing Project name.').'</span>'.END_LINE;
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="handle" class="col-sm-2 col-form-label">Handle</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="handle" invalid="invalid" name="handle" class="form-control'.(!empty($errors['project']['handle']) ? ' is-invalid' : '').'" value="'.htmlspecialchars($projectHandle, ENT_QUOTES).'" required />'.END_LINE;
echo '      <span class="invalid-feedback">'.(!empty($errors['project']['handle']) ? htmlspecialchars($errors['project']['handle']) : 'Missing Project handle.').'</span>'.END_LINE;
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="priority" class="col-sm-2 col-form-label">Priority</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="priority" invalid="invalid" name="priority" class="form-control'.(!empty($errors['project']['priority']) ? ' is-invalid' : '').'" value="'.intval($projectPriority).'" />'.END_LINE;
echo '      <span class="invalid-feedback">'.(!empty($errors['project']['priority']) ? htmlspecialchars($errors['project']['priority']) : '').'</span>'.END_LINE;
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
if ($projectId !== 'new') {
  echo '  <div class="mb-3 row actionsHover">'.END_LINE;
  echo '    <div class="col-sm-2 col-form-label actionsCover">'.END_LINE;
  if (isSuperuser()) {
    echo '      <div class="actions">'.END_LINE;
    echo '        <a class="btn btn-outline-secondary" href="#" data-bs-toggle="modal" data-bs-target="#projectDashboardAssignmentModal" title="Edit Dashboards assignment." target="_blank"><i class="bi bi-pencil buttonIcon"></i></a>'.END_LINE;
    echo '      </div>'.END_LINE;
  }
  echo '      Used in Dashboards'.END_LINE;
  echo '    </div>'.END_LINE;
  echo '    <div id="dashboardsAssignedToWrapper" class="col-sm-10 align-self-center">'.END_LINE;
  $dashboardsAssigned = !empty($dashboards) ? array_filter($dashboards, fn ($dashboard) => in_array($projectId, $dashboard['project_ids'])) : [];
  if (!empty($dashboardsAssigned)) {
    foreach ($dashboardsAssigned as $dashboard) {
      echo '      <a href="'.getCorrectUrl('dashboards/'.$dashboard['_id'].'/edit').'" class="btn btn-sm mb-2 me-1 btn-outline-secondary btnMinWidth">'.$dashboard['name'].'</a>'.END_LINE;
    }
  } else {
    echo '      <span class="text-warning">Project is not assign to any Dashboard</span>'.END_LINE;
  }
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;

  $usersPermissions = !empty($users) ? array_filter($users, fn ($user) => $user['type'] === 'superuser' || (!empty($user['permissions']['projects'][$projectId]) && in_array($user['permissions']['projects'][$projectId], ['standard', 'maintainer', 'admin']))) : [];
  echo '  <div class="mb-3 row actionsHover">'.END_LINE;
  echo '    <div class="col-sm-2 col-form-label actionsCover">'.END_LINE;
  if (isSuperuser()) {
    echo '      <div class="actions">'.END_LINE;
    echo '        <a class="btn btn-outline-secondary" href="#" data-bs-toggle="modal" data-bs-target="#projectUserPermissionsModal" title="Edit Users permissions." target="_blank"><i class="bi bi-pencil buttonIcon"></i></a>'.END_LINE;
    echo '      </div>'.END_LINE;
  }
  echo '      Users access'.END_LINE;
  echo '    </div>'.END_LINE;
  echo '    <div id="usersPermissionsWrapper" class="col-sm-10 align-self-center">'.END_LINE;
  foreach ($usersPermissions as $user) {
    if (($user['type'] !== 'superuser' && isSuperuser()) || $user['_id'] === $_SESSION['LVuser']['_id']) {
      echo '        <a href="'.getCorrectUrl('users/'.$user['_id'].'/edit').'" class="btn btn-sm mb-2 btn-outline-secondary btnMinWidth">'.$user['forename'].' '.$user['surname'].' | '.($user['type'] === 'superuser' ? 'Superuser' : ucfirst($user['permissions']['projects'][$projectId])).'</a>'.END_LINE;
    } else {
      echo '        <span class="btn btn-sm mb-2 btn-outline-secondary disabled btnMinWidth">'.$user['forename'].' '.$user['surname'].' | '.($user['type'] === 'superuser' ? 'Superuser' : ucfirst($user['permissions']['projects'][$projectId])).'</span>'.END_LINE;
    }
  }
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;
}

$fieldTypeOptions = array(
  ['value' => 'string', 'name' => 'String'],
  ['value' => 'int', 'name' => 'Int'],
  ['value' => 'double', 'name' => 'Double'],
  ['value' => 'array', 'name' => 'Array'],
);

echo '  <div class="row align-items-center">'.END_LINE;
echo '    <div class="col"><h2>Fields</h2></div>'.END_LINE;
echo '    <div class="col text-end">'.END_LINE;
echo '      <a class="btn btn-outline-secondary buttonIcon" href="#" onclick="return projectFieldsExport();" title="Export Fields" role="button"><i class="bi bi-file-arrow-down"></i></a>'.END_LINE;
echo '      <a class="btn btn-outline-secondary buttonIcon" href="#" data-bs-toggle="modal" data-bs-target="#fieldsImportModal" title="Import Fields" role="button"><i class="bi bi-file-arrow-up"></i></a>'.END_LINE;
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <table id="tableFields" class="table">'.END_LINE;
echo '    <thead>'.END_LINE;
echo '     <tr>'.END_LINE;
echo '       <th>Name</th>'.END_LINE;
echo '       <th>Type</th>'.END_LINE;
echo '       <th>Required</th>'.END_LINE;
echo '       <th>Actions</th>'.END_LINE;
if (!empty($errors['fields'])) {
  echo '       <th>Errors</th>'.END_LINE;
}
echo '     </tr>'.END_LINE;
echo '    </thead>'.END_LINE;
echo '    <tbody>'.END_LINE;
foreach ($fields as $key => $field) {
  echo '      <tr id="field'.$key.'">'.END_LINE;
  echo '        <td><input name="fields['.$key.'][name]" value="'.htmlspecialchars($field['name'], ENT_QUOTES).'" class="form-control" /></td>'.END_LINE;
  echo '        <td>'.selectHtml('fields['.$key.'][type]', $fieldTypeOptions, $field['type']).'</td>'.END_LINE;
  echo '        <td class="align-middle"><input type="checkbox" name="fields['.$key.'][required]"'.(!empty($field['required']) ? ' checked="checked"' : '').' class="form-check-input" /></td>'.END_LINE;
  echo '        <td class="rowActions"><a href="#" onclick="tableRowRemove(\'field'.$key.'\', \'Field\'); return false;" class="btn btn-outline-danger"  title="Remove Field" role="button"><i class="bi bi-trash3 buttonIcon"></i></a></td>'.END_LINE;
  if (!empty($errors['fields'][$key])) {
    echo '        <td class="text-danger">'.$errors['fields'][$key].'</td>'.END_LINE;
  }
  echo '      </tr>'.END_LINE;
}
echo '    </tbody>'.END_LINE;
echo '  </table>'.END_LINE;
echo '  <div class="row align-items-center">'.END_LINE;
echo '    <div class="col"><a href="#" onclick="fieldRowAdd(); return false" class="btn btn-outline-primary" role="button">Add Field</a></div>'.END_LINE;
echo '    <div class="col text-end"><input type="submit" class="btn btn-success" value="Save"></div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '</form>'.END_LINE;

echo floatingSaveButtonHtml('formProjectEdit');

echo '<template id="rowFieldsTemplate">'.END_LINE;
echo '  <tr id="rowFieldsNew">'.END_LINE;
echo '    <td><input name="fields[__replace__][name]" value="" class="form-control" /></td>'.END_LINE;
echo '    <td>'.selectHtml('fields[__replace__][type]', $fieldTypeOptions).'</td>'.END_LINE;
echo '    <td class="align-middle"><input type="checkbox" name="fields[__replace__][required]" class="form-check-input" /></td>'.END_LINE;
echo '    <td class="rowActions"><a href="#" onclick="tableRowRemove(\'__replace__\', \'Field\'); return false;" class="btn btn-outline-danger" title="Remove Field" role="button"><i class="bi bi-trash3 buttonIcon"></i></a></td>'.END_LINE;
echo '  </tr>'.END_LINE;
echo '</template>'.END_LINE;

echo '<script>'.END_LINE;
echo '  var fieldRowCounter = '.count($fields).END_LINE;
echo '  handleFormSubmitValidate()'.END_LINE;
echo '</script>'.END_LINE;

echo pageFooter();
