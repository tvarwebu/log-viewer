<?php

$database = getDatabaseOrDie();

$tagId = !empty($_REQUEST['tag_id']) ? sanitizeStringInput($_REQUEST['tag_id']) : '';
if (empty($tagId)) {
  htmlError('Missing Tag ID to deactivate.', 'Tag deactivate');
}
$tag = $database->tags->findOne(['_id' => new MongoDB\BSON\ObjectID($tagId)]);
if (empty($tag)) {
  htmlError('Could not find Tag with ID: '.$tagId, 'Tag deactivate');
}
$database->tags->updateOne(['_id' => new MongoDB\BSON\ObjectID($tagId)], ['$set' => ['active' => 0, 'deactivated_on' => time()]]);

messageAdd('Tag '.$tag['name'].' was deactivated', 'success', 'tag_deactivate');
Header('Location: '.getCorrectUrl('tags'));
die();
