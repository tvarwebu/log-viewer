<?php

$database = getDatabaseOrDie();
$dashboardId = 'new';
if (!empty($_REQUEST['dashboard_id'])) {
  $dashboardId = sanitizeStringInput($_REQUEST['dashboard_id']);
}

$dashboard = array(
  'name' => '',
  'color' => '#3a5688',
  'active' => 1,
  'project_ids' => [],
);

if (!empty($_POST)) {
  $dashboard = array_merge($dashboard, $_POST);
} elseif ($dashboardId !== 'new') {
  $dashboard = $database->dashboards->findOne(['_id' => new MongoDB\BSON\ObjectID($dashboardId)]);
  if (empty($dashboard)) {
    htmlError('Could not find Dashboard with ID: '.$dashboardId, 'Dashboard Edit');
  }
}
if (empty($dashboard['color'])) {
  $dashboard['color'] = '#3a5688';
}

$links = [];
if ($dashboardId !== 'new') {
  $links[] = ['url' => 'dashboards/'.$dashboardId.'/view', 'value' => '<i class="bi bi-eye buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'View Dashboard'];
  if (!empty($dashboard['active'])) {
    $links[] = array(
      'url' => 'dashboards/'.$dashboardId.'/deactivate',
      'value' => '<i class="bi bi-trash3 buttonIcon"></i>',
      'class' => 'btn-outline-danger',
      'onclick' => 'return confirm(\'Are you sure you want to deactivate '.htmlspecialchars($dashboard['name'], ENT_QUOTES).'?\')',
      'title' => 'Deactivate Dashboard',
    );
  }
}

$pageName = '';
if ($dashboardId !== 'new') {
  $pageName .= $dashboard['name'];
  if (empty($dashboard['active'])) {
    $pageName .= ' - inactive';
  }
} else {
  $pageName .= 'New';
}
$pageName .= ' - Dashboard Edit';

echo pageHeader($pageName, 'dashboards/'.$dashboardId.'/edit', $links);

if (!empty($errors['global'])) {
  echo '<div class="alert alert-danger text-center">'.implode('<br />'.END_LINE, $errors['global']).'</div>'.END_LINE;
}

$formClass = !empty($errors) && !empty(array_sum(array_map('count', $errors))) ? 'was-validated' : 'needs-validation';
echo '<form id="formDashboardEdit" method="post" action="'.getCorrectUrl('dashboards/'.($dashboardId !== 'new' ? $dashboardId : 'new').'/save').'" class="'.$formClass.'" novalidate>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="inputName" class="col-sm-2 col-form-label">Name</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="inputName" name="name" class="form-control'.(!empty($errors['dashboard']['name']) ? ' is-invalid' : '').'" value="'.htmlspecialchars($dashboard['name'], ENT_QUOTES).'" required />'.END_LINE;
if (!empty($errors['dashboard']['name'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['dashboard']['name']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="inputColor" class="col-sm-2 col-form-label">Color</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input type="color" id="inputColor" name="color" class="form-control form-control-color'.(!empty($errors['dashboard']['color']) ? ' is-invalid' : '').'" value="'.htmlspecialchars($dashboard['color'], ENT_QUOTES).'" required />'.END_LINE;
if (!empty($errors['dashboard']['color'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['dashboard']['color']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;

$projects = $database->projects->find()->toArray();
usort($projects, arraySort(['field' => 'priority', 'order' => 'asc']));

echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="selectProjectIds" class="col-sm-2 col-form-label">Projects</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <select id="selectProjectIds" class="form-select'.(!empty($errors['dashboard']['project_ids']) ? ' is-invalid' : '').'" name="project_ids[]" size="10" multiple required>'.END_LINE;
foreach ($projects as $project) {
  echo '        <option value="'.$project['_id'].'"'.(in_array((string)$project['_id'], (array)$dashboard['project_ids']) ? ' selected="selected"' : '').'>'.$project['name'].'</option>'.END_LINE;
}
echo '      </select>'.END_LINE;

if (!empty($errors['dashboard']['project_ids'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['dashboard']['project_ids']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;

if ($dashboardId !== 'new') {
  $usersFilter = array(
    'active' => 1,
    '$or' => array(
      ['type' => 'superuser'],
      ['permissions.dashboards' => $dashboardId],
    )
  );
  $users = cursorToArray($database->users->find($usersFilter));
  echo '  <div class="mb-3 row">'.END_LINE;
  echo '    <div class="col-sm-2 col-form-label">Users access</div>'.END_LINE;
  echo '    <div class="col-sm-10 align-self-center">'.END_LINE;
  /** @var array{_id: string, forename: string, surname: string, active: int, type: string, permissions: array{dashboards: string[], projects: array<string, string>, additional: string[]}} $user */
  foreach ($users as $user) {
    if ($user['type'] !== 'superuser' || $user['_id'] === $_SESSION['LVuser']['_id']) {
      echo '        <a href="'.getCorrectUrl('users/'.$user['_id'].'/edit').'" class="btn btn-sm mb-2 btn-outline-secondary btnMinWidth">'.$user['forename'].' '.$user['surname'].'</a>'.END_LINE;
    } else {
      echo '        <span class="btn btn-sm mb-2 btn-outline-secondary disabled btnMinWidth">'.$user['forename'].' '.$user['surname'].'</span>'.END_LINE;
    }
  }
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;
}

echo '  <div class="row align-items-center">'.END_LINE;
echo '    <div class="col text-end"><input type="submit" class="btn btn-success" value="Save'.(empty($dashboard['active']) ? ' and reactivate' : '').'"></div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '</form>'.END_LINE;

echo floatingSaveButtonHtml('formDashboardEdit');

echo pageFooter();
