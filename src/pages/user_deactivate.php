<?php

$database = getDatabaseOrDie();

$userId = !empty($_REQUEST['user_id']) ? sanitizeStringInput($_REQUEST['user_id']) : '';
if (empty($userId)) {
  htmlError('Missing User ID to deactivate.', 'User deactivate');
}
if ($userId === $_SESSION['LVuser']['_id']) {
  htmlError('You cannot deactivate yourself.', 'User deactivate');
}
$user = $database->users->findOne(['_id' => new MongoDB\BSON\ObjectID($userId)]);
if (empty($user)) {
  htmlError('Could not find User with ID: '.$userId, 'User deactivate');
}
$database->users->updateOne(['_id' => new MongoDB\BSON\ObjectID($userId)], ['$set' => ['active' => 0, 'deactivated_on' => time()]]);

messageAdd('User '.$user['forename'].' '.$user['forename'].' was deactivated', 'success', 'user_deactivate');
Header('Location: '.getCorrectUrl('users'));
die();
