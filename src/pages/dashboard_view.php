<?php

$database = getDatabaseOrDie();

$dashboardId = !empty($_REQUEST['dashboard_id']) ? sanitizeStringInput($_REQUEST['dashboard_id']) : '';
if (empty($dashboardId)) {
  htmlError('Missing Dashboard ID.', 'Dashboard View');
}

$dashboard = $database->dashboards->findOne(['_id' => new MongoDB\BSON\ObjectID($dashboardId)]);
if (empty($dashboard)) {
  htmlError('Could not find Dashboard with ID: '.$dashboardId, 'Dashboard View');
}
if (empty($dashboard['active'])) {
  htmlError('Dashboard is not active.', 'Dashboard View');
}

$projectIds = [];
foreach ((array)$dashboard['project_ids'] as $projectId) {
  if (permissionsCheck('project_logs', $projectId)) {
    $projectIds[] = new MongoDB\BSON\ObjectID($projectId);
  }
}

$projects = $database->projects->find(['_id' => ['$in' => $projectIds]])->toArray();
$projectLogsFilters = iterator_to_array($database->parser_settings->find(['handle' => 'filters', 'active' => 1, 'favourite' => 1]));
$projectListGraphSettings = projectListGraphSettings(array_map(fn ($project) => $project['handle'], $projects));
$projectListData = projectListData($projects);

$request = $_REQUEST;
unset($request['page']);
if (empty($request['sort'])) {
  $request['sort'] = ['field' => 'priority', 'order' => 'asc'];
}
$sort = $request['sort'];
if (!empty($projectListData)) {
  usort($projectListData, arraySort($sort));
}

echo pageHeader($dashboard['name'].' - Dashboard', 'dashboards/'.$dashboardId.'/view', projectListSubmenu($projectListData, $dashboardId));

$projectListHandlesMarkAllowed = array_column(array_filter($projectListData, fn ($logInfo) => !empty($logInfo['userCanMark'])), 'handle');
if (!empty($projectListHandlesMarkAllowed)) {
  echo logsMarkModal($projectListHandlesMarkAllowed);
}
echo projectLogsSearchModal($projectListData);
if (count($projectListData) > 0) {
  echo projectLogsGraphModal($projectListData);
  echo projectLogsGraphSaveModal();
}

echo projectListPieGraphsHtml($projectListData, $projectListGraphSettings['pie']);

echo projectListTableHtml($projectListData, $sort, $projectListGraphSettings['row'], $projectLogsFilters);

echo pageFooter();
