<?php

$database = getDatabaseOrDie();

$errors = ['global' => [], 'dashboard' => []];

if (empty($_POST)) {
  $content = file_get_contents('php://input');
  if (!empty($content)) {
    $_POST = json_decode($content, true);
  }
}

if (empty($_POST)) {
  htmlError('Missing data to save.', 'Dashboard Edit');
}

$dashboardId = !empty($_REQUEST['dashboard_id']) ? sanitizeStringInput($_REQUEST['dashboard_id']) : 'new';
$dashboardCurrent = null;
if ($dashboardId !== 'new') {
  $dashboardCurrent = $database->dashboards->findOne(['_id' => new MongoDB\BSON\ObjectID($dashboardId)]);
  if (empty($dashboardCurrent)) {
    htmlError('Supplied unknown ID for Dashboard.', 'Dashboard Edit');
  }
}

$dashboardName = !empty($_POST['name']) && is_string($_POST['name']) ? sanitizeStringInput($_POST['name']) : '';
if (empty($dashboardName)) {
  $errors['dashboard']['name'] = 'Missing name.';
}

$dashboardColor = !empty($_POST['color']) && is_string($_POST['color']) ? sanitizeStringInput($_POST['color']) : '';
if (empty($dashboardColor)) {
  $errors['dashboard']['color'] = 'Missing color.';
}

$dashboardProjectIds = !empty($_POST['project_ids']) ? array_unique(array_filter(array_map('sanitizeStringInput', (array)$_POST['project_ids']))) : [];
if (empty($dashboardProjectIds)) {
  $errors['dashboard']['project_ids'] = 'Missing Projects for Dashboard.';
}

if (empty(array_sum(array_map('count', $errors)))) {
  $projects = $database->projects->find()->toArray();
  $projectsIds = array_map(fn ($project) => (string)$project['_id'], $projects);
  $projectIdsInvalid = array_diff($dashboardProjectIds, $projectsIds);
  if (!empty($projectIdsInvalid)) {
    $errors['dashboard']['project_ids'] = 'Invalid Projects ('.implode(', ', $projectIdsInvalid).') supplied for dashboard.';
  }
}

if (!empty(array_sum(array_map('count', $errors)))) {
  includePageAndDie('dashboard_edit', 400);
}

$dashboardPost = array(
  'active' => 1,
  'name' => $dashboardName,
  'color' => $dashboardColor,
  'project_ids' => $dashboardProjectIds,
);

if (!is_null($dashboardCurrent)) {
  $dashboardUpdate = [];
  $dashboardCurrent = bsonDocumentToArray($dashboardCurrent);
  foreach ($dashboardPost as $key => $value) {
    if (!isset($dashboardCurrent[$key]) || !compareValues($dashboardCurrent[$key], $value)) {
      $dashboardUpdate[$key] = $value;
    }
  }

  if (!empty($dashboardUpdate)) {
    $resultDashboardUpdate = $database->dashboards->updateOne(['_id' => new MongoDB\BSON\ObjectID($dashboardId)], ['$set' => $dashboardUpdate]);
    if ($resultDashboardUpdate->getMatchedCount() === 0) {
      $errors['global'][] = 'Could not update Dashboard.';
      includePageAndDie('dashboard_edit', 400);
    }
  }
} else {
  $resultDashboardCreate = $database->dashboards->insertOne($dashboardPost);
  if ($resultDashboardCreate->getInsertedCount() === 0) {
    $errors['global'][] = 'Could not add Dashboard to DB.';
    includePageAndDie('dashboard_edit', 400);
  }
}

messageAdd('Dashboard '.(!is_null($dashboardCurrent) ? ' updated' : 'created'), 'success', 'dashboard_save');
Header('Location: '.getCorrectUrl('dashboards'));
die();
