<?php

$database = getDatabaseOrDie();
$deletedShow = !empty($_REQUEST['deleted_show']);
$sort = !empty($_REQUEST['sort']) ? $_REQUEST['sort'] : ['field' => 'name', 'order' => 'asc'];
$dashboards = cursorToArray($database->dashboards->find(!$deletedShow ? ['active' => 1] : []));
if (!empty($dashboards)) {
  usort($dashboards, arraySort($sort));
}
$users = cursorToArray($database->users->find(['active' => 1]));
$projectsAssoc = [];
foreach ($database->projects->find() as $project) {
  $projectsAssoc[(string)$project['_id']] = $project;
}

$usersPerDashboard = array_fill_keys(array_column($dashboards, '_id'), []);
/** @var array{_id: string, forename: string, surname: string, active: int, type: string, permissions: array{dashboards: string[], projects: array<string, string>, additional: string[]}} $user */
foreach ($users as $user) {
  if ($user['type'] === 'superuser') {
    foreach ($usersPerDashboard as $dashboardId => $usersForDashboard) {
      $usersPerDashboard[$dashboardId][] = $user;
    }
  } elseif (!empty($user['permissions']['dashboards'])) {
    foreach ($user['permissions']['dashboards'] as $dashboardId) {
      $usersPerDashboard[$dashboardId][] = $user;
    }
  }
}

$links = array(
  ['url' => 'dashboards/new/edit', 'value' => '<i class="bi bi-plus buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Add Dashboard'],
  ['url' => 'dashboards?deleted_show=1', 'value' => 'Show Deleted', 'class' => 'btn-outline-secondary', 'title' => 'Show Deleted'],
);

echo pageHeader('Dashboards', 'dashboards', $links);

echo '<table class="table table-hover align-middle">'.END_LINE;
echo '  <thead class="table-sortable-header">'.END_LINE;
echo '    <tr>'.END_LINE;
echo tableHeaderSortable('name', 'Name', $sort, '', 'w-25');
if ($deletedShow) {
  echo tableHeaderSortable('active', 'Active', $sort);
}
echo tableHeaderSortable('color', 'Color', $sort);
echo '      <th>Projects</th>'.END_LINE;
echo '      <th class="w-25">Users assigned</th>'.END_LINE;
echo '    </tr>'.END_LINE;
echo '  </thead>'.END_LINE;
echo '  <tbody>'.END_LINE;
if (!empty($dashboards)) {
  /** @var array{_id: string, name: string, active: int, color?: string, project_ids: array<string>} $dashboard */
  foreach ($dashboards as $dashboard) {
    echo '    <tr'.(empty($dashboard['active']) ? ' class="opacity-50"' : '').'>'.END_LINE;
    echo '      <td>'.$dashboard['name'].'</td>'.END_LINE;
    if ($deletedShow) {
      echo '      <td>'.(!empty($dashboard['active']) ? 'Active' : 'Inactive').'</td>'.END_LINE;
    }
    echo '      <td><input type="color" id="inputColor" name="color" class="form-control form-control-color" value="'.(!empty($dashboard['color']) ? htmlspecialchars($dashboard['color'], ENT_QUOTES) : '#3a5688').'" disabled /></td>'.END_LINE;
    echo '      <td>'.END_LINE;
    if (!empty($dashboard['project_ids'])) {
      foreach ($dashboard['project_ids'] as $projectId) {
        if (!empty($projectsAssoc[$projectId])) {
          echo '        <a href="'.getCorrectUrl('projects/'.$projectId.'/edit').'" class="btn btn-sm mb-2 btn-outline-secondary">'.$projectsAssoc[$projectId]['name'].'</a>'.END_LINE;
        }
      }
    } else {
      echo '      No projects assigned'.END_LINE;
    }
    echo '      </td>'.END_LINE;
    echo '      <td class="actionsCover">'.END_LINE;
    echo '        <div class="actions">'.END_LINE;
    if (!empty($dashboard['active'])) {
      echo '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('dashboards/'.$dashboard['_id'].'/view').'" title="View Dashboard"><i class="bi bi-eye buttonIcon"></i></a>'.END_LINE;
    }
    echo '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('dashboards/'.$dashboard['_id'].'/edit').'" title="Edit Dashboard"><i class="bi bi-pencil buttonIcon"></i></a>'.END_LINE;
    echo '        </div>'.END_LINE;
    if (!empty($usersPerDashboard[$dashboard['_id']])) {
      foreach ($usersPerDashboard[$dashboard['_id']] as $user) {
        if ($user['type'] !== 'superuser' || $user['_id'] === $_SESSION['LVuser']['_id']) {
          echo '        <a href="'.getCorrectUrl('users/'.$user['_id'].'/edit').'" class="btn btn-sm mb-2 btn-outline-secondary">'.$user['forename'].' '.$user['surname'].'</a>'.END_LINE;
        } else {
          echo '        <span class="btn btn-sm mb-2 btn-outline-secondary disabled">'.$user['forename'].' '.$user['surname'].'</span>'.END_LINE;
        }
      }
    }
    echo '      </td>'.END_LINE;
    echo '    </tr>'.END_LINE;
  }
} else {
  echo '    <tr>'.END_LINE;
  echo '      <td colspan="3" class="text-center">There are no Dashboards. <a href="'.getCorrectUrl('dashboards/new/edit').'" class="btn btn-sm btn-outline-primary" title="Add Dashboard"><i class="bi bi-plus"></i></a></td>'.END_LINE;
  echo '    </tr>'.END_LINE;
}
echo '  </tbody>'.END_LINE;
echo '</table>'.END_LINE;

echo pageFooter();
