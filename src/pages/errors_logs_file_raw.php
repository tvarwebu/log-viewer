<?php

$fileName = !empty($_REQUEST['file_name']) ? preg_replace('/[^a-z0-9A-Z_]/', '', $_REQUEST['file_name']) : '';
if (empty($fileName)) {
  htmlError('Missing error log file name.', 'Error Logs raw');
}

$filePath = dirname(__DIR__).'/logs/'.$fileName.'.log';
if (!is_file($filePath)) {
  htmlError('Unknown file name supplied: '.$fileName, 'Error Logs raw');
}

echo '<pre>'.END_LINE;
echo htmlspecialchars((string)file_get_contents($filePath));
echo '</pre>'.END_LINE;
