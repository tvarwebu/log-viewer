<?php

$database = getDatabaseOrDie();

$location = !empty($_POST['location']) ? $_POST['location'] : '';
if (empty($location) && !empty($_SERVER['REQUEST_URI']) && strpos($_SERVER['REQUEST_URI'], 'login') === false  && strpos($_SERVER['REQUEST_URI'], 'logout') === false) {
  $location = $_SERVER['REQUEST_URI'];
  if (strpos($location, '/log-viewer/') === 0) {
    $location = strtr($location, ['/log-viewer/' => '']);
  }
}

$usersExist = collectionExists('users') && $database->users->countDocuments() > 0;
if (isUser()) {
  Header('Location: '.getCorrectUrl($location));
  die();
}

$username = isset($_POST['username']) && is_string($_POST['username']) ? sanitizeStringInput($_POST['username']) : '';
$password = isset($_POST['password']) && is_string($_POST['password']) ? $_POST['password'] : '';
$rateLimits = loginRateLimits($username);

if (memcachedRateLimitExceeded('login_invalid', $rateLimits, 60 * 15)) {
  htmlError('Access is temporarily locked because you have attempted too many incorrect logins.', 'Login', 429);
}

$errors = [];
$loginTried = false;
if (!empty($username) && !empty($password)) {
  $errors = $usersExist ? userLogin($username, $password) : usersCollectionInit($_POST);
  $loginTried = true;
} elseif (!empty($_COOKIE['loginHash'])) {
  $errors = sessionLogin($_COOKIE['loginHash']);
  $loginTried = true;
}

if ($loginTried) {
  if (empty($errors)) {
    loginHashUpdate(!empty($_POST['keepMeLoggedIn']));
    Header('Location: '.getCorrectUrl($location));
    die();
  }
  memcachedRateLimitIncrement('login_invalid', $rateLimits, 60 * 15);
}

echo pageHeader('Login');

echo '<div>'.END_LINE;
echo '  <form action="'.getCorrectUrl('login').'" method="post" class="h-75 d-flex flex-column align-items-center justify-content-center'.(!empty($errors) ? ' was-validated' : ' needs-validation').'">'.END_LINE;
if (!empty($errors['global'])) {
  echo '<div class="alert alert-danger text-center">'.$errors['global'].'</div>'.END_LINE;
}
if (!$usersExist) {
  echo '  <p>Please create superuser account.</p>'.END_LINE;
  echo '    <div class="form-group mb-4">'.END_LINE;
  echo '      <label for="inputForename">Forename</label>'.END_LINE;
  echo '      <input name="forename" class="form-control'.(!empty($errors['forename']) ? ' is-invalid' : '').'" id="inputForename" required>'.END_LINE;
  if (!empty($errors['forename'])) {
    echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['forename']).'</span>'.END_LINE;
  }
  echo '    </div>'.END_LINE;
  echo '    <div class="form-group mb-4">'.END_LINE;
  echo '      <label for="inputSurname">Surname</label>'.END_LINE;
  echo '      <input name="surname" class="form-control'.(!empty($errors['surname']) ? ' is-invalid' : '').'" id="inputSurname" required>'.END_LINE;
  if (!empty($errors['surname'])) {
    echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['surname']).'</span>'.END_LINE;
  }
  echo '    </div>'.END_LINE;
}
echo '    <div class="form-group mb-4">'.END_LINE;
echo '      <label for="inputUsername">Username</label>'.END_LINE;
echo '      <input name="username" class="form-control'.(!empty($errors['username']) ? ' is-invalid' : '').'" id="inputUsername" required>'.END_LINE;
if (!empty($errors['username'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['username']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '    <div class="form-group mb-4">'.END_LINE;
echo '      <label for="inputPassword">Password</label>'.END_LINE;
echo '      <input type="password" name="password" class="form-control'.(!empty($errors['password']) ? ' is-invalid' : '').'" id="inputPassword" required'.(!$usersExist ? ' autocomplete="new-password"' : '').'>'.END_LINE;
if (!empty($errors['password'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['password']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
if ($usersExist) {
  echo '    <div class="form-group form-check mb-4">'.END_LINE;
  echo '      <input type="checkbox" name="keepMeLoggedIn" class="form-check-input" id="checkboxKeepMeLoggedIn">'.END_LINE;
  echo '      <label class="form-check-label" for="checkboxKeepMeLoggedIn">Keep me logged in</label>'.END_LINE;
  echo '    </div>'.END_LINE;
} else {
  echo '    <div class="form-group mb-4">'.END_LINE;
  echo '      <label for="inputPasswordCheck">Confirm Password</label>'.END_LINE;
  echo '      <input type="password" name="passwordCheck" class="form-control'.(!empty($errors['passwordCheck']) ? ' is-invalid' : '').'" id="inputPasswordCheck" required autocomplete="new-password">'.END_LINE;
  if (!empty($errors['passwordCheck'])) {
    echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['passwordCheck']).'</span>'.END_LINE;
  }
  echo '    </div>'.END_LINE;
}
echo '    <button type="submit" class="btn btn-primary">'.($usersExist ? 'Sign in' : 'Create user').'</button>'.END_LINE;
if (!empty($location)) {
  echo '    <input type="hidden" name="location" value="'.$location.'">'.END_LINE;
}

echo '  </form>'.END_LINE;
echo '</div>'.END_LINE;

echo pageFooter();
