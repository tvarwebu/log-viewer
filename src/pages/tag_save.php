<?php

$database = getDatabaseOrDie();

$errors = ['global' => [], 'tag' => [],];

if (empty($_POST)) {
  $content = file_get_contents('php://input');
  if (!empty($content)) {
    $_POST = json_decode($content, true);
  }
}

if (empty($_POST)) {
  htmlError('Missing data to save.', 'Tag Edit');
}

$tagId = !empty($_REQUEST['tag_id']) ? sanitizeStringInput($_REQUEST['tag_id']) : 'new';
$tagCurrent = null;
if ($tagId !== 'new') {
  $tagCurrent = $database->tags->findOne(['_id' => new MongoDB\BSON\ObjectID($tagId)]);
  if (empty($tagCurrent)) {
    htmlError('Supplied unknown ID for Tag.', 'Tag Edit');
  }
}

$tagPost = [];
try {
  $tagPost = tagPostValidateAndSanitize($_POST);
} catch (\LogViewer\Exception\ExceptionWithData $ex) {
  $errors = array_merge_recursive($errors, $ex->getFormError());
} catch (\Throwable $th) {
  trigger_error('Unexpected exception throw: '.$th->getMessage().', trace: '.$th->getTraceAsString(), E_USER_WARNING);
  $errors['global'][] = 'Unknown exception throw, contact support.';
}

if (!empty(array_sum(array_map('count', $errors)))) {
  includePageAndDie('tag_edit', 400);
}

if (!is_null($tagCurrent)) {
  $tagUpdate = [];
  $tagCurrent = bsonDocumentToArray($tagCurrent);
  foreach ($tagPost as $key => $value) {
    if (!isset($tagCurrent[$key]) || !compareValues($tagCurrent[$key], $value)) {
      $tagUpdate[$key] = $value;
    }
  }

  if (!empty($tagUpdate)) {
    $resultTagUpdate = $database->tags->updateOne(['_id' => new MongoDB\BSON\ObjectID($tagId)], ['$set' => $tagUpdate]);
    if ($resultTagUpdate->getMatchedCount() === 0) {
      $errors['global'][] = 'Could not update Tag.';
      includePageAndDie('tag_edit', 400);
    }
  }
} else {
  $resultTagCreate = $database->tags->insertOne($tagPost);
  if ($resultTagCreate->getInsertedCount() === 0) {
    $errors['global'][] = 'Could not add Tag to DB.';
    includePageAndDie('tag_edit', 400);
  }
  $tagId = (string)$resultTagCreate->getInsertedId();
}

$tagPost['_id'] = $tagId;

$message = 'Successfully '.(!is_null($tagCurrent) ? 'updated' : 'created').' Tag.';

if (!empty($_POST['save_and_process']) && !empty($tagPost['rules'])) {
  $message .= ' Marked '.tagProcess($tagPost).' logs.';
}

messageAdd($message, 'success', 'tag_save');
Header('Location: '.getCorrectUrl('tags'));
die();
