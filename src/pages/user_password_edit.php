<?php

$database = getDatabaseOrDie();

$userId = !empty($_REQUEST['user_id']) ? sanitizeStringInput($_REQUEST['user_id']) : '';
if ($userId === 'me') {
  $userId = (string)$_SESSION['LVuser']['_id'];
}
if (empty($userId)) {
  htmlError('Missing User ID.', 'User Password Edit');
}

$user = $database->users->findOne(['_id' => new MongoDB\BSON\ObjectID($userId)]);

if ($userId !== $_SESSION['LVuser']['_id'] && ($user['type'] !== 'standard' || !isSuperuser())) {
  htmlError('You are not allowed to change password for this user.', 'User Password Edit', 403);
}
if (empty($user)) {
  htmlError('Could not find User with ID: '.$userId, 'User Password Edit');
}
if (empty($user['active'])) {
  htmlError('You cannot change password for inactive user.', 'User Password Edit');
}

echo pageHeader($user['forename'].' '.$user['surname'].' - User Password Edit', 'users/'.($userId === $_SESSION['LVuser']['_id'] ? 'me' : $userId).'/password-edit');

if (!empty($errors['global'])) {
  echo '<div class="alert alert-danger text-center">'.implode('<br />'.END_LINE, $errors['global']).'</div>'.END_LINE;
}

$formClass = !empty($errors) && !empty(array_sum(array_map('count', $errors))) ? 'was-validated' : 'needs-validation';
echo '<form id="formUserPasswordEdit" method="post" action="'.getCorrectUrl('users/'.($userId === $_SESSION['LVuser']['_id'] ? 'me' : $userId).'/password-save').'" class="'.$formClass.'" novalidate>'.END_LINE;
if ($userId === $_SESSION['LVuser']['_id']) {
  echo '  <div class="mb-3 row">'.END_LINE;
  echo '    <label for="inputPasswordCurrent" class="col-sm-2 col-form-label">Current Password</label>'.END_LINE;
  echo '    <div class="col-sm-10">'.END_LINE;
  echo '      <input type="password" id="inputPasswordCurrent" name="passwordCurrent" class="form-control'.(!empty($errors['user']['passwordCurrent']) ? ' is-invalid' : '').'" autocomplete="off" required />'.END_LINE;
  if (!empty($errors['user']['passwordCurrent'])) {
    echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['passwordCurrent']).'</span>'.END_LINE;
  }
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;
}
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="inputPasswordNew" class="col-sm-2 col-form-label">Password</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="inputPasswordNew" name="passwordNew" type="password" class="form-control'.(!empty($errors['user']['passwordNew']) ? ' is-invalid' : '').'" required autocomplete="new-password" />'.END_LINE;
if (!empty($errors['user']['passwordNew'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['passwordNew']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="inputPasswordCheck" class="col-sm-2 col-form-label">Confirm Password</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="inputPasswordCheck" name="passwordCheck" type="password" class="form-control'.(!empty($errors['passwordCheck']) ? ' is-invalid' : '').'" required autocomplete="new-password" />'.END_LINE;
if (!empty($errors['user']['passwordCheck'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['passwordCheck']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <div class="row align-items-center">'.END_LINE;
echo '    <div class="col text-end"><input type="submit" class="btn btn-success" value="Change password"></div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '</form>'.END_LINE;

echo pageFooter();
