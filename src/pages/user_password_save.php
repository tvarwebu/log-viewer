<?php

$database = getDatabaseOrDie();

$errors = ['global' => [], 'user' => []];

if (empty($_POST)) {
  htmlError('Missing data to save.', 'User Password Edit');
}

$userId = !empty($_REQUEST['user_id']) ? sanitizeStringInput($_REQUEST['user_id']) : '';
if ($userId === 'me') {
  $userId = (string)$_SESSION['LVuser']['_id'];
}
if (empty($userId)) {
  htmlError('Missing user ID.', 'User Password Edit');
}

$userCurrent = $database->users->findOne(['_id' => new MongoDB\BSON\ObjectID($userId)]);
if (empty($userCurrent)) {
  htmlError('Supplied unknown ID for user.', 'User Password ');
}

$keysValidate = ['passwordNew' => 'Password', 'passwordCheck' => 'Confirm Password'];
if ($userId === $_SESSION['LVuser']['_id']) {
  $keysValidate['passwordCurrent'] = 'Current Password';
}
$postValid = [];
foreach ($keysValidate as $key => $niceName) {
  $postValid[$key] = !empty($_POST[$key]) && is_string($_POST[$key]) ? $_POST[$key] : '';
  if (empty($postValid[$key])) {
    $errors['user'][$key] = 'Missing '.$niceName.'.';
  } elseif ($key === 'passwordCurrent' && !passwordVerifyAndRehash($postValid['passwordCurrent'], $userCurrent)) {
    $errors['user'][$key] = 'Invalid password.';
  } elseif ($key === 'passwordNew') {
    $error = passwordValidate($postValid[$key]);
    if (!empty($error)) {
      $errors['user'][$key] = $error;
    }
  }
}

if (empty(array_sum(array_map('count', $errors)))) {
  if ($postValid['passwordNew'] !== $postValid['passwordCheck']) {
    $errors['user']['passwordNew'] = 'Passwords do not match.';
  }
  if (!empty($postValid['passwordCurrent']) && $postValid['passwordNew'] === $postValid['passwordCurrent']) {
    $errors['global'][] = 'New password password cannot match previous password.';
  }
}

if (!empty(array_sum(array_map('count', $errors)))) {
  includePageAndDie('user_password_edit', 400);
}

$resultUserUpdate = $database->users->updateOne(['_id' => $userCurrent['_id']], ['$set' => ['password' => password_hash($postValid['passwordNew'], PASSWORD_DEFAULT)]]);
if ($resultUserUpdate->getMatchedCount() === 0) {
  $errors['global'][] = 'Could not update User.';
  includePageAndDie('user_password_edit', 400);
}

messageAdd('Successfully changed password.', 'success', 'user_password_edit');
Header('Location: '.getCorrectUrl('users/'.($userId === $_SESSION['LVuser']['_id'] ? 'me' : $userId).'/edit'));
die();
