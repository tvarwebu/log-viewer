<?php

$database = getDatabaseOrDie();

$projectId = !empty($_REQUEST['project_id']) ? sanitizeStringInput($_REQUEST['project_id']) : '';
if (empty($projectId)) {
  htmlError('Missing Project ID', 'Logs');
}

$project = $database->projects->findOne(['_id' => new MongoDB\BSON\ObjectID($projectId)]);
if (empty($project)) {
  htmlError('Could not find Project with ID: '.$projectId, 'Project Edit');
}

$logsFilterId = !empty($_REQUEST['logs_filter_id']) ? sanitizeStringInput($_REQUEST['logs_filter_id']) : '';
$logsFilterSetting = null;
$links = [];
if (!empty($_REQUEST['to_be_deleted_show'])) {
  $links[] = ['url' => getCorrectUrl('projects/'.$projectId.'/logs'), 'value' => 'Hide to be Deleted', 'class' => 'btn-outline-secondary'];
} else {
  $links[] = ['url' => getCorrectUrl('projects/'.$projectId.'/logs?to_be_deleted_show=1'), 'value' => 'Show to be Deleted', 'class' => 'btn-outline-secondary'];
}
if (permissionsCheck('project_edit', $projectId)) {
  $links[] = array(
    'url' => 'projects/'.$project['_id'].'/edit',
    'value' => '<i class="bi bi-pencil buttonIcon"></i>',
    'class' => 'btn-outline-secondary',
    'title' => 'Edit Project',
  );
}
echo pageHeader($project['name'].' - Project Logs', 'projects/'.$projectId.'/logs', $links, 'container-fluid');

$parserSettingsAll = cursorToArray($database->parser_settings->find(['project_handle' => $project['handle'], 'active' => 1]));
$parserSettings = ['parserKeys' => [], 'parserKeysTypes' => []];
/**
 * @var array{_id: MongoDB\BSON\ObjectID, handle: string, key: string, active: int, favourite: int, settings: array<string, mixed>} $settings
 */
foreach ($parserSettingsAll as $settings) {
  if ($settings['handle'] === 'options') {
    $settings['settings']['_id'] = (string)$settings['_id'];
    $parserSettings[$settings['handle']] = $settings['settings'];
  } elseif ($settings['handle'] === 'filters') {
    if ((string)$settings['_id'] === $logsFilterId) {
      $logsFilterSetting = $settings;
    }
    if (!isset($parserSettings[$settings['handle']])) {
      $parserSettings[$settings['handle']] = [];
    }
    $settings['_id'] = (string)$settings['_id'];
    $parserSettings[$settings['handle']][] = $settings;
  } else {
    $settings['settings']['_id'] = (string)$settings['_id'];
    $parserSettings[$settings['handle']][$settings['key']] = $settings['settings'];
  }
}

$logsFilter = [];
if (!empty($logsFilterSetting)) {
  $logsFilter = mongoDBLogsFilter($logsFilterSetting['settings']);
} elseif (!empty($_REQUEST['filter_logs'])) {
  $logsFilter = mongoDBLogsFilter(requestFilterSettingsParse(logsFields($projectId), 'contains'));
}
if (empty($_REQUEST['to_be_deleted_show'])) {
  $logsFilter['to_be_deleted'] = ['$ne' => true];
}
$logsFilter['__ignored'] = ['$ne' => true];

$options = [];
if (!empty($_REQUEST['limit'])) {
  $options['limit'] = (int)$_REQUEST['limit'];
}
if (!empty($_REQUEST['skip'])) {
  $options['skip'] = (int)$_REQUEST['skip'];
}

$logs = $database->{'logs_'.$projectId}->find($logsFilter, $options);

$fieldsTypes = [];
$fields = logsFields((string)$project['_id']);
foreach ($fields as $field) {
  $fieldsTypes[$field['name']] = $field['type'];
}

$logsArray = cursorToArray($logs);
$logsJson = json_encode($logsArray);
if ($logsJson === false) {
  trigger_error('Could not encode logs to JSON.', E_USER_WARNING);
  $logsJson = '';
}

echo '<script>'.END_LINE;
echo '  var parserSettings = '.json_encode($parserSettings).';'.END_LINE;
echo '  var logsData = '.strtr($logsJson, ['<' => '&lt;', '>' => '&gt;']).';'.END_LINE;
echo '  var fieldsTypes = '.json_encode($fieldsTypes).';'.END_LINE;
echo '  var projectId = \''.$project['_id'].'\';'.END_LINE;
echo '  var projectHandle = \''.$project['handle'].'\';'.END_LINE;
echo '  var logsLimit = '.(!empty($options['limit']) && $options['limit'] === count($logsArray) ? $options['limit'] : 0).';'.END_LINE;
echo '  var logsToBeDeletedShow = '.(!empty($_REQUEST['to_be_deleted_show']) ? 'true' : 'false').';'.END_LINE;
if (!empty($logsFilterSetting)) {
  echo '  var logsFilterId = \''.$logsFilterSetting['_id'].'\';'.END_LINE;
}
if (isSuperuser()) {
  echo '  var userPermissions = \'superuser\';'.END_LINE;
} else {
  echo '  var userPermissions = \''.($_SESSION['LVuser']['permissions']['projects'][(string)$project['_id']]).'\';'.END_LINE;
}
echo '</script>'.END_LINE;

echo '<div id="app"></div>'.END_LINE;

echo pageFooter(true);
