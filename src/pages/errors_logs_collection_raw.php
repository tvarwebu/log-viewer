<?php

$database = getDatabaseOrDie();

$collectionName = !empty($_REQUEST['collection_name']) ? sanitizeStringInput(basename($_REQUEST['collection_name'])) : '';
if (empty($collectionName)) {
  htmlError('Missing error log collection name.', 'Error Logs raw');
}

$collection = $database->{$collectionName};
$collectionData = $collection->find([], ['sort' => ['_id' => -1]]);

echo '<pre>'.END_LINE;
echo htmlspecialchars(print_r(cursorToArray($collectionData), true));
echo '</pre>'.END_LINE;
