<?php

$database = getDatabaseOrDie();

$dashboardId = !empty($_REQUEST['dashboard_id']) ? sanitizeStringInput($_REQUEST['dashboard_id']) : '';
if (empty($dashboardId)) {
  htmlError('Missing Dashboard ID to deactivate.', 'Dashboard deactivate');
}
$dashboard = $database->dashboards->findOne(['_id' => new MongoDB\BSON\ObjectID($dashboardId)]);
if (empty($dashboard)) {
  htmlError('Could not find Dashboard with ID: '.$dashboardId, 'Dashboard deactivate');
}
$database->dashboards->updateOne(['_id' => new MongoDB\BSON\ObjectID($dashboardId)], ['$set' => ['active' => 0, 'deactivated_on' => time()]]);

messageAdd('Dashboard '.$dashboard['name'].' was deactivated', 'success', 'dashboard_deactivate');
Header('Location: '.getCorrectUrl('dashboards'));
die();
