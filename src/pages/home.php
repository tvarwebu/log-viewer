<?php

$database = getDatabaseOrDie();

if (isSuperuser()) {
  $dashboards = $database->dashboards->find(['active' => 1])->toArray();
} else {
  $dashboardIds = array_map(fn ($dashboardId) => new MongoDB\BSON\ObjectID($dashboardId), (array)$_SESSION['LVuser']['permissions']['dashboards']);
  $dashboards = !empty($dashboardIds) ? $database->dashboards->find(['active' => 1, '_id' => ['$in' => $dashboardIds]])->toArray() : [];
}

echo pageHeader('Dashboards');

echo '<div class="dashboardsWrapper justify-content-center d-flex flex-wrap">'.END_LINE;
if (!empty($dashboards)) {
  foreach ($dashboards as $dashboard) {
    $style = 'background-color: '.(!empty($dashboard['color']) ? htmlspecialchars($dashboard['color'], ENT_QUOTES) : '#3a5688').';';
    $style .= 'background-image: none;';
    if (!empty($dashboard['color']) && isColorLight($dashboard['color'])) {
      $style .= 'color: #212529;';
    }
    echo '  <a href="'.getCorrectUrl('dashboards/'.$dashboard['_id'].'/view').'" class="btn m-3 w-25 text-center align-content-center btnDashboard" style="'.$style.'">'.END_LINE;
    echo '    <h3 class="card-title">'.$dashboard['name'].'</h3>'.END_LINE;
    echo '  </a>'.END_LINE;
  }
} else {
  if ($database->dashboards->countDocuments(['active' => 1]) === 0) {
    echo '<div class="alert alert-info text-center mt-5">'.END_LINE;
    echo '  Currently there are no active Dashboards'.END_LINE;
    if (isSuperuser()) {
      echo '<a class="btn btn-success" href="'.getCorrectUrl('dashboards/new/edit').'" title="Add Dashboard">Create Dashboard</a>'.END_LINE;
    } else {
      echo 'Contact administrator to create some.'.END_LINE;
    }
    echo '</div>'.END_LINE;
  } else {
    echo '<div class="alert alert-error text-center mt-5">You do not have any Dashboards set. Contact administrator to update your profile.</div>'.END_LINE;
  }
}
echo '</div>'.END_LINE;

echo pageFooter();
