<?php

$database = getDatabaseOrDie();
$deletedKeysShow = !empty($_REQUEST['deleted_keys_show']);
$passwordChange = false;
$userId = 'new';
if (!empty($_REQUEST['user_id'])) {
  $userId = sanitizeStringInput($_REQUEST['user_id']);
}
$userIdForLinks = $userId;
if ($userId === 'me') {
  $userId = (string)$_SESSION['LVuser']['_id'];
}

$user = array(
  'forename' => '',
  'surname' => '',
  'username' => '',
  'type' => 'standard',
  'active' => 1,
  'permissions' => array(
    'projects' => [],
    'dashboards' => [],
    'additional' => [],
  ),
);

if (!empty($_POST)) {
  $user = array_merge($user, $_POST);
} elseif ($userId !== 'new') {
  $user = $database->users->findOne(['_id' => new MongoDB\BSON\ObjectID($userId)]);
  if (empty($user)) {
    htmlError('Could not find User with ID: '.$userId, 'User Edit');
  }

  $passwordChange = $userId === $_SESSION['LVuser']['_id'];
  if (!$passwordChange) {
    if ($user['type'] === 'standard' && isSuperuser()) {
      $passwordChange = true;
    }
  }
}

$links = [];
if ($userId !== 'new') {
  if (!empty($user['active']) && isSuperuser() && $userId !== $_SESSION['LVuser']['_id']) {
    $links[] = array(
      'url' => 'users/'.$userId.'/deactivate',
      'value' => '<i class="bi bi-trash3 buttonIcon"></i>',
      'class' => 'btn-outline-danger',
      'onclick' => 'return confirm(\'Are you sure you want to deactivate '.htmlspecialchars($user['forename'].' '.$user['surname'], ENT_QUOTES).'?\')',
      'title' => 'Deactivate User',
    );
  }
  if ($userId === $_SESSION['LVuser']['_id']) {
    $links[] = ['url' => 'users/me/edit?session_refresh=1', 'value' => '<i class="bi bi-arrow-clockwise buttonIcon"></i>', 'class' => 'btn-outline-secondary', 'title' => 'Refresh Permissions'];
  }
  $links[] = ['url' => 'users/'.$userIdForLinks.'/edit?deleted_keys_show=1', 'value' => '<i class="bi bi-eye buttonIcon"></i>', 'class' => 'btn-outline-secondary', 'title' => 'Show deleted Keys'];
}

$pageName = '';
if ($userId !== 'new') {
  $pageName .= $user['forename'].' '.$user['surname'];
  if (empty($user['active'])) {
    $pageName .= ' - inactive';
  }
} else {
  $pageName .= 'New';
}
$pageName .= ' - User Edit';
echo pageHeader($pageName, 'users/'.$userIdForLinks.'/edit', $links);

if (!empty($_REQUEST['session_refresh']) && $userId === $_SESSION['LVuser']['_id']) {
  try {
    sessionRefresh();
    echo '<div class="alert alert-success text-center">Successfully refreshed permissions</div>'.END_LINE;
  } catch (\Exception $ex) {
    htmlError($ex->getMessage(), 'User Edit');
  }
}

foreach (['global', 'projects', 'dashboards', 'additional'] as $errorKey) {
  if (!empty($errors[$errorKey])) {
    echo '<div class="alert alert-danger">'.implode('<br />'.END_LINE, $errors[$errorKey]).'</div>'.END_LINE;
  }
}

$formClass = !empty($errors) && !empty(array_sum(array_map('count', $errors))) ? 'was-validated' : 'needs-validation';
if ($userId === $_SESSION['LVuser']['_id']) {
  $actionLink = getCorrectUrl('users/me/save');
} elseif ($userId === 'new') {
  $actionLink = getCorrectUrl('users/new/save');
} else {
  $actionLink = getCorrectUrl('users/'.$userId.'/save');
}
echo '<form id="formUserEdit" method="post" action="'.$actionLink.'" class="'.$formClass.'" novalidate>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="inputForename" class="col-sm-2 col-form-label">Forename</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="inputForename" name="forename" class="form-control'.(!empty($errors['user']['forename']) ? ' is-invalid' : '').'" value="'.htmlspecialchars($user['forename'], ENT_QUOTES).'" required />'.END_LINE;
if (!empty($errors['user']['forename'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['forename']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="inputSurname" class="col-sm-2 col-form-label">Surname</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="inputSurname" name="surname" class="form-control'.(!empty($errors['user']['surname']) ? ' is-invalid' : '').'" value="'.htmlspecialchars($user['surname'], ENT_QUOTES).'" required />'.END_LINE;
if (!empty($errors['user']['surname'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['surname']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <div class="mb-3 row">'.END_LINE;
echo '    <label for="inputUsername" class="col-sm-2 col-form-label">Username</label>'.END_LINE;
echo '    <div class="col-sm-10">'.END_LINE;
echo '      <input id="inputUsername" name="username" class="form-control'.(!empty($errors['user']['username']) ? ' is-invalid' : '').'" value="'.htmlspecialchars($user['username'], ENT_QUOTES).'" required autocomplete="off" />'.END_LINE;
if (!empty($errors['user']['username'])) {
  echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['username']).'</span>'.END_LINE;
}
echo '    </div>'.END_LINE;
echo '  </div>'.END_LINE;
if ($userId === 'new') {
  echo '  <div class="mb-3 row">'.END_LINE;
  echo '    <label for="inputPassword" class="col-sm-2 col-form-label">Password</label>'.END_LINE;
  echo '    <div class="col-sm-10">'.END_LINE;
  echo '      <input id="inputPassword" name="password" type="password" class="form-control'.(!empty($errors['user']['password']) ? ' is-invalid' : '').'" required autocomplete="new-password" />'.END_LINE;
  if (!empty($errors['user']['password'])) {
    echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['password']).'</span>'.END_LINE;
  }
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;
  echo '  <div class="mb-3 row">'.END_LINE;
  echo '    <label for="inputPasswordCheck" class="col-sm-2 col-form-label">Confirm Password</label>'.END_LINE;
  echo '    <div class="col-sm-10">'.END_LINE;
  echo '      <input id="inputPasswordCheck" name="passwordCheck" type="password" class="form-control'.(!empty($errors['passwordCheck']) ? ' is-invalid' : '').'" required autocomplete="new-password" />'.END_LINE;
  if (!empty($errors['user']['passwordCheck'])) {
    echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['passwordCheck']).'</span>'.END_LINE;
  }
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;
} elseif ($passwordChange) {
  echo '  <div class="mb-3 row">'.END_LINE;
  echo '    <span class="col-sm-2 col-form-label">Password</span>'.END_LINE;
  echo '    <div class="col-sm-10">'.END_LINE;
  echo '      <a href="'.getCorrectUrl('users/'.$userIdForLinks.'/password-edit').'" class="btn btn-outline-secondary">Change Password</a>'.END_LINE;
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;
}
if (isSuperuser()) {
  echo '  <div class="mb-3 row">'.END_LINE;
  echo '    <label for="selectType" class="col-sm-2 col-form-label">Type</label>'.END_LINE;
  echo '    <div class="col-sm-10">'.END_LINE;
  echo '      <select id="selectType" name="type" class="form-select'.(!empty($errors['user']['type']) ? ' is-invalid' : '').'" onchange="this.value === \'standard\' ? document.getElementById(\'userPermissionsWrapper\').classList.remove(\'d-none\') : document.getElementById(\'userPermissionsWrapper\').classList.add(\'d-none\')">'.END_LINE;
  echo '        <option value="standard"'.($user['type'] === 'standard' ? ' selected="selected"' : '').'>Standard</option>'.END_LINE;
  echo '        <option value="superuser"'.($user['type'] === 'superuser' ? ' selected="selected"' : '').'>Superuser</option>'.END_LINE;
  echo '      </select>'.END_LINE;
  if (!empty($errors['user']['type'])) {
    echo '      <span class="invalid-feedback">'.htmlspecialchars($errors['user']['type']).'</span>'.END_LINE;
  }
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;
  echo '  <div id="userPermissionsWrapper"'.($userId === $_SESSION['LVuser']['_id'] ? ' class="d-none"' : '').'>'.END_LINE;
  $projects = $database->projects->find()->toArray();
  usort($projects, arraySort(['field' => 'priority', 'order' => 'asc']));

  echo '    <h2>Projects Permissions</h2>'.END_LINE;
  foreach ($projects as $project) {
    $projectPermission = !empty($user['permissions']['projects'][(string)$project['_id']]) ? $user['permissions']['projects'][(string)$project['_id']] : '';
    echo '    <div class="mb-3 row hoverBackground">'.END_LINE;
    echo '      <label class="col col-form-label">'.$project['name'].'</label>'.END_LINE;
    echo '      <div class="col text-end">'.END_LINE;
    echo '        <div class="btn-group" role="group" aria-label="Project permissions select">'.END_LINE;
    echo '          <input type="radio" class="btn-check" name="permissions[projects]['.$project['_id'].']" value="admin" id="checkboxPermissionsAdmin_'.$project['_id'].'" autocomplete="off"'.($projectPermission === 'admin' ? ' checked' : '').'>'.END_LINE;
    echo '          <label class="btn btn-outline-primary" for="checkboxPermissionsAdmin_'.$project['_id'].'" title="User can view, clear, insert and delete logs. Additionally they can edit the project settings.">Admin</label>'.END_LINE;
    echo '          <input type="radio" class="btn-check" name="permissions[projects]['.$project['_id'].']" value="maintainer" id="checkboxPermissionsMaintainer_'.$project['_id'].'" autocomplete="off"'.($projectPermission === 'maintainer' ? ' checked' : '').'>'.END_LINE;
    echo '          <label class="btn btn-outline-primary" for="checkboxPermissionsMaintainer_'.$project['_id'].'" title="User can view and clear logs.">Maintainer</label>'.END_LINE;
    echo '          <input type="radio" class="btn-check" name="permissions[projects]['.$project['_id'].']" value="standard" id="checkboxPermissionsStandard_'.$project['_id'].'" autocomplete="off"'.($projectPermission === 'standard' ? ' checked' : '').'>'.END_LINE;
    echo '          <label class="btn btn-outline-primary" for="checkboxPermissionsStandard_'.$project['_id'].'" title="User can view logs.">Standard</label>'.END_LINE;
    echo '          <input type="radio" class="btn-check" name="permissions[projects]['.$project['_id'].']" value="none" id="checkboxPermissionsNone_'.$project['_id'].'" autocomplete="off"'.(empty($projectPermission) ? ' checked' : '').'>'.END_LINE;
    echo '          <label class="btn btn-outline-primary" for="checkboxPermissionsNone_'.$project['_id'].'" title="No access.">None</label>'.END_LINE;
    echo '        </div>'.END_LINE;
    echo '      </div>'.END_LINE;
    echo '    </div>'.END_LINE;
  }

  echo '    <h2>Dashboard Permissions</h2>'.END_LINE;
  $dashboards = $database->dashboards->find(['active' => 1])->toArray();
  usort($dashboards, arraySort(['field' => 'name', 'order' => 'asc']));

  echo '    <div class="mb-3 col dashboardsPermissionsWrapper">'.END_LINE;
  foreach ($dashboards as $dashboard) {
    $checked = !empty($user['permissions']['dashboards']) && in_array((string)$dashboard['_id'], (array)$user['permissions']['dashboards']) ? ' checked="checked"' : '';
    echo '      <input type="checkbox" class="btn-check" name="permissions[dashboards][]" value="'.$dashboard['_id'].'" id="checkboxPermissionsDashboard_'.$dashboard['_id'].'" autocomplete="off"'.$checked.'>'.END_LINE;
    echo '      <label class="btn btn-outline-primary mb-2" for="checkboxPermissionsDashboard_'.$dashboard['_id'].'">'.$dashboard['name'].'</label>'.END_LINE;
  }
  echo '    </div>'.END_LINE;

  echo '    <h2>Additional Permissions</h2>'.END_LINE;
  echo '    <div class="mb-3 col">'.END_LINE;

  $checked = !empty($user['permissions']['additional']) && in_array('error_logs_manage', (array)$user['permissions']['additional']) ? ' checked="checked"' : '';
  echo '      <input type="checkbox" class="btn-check" name="permissions[additional][]" value="error_logs_manage" id="checkboxPermissionsAdditional_error_logs_manage" autocomplete="off"'.$checked.'>'.END_LINE;
  echo '      <label class="btn btn-outline-primary mb-2" for="checkboxPermissionsAdditional_error_logs_manage">Manage error logs</label>'.END_LINE;
  echo '    </div>'.END_LINE;
  echo '  </div>'.END_LINE;
}
echo '  <div class="row align-items-center">'.END_LINE;
echo '    <div class="col text-end"><input type="submit" class="btn btn-success" value="Save'.(empty($user['active']) ? ' and reactivate' : '').'"></div>'.END_LINE;
echo '  </div>'.END_LINE;
echo '</form>'.END_LINE;

if ($userId !== 'new') {
  $keys = cursorToArray($database->keys->find(!$deletedKeysShow ? ['active' => 1, 'user_id' => $userId] : ['user_id' => $userId]));

  echo '<div class="row align-items-center">';
  echo '  <div class="col"><h2>API Keys</h2></div>';
  echo '  <div class="col text-end col-md-auto">'.END_LINE;
  echo '    <a href="#" class="btn btn-outline-primary" title="Add API Key" data-bs-toggle="modal" data-bs-target="#apiKeyCreateModal" role="button"><i class="bi bi-plus buttonIcon"></i></a>'.END_LINE;
  echo '  </div>'.END_LINE;
  echo '</div>'.END_LINE;
  echo '<table id="apiKeysTable" class="table table-hover align-middle">'.END_LINE;
  echo '  <thead>'.END_LINE;
  echo '    <tr>'.END_LINE;
  echo '      <th>Name</th>'.END_LINE;
  if ($deletedKeysShow) {
    echo '      <th>Active</th>'.END_LINE;
  }
  echo '      <th>Token</th>'.END_LINE;
  echo '      <th>Last Used</th>'.END_LINE;
  echo '    </tr>'.END_LINE;
  echo '  </thead>'.END_LINE;
  echo '  <tbody>'.END_LINE;
  if (!empty($keys)) {
    /** @var array{_id: string, user_id: string, name: string, active: int, last_used: float, token: string} $key */
    foreach ($keys as $key) {
      echo '    <tr id="apiKeyRow_'.htmlspecialchars($key['_id'], ENT_QUOTES).'"'.(empty($key['active']) ? ' class="opacity-50"' : '').'>'.END_LINE;
      echo '      <td>'.$key['name'].'</td>'.END_LINE;
      if ($deletedKeysShow) {
        echo '      <td>'.(!empty($key['active']) ? 'Active' : 'Inactive').'</td>'.END_LINE;
      }
      echo '      <td>'.END_LINE;
      echo '        <span id="tokenPlaceholder_'.htmlspecialchars($key['_id'], ENT_QUOTES).'">***</span>'.END_LINE;
      echo '        <span id="tokenValue_'.htmlspecialchars($key['_id'], ENT_QUOTES).'" class="d-none">'.htmlspecialchars($key['token'], ENT_QUOTES).'</span>'.END_LINE;
      echo '      </td>'.END_LINE;
      echo '      <td class="actionsCover">'.END_LINE;
      echo '        <div class="actions">'.END_LINE;
      echo '          <a href="#" class="btn btn-outline-secondary" title="View token" onclick="apiKeyShow(\''.htmlspecialchars($key['_id'], ENT_QUOTES).'\'); return false;"><i class="bi bi-eye buttonIcon"></i></a>'.END_LINE;
      echo '          <a href="#" class="btn btn-outline-secondary" title="Copy to clipboard" onclick="navigator.clipboard.writeText(\''.htmlspecialchars($key['token'], ENT_QUOTES).'\'); messageShow(\'Copied to clipboard.\', \'success\'); return false;"><i class="bi bi-copy buttonIcon"></i></a>'.END_LINE;
      if (!empty($key['active'])) {
        echo '          <a href="#" class="btn btn-outline-danger" title="Deactivate API Key" onclick="apiKeyDeactivate(\''.htmlspecialchars($key['_id'], ENT_QUOTES).'\'); return false;"><i class="bi bi-trash3 buttonIcon"></i></a>'.END_LINE;
      }
      echo '        </div>'.END_LINE;
      echo !empty($key['last_used']) ? gmdate('Y-m-d H:i e', (int)($key['last_used'] / 1000)) : 'Never';
      echo '      </td>'.END_LINE;
      echo '    </tr>'.END_LINE;
    }
  } else {
    echo '    <tr id="noApiKeysMessage">'.END_LINE;
    echo '      <td colspan="'.(3 + ($deletedKeysShow ? 1 : 0)).'" class="text-center">There are no Keys. <a href="#" class="btn btn-sm btn-outline-primary" title="Add API Key" data-bs-toggle="modal" data-bs-target="#apiKeyCreateModal" role="button"><i class="bi bi-plus"></i></a></td>'.END_LINE;
    echo '    </tr>'.END_LINE;
  }
  echo '  </tbody>'.END_LINE;
  echo '</table>'.END_LINE;
  echo apiKeyCreateModal($userId, $deletedKeysShow);
}

echo floatingSaveButtonHtml('formUserEdit');

echo pageFooter();
