<?php

global $dashboard;

$database = getDatabaseOrDie();

if (isSuperuser()) {
  $projects = $database->projects->find()->toArray();
} else {
  $projectIds = array_map(fn ($projectId) => new MongoDB\BSON\ObjectID((string)$projectId), array_keys($_SESSION['LVuser']['permissions']['projects']));
  $projects = $database->projects->find(['_id' => ['$in' => $projectIds]])->toArray();
}
$projectLogsFilters = iterator_to_array($database->parser_settings->find(['handle' => 'filters', 'active' => 1, 'favourite' => 1]));
$projectListGraphSettings = projectListGraphSettings(array_map(fn ($project) => $project['handle'], $projects));
$projectListData = projectListData($projects);

$request = $_REQUEST;
unset($request['page']);
if (empty($request['sort'])) {
  $request['sort'] = ['field' => 'priority', 'order' => 'asc'];
}
$sort = $request['sort'];
if (!empty($projectListData)) {
  usort($projectListData, arraySort($sort));
}

echo pageHeader('Projects', 'projects', projectListSubmenu($projectListData));

$projectListHandlesMarkAllowed = array_column(array_filter($projectListData, fn ($logInfo) => !empty($logInfo['userCanMark'])), 'handle');
if (!empty($projectListHandlesMarkAllowed)) {
  echo logsMarkModal($projectListHandlesMarkAllowed);
}
echo projectLogsSearchModal($projectListData);
if (count($projectListData) > 0) {
  echo projectLogsGraphModal($projectListData);
  echo projectLogsGraphSaveModal();
}

echo projectListPieGraphsHtml($projectListData, $projectListGraphSettings['pie']);

echo projectListTableHtml($projectListData, $sort, $projectListGraphSettings['row'], $projectLogsFilters);

echo pageFooter();
