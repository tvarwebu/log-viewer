<?php

$database = getDatabaseOrDie();

$errors = ['global' => [], 'user' => [], 'projects' => [], 'dashboards' => []];

if (empty($_POST)) {
  $content = file_get_contents('php://input');
  if (!empty($content)) {
    $_POST = json_decode($content, true);
  }
}

if (empty($_POST)) {
  htmlError('Missing data to save.', 'User Edit');
}

$userId = !empty($_REQUEST['user_id']) ? sanitizeStringInput($_REQUEST['user_id']) : 'new';
if ($userId === 'me') {
  $userId = (string)$_SESSION['LVuser']['_id'];
}
$userCurrent = null;
if ($userId !== 'new') {
  $userCurrent = $database->users->findOne(['_id' => new MongoDB\BSON\ObjectID($userId)]);
  if (empty($userCurrent)) {
    htmlError('Supplied unknown ID for user.', 'User Edit');
  }
}

$userTypesAllowed = ['standard'];
if (isSuperuser()) {
  $userTypesAllowed[] = 'superuser';
}

/** @var array{active: int, forename?: string, surname?: string, username?: string, type?: string, password?: string, permissions: array{projects: array<string, string>, dashboards: string[]}} $userPost */
$userPost = array(
  'active' => 1,
  'permissions' => array(
    'projects' => [],
    'dashboards' => [],
    'additional' => [],
  ),
);

$keysValidate = ['forename', 'surname', 'username', 'type'];
if ($userId === 'new') {
  $keysValidate[] = 'password';
} else {
  unset($_POST['password'], $_POST['passwordCheck']);
}
foreach ($keysValidate as $key) {
  $userPost[$key] = !empty($_POST[$key]) && is_string($_POST[$key]) ? sanitizeStringInput($_POST[$key]) : '';
  if (empty($userPost[$key])) {
    $errors['user'][$key] = 'Missing '.$key.'.';
  } elseif ($key === 'type' && !in_array($userPost[$key], $userTypesAllowed)) {
    $errors['user'][$key] = 'Invalid value supplied for type.';
  }
}

if ($userId === 'new' && !empty($userPost['username'])) {
  $user = $database->users->findOne(['username' => $userPost['username']]);
  if (!empty($user)) {
    $errors['user']['username'] = 'Username already exists.';
  }
}

if ($userId === 'new' && !empty($userPost['password'])) {
  if (empty($_POST['passwordCheck']) || !is_string($_POST['passwordCheck'])) {
    $errors['user']['passwordCheck'] = 'Missing passwords confirmation.';
  } elseif ($userPost['password'] !== sanitizeStringInput($_POST['passwordCheck'])) {
    $errors['user']['passwordCheck'] = 'Passwords do not match.';
  } else {
    $userPost['password'] = password_hash($userPost['password'], PASSWORD_DEFAULT);
  }
}

if (!empty($_POST['permissions']) && !empty($userPost['type']) && $userPost['type'] === 'standard') {
  if (!empty($_POST['permissions']['projects'])) {
    $projects = $database->projects->find()->toArray();
    $projectsIds = array_map(fn ($project) => (string)$project['_id'], $projects);
    foreach ($_POST['permissions']['projects'] as $projectId => $permission) {
      if (empty($permission) || $permission === 'none') {
        continue;
      }
      if (!in_array($projectId, $projectsIds)) {
        $errors['global'][] = 'Invalid Project supplied.';
        continue;
      } elseif (!in_array($permission, ['standard', 'maintainer', 'admin'])) {
        $errors['projects'][] = 'Invalid permission type supplied.';
        continue;
      }
      $userPost['permissions']['projects'][$projectId] = $permission;
    }
  }

  $permissionsDashboards = !empty($_POST['permissions']['dashboards']) ? array_unique(array_filter(array_map('sanitizeStringInput', (array)$_POST['permissions']['dashboards']))) : [];
  if (!empty($permissionsDashboards)) {
    $dashboards = $database->dashboards->find();
    $dashboardsAssoc = [];
    foreach ($dashboards as $dashboard) {
      $dashboardsAssoc[(string)$dashboard['_id']] = $dashboard;
    }

    $userProjectIds = array_keys($userPost['permissions']['projects']);
    foreach ($permissionsDashboards as $dashboardId) {
      if (empty($dashboardsAssoc[$dashboardId])) {
        $errors['dashboards'][] = 'Invalid Dashboard ('.$dashboardId.') supplied.';
        continue;
      }
      if (empty($dashboardsAssoc[$dashboardId]['active'])) {
        continue;
      }

      $projectPermissions = array_intersect($userProjectIds, (array)$dashboardsAssoc[$dashboardId]['project_ids']);
      if (empty($projectPermissions)) {
        $errors['dashboards'][] = 'User does not have any permission for projects in '.$dashboardsAssoc[$dashboardId]['name'].' dashboard.';
        continue;
      }
      $userPost['permissions']['dashboards'][] = $dashboardId;
    }
  }

  if (!empty($_POST['permissions']['additional'])) {
    $permissionsAdditionalValid = ['error_logs_manage'];
    $permissionsAdditional = array_filter(array_unique(sanitizeArray($_POST['permissions']['additional'])));
    $permissionsAdditionalInvalid = array_diff($permissionsAdditional, $permissionsAdditionalValid);
    if (!empty($permissionsAdditionalInvalid)) {
      $errors['additional'][] = 'Invalid additional permissions supplied: '.implode($permissionsAdditionalInvalid);
    } else {
      $userPost['permissions']['additional'] = $permissionsAdditional;
    }
  }
}

if (!empty(array_sum(array_map('count', $errors)))) {
  includePageAndDie('user_edit', 400);
}

if (!is_null($userCurrent)) {
  $userUpdate = [];
  $userCurrent = bsonDocumentToArray($userCurrent);
  foreach ($userPost as $key => $value) {
    if (!compareValues($userCurrent[$key], $value)) {
      $userUpdate[$key] = $value;
    }
  }

  if (!empty($userUpdate)) {
    $resultUserUpdate = $database->users->updateOne(['_id' => new MongoDB\BSON\ObjectID($userId)], ['$set' => $userUpdate]);
    if ($resultUserUpdate->getMatchedCount() === 0) {
      $errors['global'][] = 'Could not update User.';
      includePageAndDie('user_edit', 400);
    }
  }
} else {
  $resultUserCreate = $database->users->insertOne($userPost);
  if ($resultUserCreate->getInsertedCount() === 0) {
    $errors['global'][] = 'Could not add User to DB.';
    includePageAndDie('user_edit', 400);
  }
}

messageAdd('User '.(!is_null($userCurrent) ? ' updated' : 'created'), 'success', 'user_save');
Header('Location: '.getCorrectUrl('users'));
die();
