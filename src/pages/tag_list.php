<?php

$database = getDatabaseOrDie();
$deletedShow = !empty($_REQUEST['deleted_show']);
$tagsFilter = !$deletedShow ? ['active' => 1] : [];
if (!isSuperuser()) {
  $tagsFilter['project_id'] = ['$in' => array_keys($_SESSION['LVuser']['permissions']['projects'])];
}
$tags = cursorToArray($database->tags->find($tagsFilter));
$sort = !empty($_REQUEST['sort']) ? $_REQUEST['sort'] : ['field' => 'name', 'order' => 'asc'];
if (!empty($tags)) {
  usort($tags, arraySort($sort));
}

$projectIds = array_map(fn ($projectId) => new MongoDB\BSON\ObjectID($projectId), array_values(array_unique(array_column($tags, 'project_id'))));
$projects = !empty($projectIds) ? $database->projects->find(['_id' => ['$in' => $projectIds]])->toArray() : [];
$projectsAssoc = [];
$logsTaggedPerTagCount = [];
foreach ($projects as $project) {
  $projectsAssoc[(string)$project['_id']] = $project;
  $logsTagged = $database->{'logs_'.$project['_id']}->find(['__tag_id' => ['$exists' => true]], ['__tag_id' => true]);
  foreach ($logsTagged as $logTagged) {
    if (!isset($logsTaggedPerTagCount[$logTagged['__tag_id']])) {
      $logsTaggedPerTagCount[$logTagged['__tag_id']] = 0;
    }
    $logsTaggedPerTagCount[$logTagged['__tag_id']]++;
  }
}

$links = array(
  ['url' => 'tags/new/edit', 'value' => '<i class="bi bi-plus buttonIcon"></i>', 'class' => 'btn-outline-primary', 'title' => 'Add User'],
  ['url' => 'tags?deleted_show=1', 'value' => 'Show Deleted', 'class' => 'btn-outline-secondary', 'title' => 'Show Deleted'],
);
echo pageHeader('Tags', 'tags', $links);

echo '<table class="table table-hover align-middle">'.END_LINE;
echo '  <thead class="table-sortable-header">'.END_LINE;
echo '    <tr>'.END_LINE;
echo tableHeaderSortable('name', 'Name', $sort);
if ($deletedShow) {
  echo tableHeaderSortable('active', 'Active', $sort);
}
echo tableHeaderSortable('active_until', 'Active Until', $sort);
echo tableHeaderSortable('project_id', 'Project', $sort);
echo tableHeaderSortable('action', 'Action', $sort);
echo '      <th>Logs tagged</th>'.END_LINE;
echo '    </tr>'.END_LINE;
echo '  </thead>'.END_LINE;
echo '  <tbody>'.END_LINE;
if (!empty($tags)) {
  /** @var array{_id: string, name: string, active: int, active_until: int, action: string, project_id: string, rules: array<int, array{field: string, type: string, value: string}>} $tag */
  foreach ($tags as $tag) {
    echo '    <tr'.(empty($tag['active']) ? ' class="opacity-50"' : '').'>'.END_LINE;
    echo '      <td>'.$tag['name'].'</td>'.END_LINE;
    if ($deletedShow) {
      echo '      <td>'.(!empty($tag['active']) ? 'Active' : 'Inactive').'</td>'.END_LINE;
    }
    echo '      <td>'.gmdate('Y-m-d H:i', $tag['active_until']).'</td>'.END_LINE;
    echo '      <td class="actionsCover">'.END_LINE;
    if (!empty($projectsAssoc[$tag['project_id']])) {
      echo '        <a href="'.getCorrectUrl('projects/'.$tag['project_id'].'/edit').'" class="btn btn-sm mb-2 btn-outline-secondary">'.$projectsAssoc[$tag['project_id']]['name'].'</a>'.END_LINE;
    } else {
      echo 'Unknown project';
    }
    echo '      </td>'.END_LINE;
    echo '      <td>'.ucfirst($tag['action']).'</td>'.END_LINE;
    echo '      <td class="actionsCover">'.END_LINE;
    echo '        <div class="actions">'.END_LINE;
    echo '          <a class="btn btn-outline-primary" href="'.getCorrectUrl('tags/'.$tag['_id'].'/edit').'" title="Edit Tag"><i class="bi bi-pencil buttonIcon"></i></a>'.END_LINE;
    echo '        </div>'.END_LINE;
    echo !empty($logsTaggedPerTagCount[$tag['_id']]) ? $logsTaggedPerTagCount[$tag['_id']] : 0;
    echo '      </td>'.END_LINE;
    echo '    </tr>'.END_LINE;
  }
} else {
  echo '    <tr>'.END_LINE;
  echo '      <td colspan="5" class="text-center">There are no Tags. <a href="'.getCorrectUrl('tags/new/edit').'" class="btn btn-sm btn-outline-primary" title="Add Tag"><i class="bi bi-plus"></i></a></td>'.END_LINE;
  echo '    </tr>'.END_LINE;
}
echo '  </tbody>'.END_LINE;
echo '</table>'.END_LINE;

echo pageFooter();
