<?php

global $urlPrefix;

unset($_POST['password']);

sessionDestroyCookieUnset();

messageAdd('Logged out.', 'info', 'logout');
Header('Location: '.getCorrectUrl(''));
die();
