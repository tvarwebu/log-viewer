<?php

$database = getDatabaseOrDie();

$rateLimits = array(
  ['name' => 'Invalid Login: Global', 'placeholder' => 'login_invalid_global'],
  ['name' => 'Invalid Login: IP', 'placeholder' => 'login_invalid_ip_{IP}'],
  ['name' => 'Invalid Login: USERNAME', 'placeholder' => 'login_invalid_username_{USERNAME}'],
);
$rateLimitSelected = !empty($_POST['rateLimit']) ? sanitizeStringInput($_POST['rateLimit']) : '';
$memcachedKeySelected = !empty($_POST['memcachedKey']) ? sanitizeStringInput($_POST['memcachedKey']) : '';

echo pageHeader('Rate limits', 'rate-limits');

echo '<form action="'.getCorrectUrl('rate-limits').'" method="post">'.END_LINE;
echo '  <div class="d-flex">'.END_LINE;
echo '    <select id="selectRateLimit" name="rateLimit" class="form-select me-2" onchange="document.getElementById(\'inputMemcachedKey\').value = this.value">'.END_LINE;
echo '      <option value="">-- select --</option>'.END_LINE;
foreach ($rateLimits as $rateLimit) {
  echo '      <option value="'.$rateLimit['placeholder'].'"'.($rateLimitSelected === $rateLimit['placeholder'] ? ' selected="selected"' : '').'>'.$rateLimit['name'].'</option>'.END_LINE;
}
echo '    </select>'.END_LINE;
echo '    <input id="inputMemcachedKey" type="text" size="50" name="memcachedKey" value="'.htmlspecialchars($memcachedKeySelected, ENT_QUOTES).'" class="form-control me-2" placeholder="Select item from the dropdown">'.END_LINE;
echo '    <input type="submit" class="btn btn-success" value="Check this key">'.END_LINE;
echo '  </div>'.END_LINE;
echo '  <div>'.END_LINE;
echo '    <button type="button" title="Replace the {IP} with your IP address in memcached key" class="btn" onclick="placeholderReplace(\'inputMemcachedKey\', \'{IP}\', \''.htmlspecialchars($_SERVER['REMOTE_ADDR'], ENT_QUOTES).'\')">Your IP: '.htmlspecialchars($_SERVER['REMOTE_ADDR'], ENT_QUOTES).'</button>'.END_LINE;
echo '    <button type="button" title="Replace the {USERNAME} with your USERNAME address in memcached key" class="btn" onclick="placeholderReplace(\'inputMemcachedKey\', \'{USERNAME}\', \''.htmlspecialchars($_SESSION['LVuser']['username'], ENT_QUOTES).'\')">Your USERNAME: '.htmlspecialchars($_SESSION['LVuser']['username'], ENT_QUOTES).'</button>'.END_LINE;
echo '  </div>'.END_LINE;
echo '</form>'.END_LINE;

if (!empty($memcachedKeySelected)) {
  $memcachedValue = memcachedGet($memcachedKeySelected);
  echo '<div class="d-flex">'.END_LINE;
  echo '  <h2>Results for '.htmlspecialchars($memcachedKeySelected).' ('.(!empty($memcachedValue) ? count((array)$memcachedValue) : 0).')</h2>';
  if (!empty($memcachedValue)) {
    echo '  <span class="ms-2">'.END_LINE;
    echo '    <a href="#" onclick="memcachedKeyDelete(\''.htmlspecialchars($memcachedKeySelected, ENT_QUOTES).'\'); return false" class="btn btn-outline-danger" title="Delete Memcached key" role="button"><i class="bi bi-trash3 buttonIcon"></i></a>'.END_LINE;
    echo '  </span>'.END_LINE;
  }
  echo '</div>'.END_LINE;
  if (is_array($memcachedValue) || empty($memcachedValue)) {
    echo '<ul>'.END_LINE;
    if (!empty($memcachedValue)) {
      foreach ($memcachedValue as $value) {
        echo '  <li>'.(!empty($value['time']) && is_int($value['time']) ? gmdate('Y-m-d H:i:s', $value['time']) : 'value does not have time specified').'</li>'.END_LINE;
      }
    } else {
      echo '  <li>value is empty</li>'.END_LINE;
    }
    echo '</ul>'.END_LINE;
  } else {
    trigger_error('Unknown value saved in rate limit key: '.$memcachedKeySelected.', value: '.print_r(print_r($memcachedValue), true), E_USER_WARNING);
    echo '<div class="alert alert-danger text-center">Unknown value saved in key</div>'.END_LINE;
    echo '<pre>'.END_LINE;
    print_r($memcachedValue);
    echo '</pre>'.END_LINE;
  }
} else {
  echo '<p class="text-center">Start by selecting specific Rate limit</p>'.END_LINE;
}

echo pageFooter();
