function fieldRowAdd(name, type, required) {
  if (typeof window.fieldRowCounter === 'undefined') {
    window.fieldRowCounter = 0
  }
  let rowNew = false
  let row = document.querySelector('input[name$="[name]"][value="' + name + '"]')?.closest('tr')
  if (!row) {
    rowNew = true
    window.fieldRowCounter++
    row = document.getElementById('rowFieldsTemplate').content.getElementById('rowFieldsNew').cloneNode(true)
    row.setAttribute('id', 'new_' + window.fieldRowCounter)
  }
  const nodes = row.querySelectorAll('input, select')
  nodes.forEach(node => {
    node.setAttribute('name', node.getAttribute('name').replace('__replace__', 'new_' + window.fieldRowCounter))
    if (name && node.getAttribute('name').match(/\[name\]/)) {
      node.value = name
    } else if (type && node.getAttribute('name').match(/\[type\]/)) {
      node.value = type
    } else if (required && node.getAttribute('name').match(/\[required\]/)) {
      node.checked = true
    }
  })
  if (rowNew) {
    const links = row.querySelectorAll('a')
    links.forEach(link => {
      link.setAttribute('onclick', link.getAttribute('onclick').replace('__replace__', 'new_' + window.fieldRowCounter))
    })
    document.getElementById('tableFields').getElementsByTagName('tbody')[0].appendChild(row)
  }
}

function tableRowRemove(id, itemName) {
  if (confirm('Are you sure you want to remove this ' + itemName + '?')) {
    document.getElementById(id).remove()
  }
}

function projectDeleteConfirm(projectName) {
  if (confirm('Are you sure you want to delete ' + projectName + ' and all related Logs?')) {
    return true
  }
  return false
}

function logMarkedDelete(projectHandle, projectName) {
  if (!projectHandle) {
    messageShow('Missing Project handle', 'error')
    return
  }
  if (!confirm('Are you sure you want to delete marked Logs for ' + projectName + '?')) {
    return
  }

  asyncRequest('api/logs/delete?project_handles=' + projectHandle).then(json => {
    if (!json) {
      return
    }

    messageShow(json.message, json.result)
    document.getElementById('button_' + projectHandle + '_logs_delete').remove()
    if (projectHandle === '__all') {
      Array.from(document.getElementsByClassName('logs-delete')).forEach(button => {
        button.remove()
      })
    }
  })
}

function handleFormSubmitValidate() {
  const forms = document.querySelectorAll('.needs-validation')

  Array.from(forms).forEach(form => {
    form.addEventListener('submit', event => {
      if (!form.checkValidity()) {
        event.preventDefault()
        event.stopPropagation()
      }

      form.classList.add('was-validated')
    }, false)
  })
}

function projectFieldsExport() {
  let name = 'fields'
  const formData = new FormData(document.getElementById('formProjectEdit'))
  const data = {}
  formData.forEach((value, key) => {
    if (key.startsWith('fields')) {
      const found = key.matchAll(/fields\[((new_)?\d+)\]\[(name|type|required)\]/g).toArray()
      if (found[0]) {
        if (!data[found[0][1]]) {
          data[found[0][1]] = {}
        }
        data[found[0][1]][found[0][3]] = value
      } else {
        console.warn('Could no parse key', key)
      }
    } else if (key === 'name') {
      name = value + ' fields'
    }
  })

  const url = window.URL.createObjectURL(new Blob([JSON.stringify(Object.values(data), null, 2)], { type: 'application/json' }))
  const anchor = document.createElement('a')
  anchor.href = url
  anchor.download = name + '.json'
  anchor.click()
  window.URL.revokeObjectURL(url)
}

function projectFieldsImport() {
  const fileInput = document.getElementById('fieldsImportInput')
  if (!fileInput.files[0]) {
    console.error('Missing file for file upload.')
    return
  }
  const file = fileInput.files[0]
  file.text().then(text => {
    try {
      const rowsCurrent = document.getElementById('tableFields').getElementsByTagName('tr')
      if (rowsCurrent.length === 2) {
        rowsCurrent[1].remove()
      }
      const json = JSON.parse(text)
      json.forEach(field => {
        fieldRowAdd(field.name, field.type, typeof field.required !== 'undefined')
      })
      const fieldsImportModal = bootstrap.Modal.getInstance('#fieldsImportModal')
      fieldsImportModal.hide()
    } catch (error) {
      console.error(error)
    }
  })
}

function getPreferredTheme() {
  const storedTheme = localStorage.getItem('theme')
  if (storedTheme) {
    return storedTheme
  }

  return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
}

function themeSwitcherUpdate(theme) {
  if (theme === 'light') {
    document.getElementById('themeSwitcherLight').classList.add('d-none')
    document.getElementById('themeSwitcherDark').classList.remove('d-none')
  } else {
    document.getElementById('themeSwitcherLight').classList.remove('d-none')
    document.getElementById('themeSwitcherDark').classList.add('d-none')
  }
}

function themeSet(theme) {
  themeSwitcherUpdate(theme)
  document.documentElement.setAttribute('data-bs-theme', theme)
  localStorage.setItem('theme', theme)
}

var messageCounter = 0

function messageShow(message, type, id, timeout = 5000) {
  const classMapping = {
    info: 'text-bg-info',
    warning: 'text-bg-warning',
    error: 'text-bg-danger',
    success: 'text-bg-success',
  }

  if (!id) {
    id = 'message_js_' + ++messageCounter
  }
  if (!['info', 'warning', 'error', 'success'].includes(type)) {
    type = 'info'
  }
  html = '<div id="' + id + '" class="toast align-items-center ' + classMapping[type] + '" role="alert" aria-live="assertive" aria-atomic="true"' + (timeout === -1 ? ' data-bs-autohide="false"' : ' data-bs-delay="' + timeout + '"') + '>'
  html += '  <div class="d-flex">'
  html += '    <div class="toast-body">' + message + '</div>'
  html += '    <button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>'
  html += '  </div>'
  html += '</div>'

  const elementCurrent = document.getElementById(id)
  if (elementCurrent) {
    elementCurrent.outerHTML = html
  } else {
    document.getElementById('toastContainerMain').insertAdjacentHTML('beforeend', html)
  }

  const toast = bootstrap.Toast.getOrCreateInstance(document.getElementById(id))
  toast.show()
}

function setCookie(name, value, expires, path, domain, secure) {
  let cookie = name + '=' + encodeURI(value)
  if (expires) {
    cookie += '; expires=' + (expires instanceof Date ? expires.toGMTString() : expires)
  }
  if (path) {
    cookie += '; path=' + path
  }
  if (domain) {
    cookie += '; domain=' + domain
  }
  if (secure) {
    cookie += '; secure'
  }

  document.cookie = cookie
}

function getCookie(name) {
  const nameEq = name + "="
  const cookies = document.cookie.split(';')
  for (let cookie of cookies) {
    while (cookie.charAt(0) === ' ') {
      cookie = cookie.substring(1, cookie.length)
    }
    if (cookie.indexOf(nameEq) == 0) {
      let cookieString = decodeURI(cookie.substring(nameEq.length, cookie.length))
      while(cookieString.match(/\+/)) { // replace all + with ' ' space - because of different interpretation in PHP
        cookieString = cookieString.replace(/\+/,' ')
      }
      return cookieString
    }
  }

  return null
}

function deleteCookie(name, path, domain) {
  if (getCookie(name)) {
    setCookie(name, '', 'Thu, 01-Jan-1970 00:00:01 GMT', path, domain)
  }
}

function floatingSaveButtonInit(formId) {
  document.addEventListener('DOMContentLoaded', () => {
    const formElements = document.querySelectorAll('#' + formId + ' input, ' + '#' + formId + ' select')
    formElements.forEach(formElement => {
      formElement.addEventListener('change', () => {
        document.querySelector('.floating-save-button').classList.add('editor-has-changed')
      })
    })

    document.querySelector('.handler-form-submit').addEventListener('click', () => {
      document.getElementById(formId).submit()
      document.querySelector('.floating-save-button .save-button').classList.add('d-none')
      document.querySelector('.floating-save-button .save-spinner').classList.remove('d-none')
    })
  })
}

function logsMarkToBeDeleted(handles) {
  const date = document.getElementById('logsMarkDateInput').value
  const dateObj = date ? new Date(date) : null
  if (!dateObj || dateObj.getFullYear() < 2010) {
    document.getElementById('logsMarkError').classList.remove('d-none')
    return
  }
  document.getElementById('logsMarkError').classList.add('d-none')

  if (!confirm('Are you sure you want to mark all logs older then ' + date + ' as to be deleted?')) {
    return
  }

  asyncRequest('api/logs/clear?project_handles=' + handles.join(',') + '&filter[_id__timestamp__less]=' + parseInt(dateObj.getTime() / 1000)).then(json => {
    if (!json) {
      return
    }

    messageShow(json.message, json.result)
  })
}

async function asyncRequest(url, postData) {
  let fetchParams = null
  if (postData) {
    fetchParams = {
      method: 'POST',
      credentials: 'include',
      body: JSON.stringify(postData),
      headers: {
        'Content-Type': 'application/json'
      }
    }
  }

  if (typeof window.correctUrl !== 'undefined' && !url.startsWith(window.correctUrl)) {
    url = window.correctUrl + url
  }

  let responseClone = null
  return fetch(url, fetchParams).then(response => {
    responseClone = response.clone()
    if (response.status !== 200) {
      response.json().then((json) => {
        if (json.message) {
          window.messageShow(json.message, 'error')
        } else {
          window.messageShow('There was problem with communication with the API.', 'error')
          console.error(json)
        }
      }).catch((error) => {
        console.error(error)
        responseClone.text().then((responseText) => {
          window.messageShow(responseText || 'There was problem with communication with the API.', 'error')
        })
      })
      return null
    }
    return response.json()
  }).catch((error) => {
    console.error(error)
    if (responseClone) {
      responseClone.text().then((responseText) => {
        window.messageShow(responseText || 'There was problem with communication with the API.', 'error')
      })
    } else {
      window.messageShow('There was problem with communication with the API.', 'error')
    }
    return null
  })
}

function logsRowGraphShow(projectHandle, logsRowGraphSettings) {
  asyncRequest('api/logs/info?project_handles=' + projectHandle + '&count_per_day=1').then(json => {
    if (!json) {
      return
    }
    logsRowGraphCreate(projectHandle, json?.items.length > 0 ? json?.items[0] : null, logsRowGraphSettings)
  })
}

function logsRowGraphCreate(projectHandle, logsInfo, logsRowGraphSettings) {
  if (!logsRowGraphSettings) {
    logsRowGraphSettings = {
      favourite: 0,
      handle: 'logsRowGraph',
      settings: {
        project_handle: projectHandle
      },
    }
  }

  html = '<div class="actions">'
  html += '  <a class="logsRowGraphUnsetFavouriteButton ' + (logsRowGraphSettings.favourite === 0 ? 'd-none' : '') + ' btn btn-outline-secondary float-end me-2" href="#" onclick="logsRowGraphFavouriteUpdate(' + escapeHtml(JSON.stringify(logsRowGraphSettings)) + ', 0); return false;" title="Unset Graph as favourite to not be shown automatically."><i class="bi bi-star-fill buttonIcon"></i></a>'
  html += '  <a class="logsRowGraphSetFavouriteButton ' + (logsRowGraphSettings.favourite === 1 ? 'd-none' : '') + ' btn btn-outline-secondary float-end me-2" href="#" onclick="logsRowGraphFavouriteUpdate(' + escapeHtml(JSON.stringify(logsRowGraphSettings)) + ', 1); return false;" title="Set Graph as favourite to be shown automatically."><i class="bi bi-star buttonIcon"></i></a>'
  html += '</div>'
  
  if (logsInfo && logsInfo?.count > 0) {
    const graphWidth = 150
    const graphHeight = 50
    const xAxisMultiplier = Math.max(1, Math.floor(graphWidth / Object.values(logsInfo.count_per_day).length))
    const points = []
    let index = 0
    for (const date in logsInfo.count_per_day) {
      if (Object.hasOwnProperty.call(logsInfo.count_per_day, date)) {
        points.push((index * xAxisMultiplier) + ',' + (graphHeight - 1 - (logsInfo.count_per_day[date] / logsInfo.count_per_day_max * (graphHeight - 2))))
        index++
      }
    }
  
    const svgWidth = (points.length - 1) * xAxisMultiplier
    const svgHeight = svgWidth / graphWidth * graphHeight
    html += '<div class="lineChartWrapper" title="Graph for number of new Logs per day">'
    html += '  <svg width="' + graphWidth + '" height="' + graphHeight + '" viewBox="0 0 ' + svgWidth + ' ' + svgHeight + '">'
    html += '    <polyline fill="none" stroke="currentColor" points="' + points.join(' ') + '" stroke-width="2" stroke-linejoin="round"/>'
    html += '  </svg>'
    html += '</div>'
  }  else {
    html += '-- no data --'
  }

  for (const element of document.querySelectorAll('.logsGraph.d-none')) {
    element.classList.remove('d-none')
  }
  document.querySelector('#row_' + projectHandle + ' .logsGraph').insertAdjacentHTML('beforeend', html)
}

function logsRowGraphFavouriteUpdate(logsRowGraphSettings, setFavourite) {
  logsRowGraphSettings.favourite = setFavourite ? 1 : 0
  asyncRequest('api/settings/' + (logsRowGraphSettings._id ? logsRowGraphSettings._id : 'new') + '/save', logsRowGraphSettings).then(json => {
    if (!json) {
      return
    }

    if (!logsRowGraphSettings._id) {
      logsRowGraphSettings._id = json.id
      document.querySelector('#row_' + logsRowGraphSettings.settings.project_handle + ' .logsRowGraphUnsetFavouriteButton').outerHTML = '<a class="logsRowGraphUnsetFavouriteButton ' + (logsRowGraphSettings.favourite === 0 ? 'd-none' : '') + ' btn btn-outline-secondary float-end me-2" href="#" onclick="logsRowGraphFavouriteUpdate(' + escapeHtml(JSON.stringify(logsRowGraphSettings)) + ', 0); return false;" title="Unset Graph as favourite to not be shown automatically."><i class="bi bi-star-fill buttonIcon"></i></a>'
      document.querySelector('#row_' + logsRowGraphSettings.settings.project_handle + ' .logsRowGraphSetFavouriteButton').outerHTML = '<a class="logsRowGraphSetFavouriteButton ' + (logsRowGraphSettings.favourite === 1 ? 'd-none' : '') + ' btn btn-outline-secondary float-end me-2" href="#" onclick="logsRowGraphFavouriteUpdate(' + escapeHtml(JSON.stringify(logsRowGraphSettings)) + ', 1); return false;" title="Set Graph as favourite to be shown automatically."><i class="bi bi-star buttonIcon"></i></a>'
    }

    if (setFavourite) {
      document.querySelector('#row_' + logsRowGraphSettings.settings.project_handle + ' .logsRowGraphSetFavouriteButton').classList.add('d-none')
      document.querySelector('#row_' + logsRowGraphSettings.settings.project_handle + ' .logsRowGraphUnsetFavouriteButton').classList.remove('d-none')
      messageShow('Projects Graph was set as favourite.', 'success')
    } else {
      document.querySelector('#row_' + logsRowGraphSettings.settings.project_handle + ' .logsRowGraphSetFavouriteButton').classList.remove('d-none')
      document.querySelector('#row_' + logsRowGraphSettings.settings.project_handle + ' .logsRowGraphUnsetFavouriteButton').classList.add('d-none')
      messageShow('Projects Graph was unset as favourite.', 'success')
    }
  })
}

function projectsGraphShow() {
  const projectHandles = Array.from(document.querySelectorAll('#logsGraphSelect option:checked')).map(option => option.value).filter(handle => typeof handle === 'string' && handle !== '')
  if (projectHandles.length < 2) {
    document.getElementById('projectLogsGraphError').classList.remove('d-none')
    return
  }
  document.getElementById('projectLogsGraphError').classList.add('d-none')

  asyncRequest('api/logs/info?project_handles=' + projectHandles.join(',')).then(json => {
    if (!json) {
      return
    }
    if (!json.items || json.items.length === 0) {
      messageShow('There are no data for Logs graph.', 'info')
      return
    }
    projectsGraphCreate(json.items)
    bootstrap.Modal.getOrCreateInstance('#projectLogsGraphModal').hide()
  })
}

function projectLogsGraphSave() {
  const graphHandles = document.getElementById('graphNameHandles').value.trim().split(',')
  const graphName = document.getElementById('graphNameInput').value.trim()
  if (!graphName) {
    document.getElementById('projectLogsGraphSaveError').classList.remove('d-none')
    return
  }
  document.getElementById('projectLogsGraphSaveError').classList.add('d-none')

  const graphSettings = { handle: 'projectLogsGraph', settings: { name: graphName, 'project_handles': graphHandles }, active: 1, favourite: 0 }
  asyncRequest('api/settings/new/save', graphSettings).then(json => {
    if (!json) {
      return
    }

    graphSettings._id = json.id
    const wrapperId = document.getElementById('graphWrapperId').value
    document.querySelector('#' + wrapperId + ' .pieChartActions').innerHTML = graphActionsHtml(graphSettings)
    document.getElementById(wrapperId).id = 'graph_' + graphSettings._id

    document.getElementById('graphWrapperId').value = ''
    document.getElementById('graphNameHandles').value = ''
    document.getElementById('graphNameInput').value = ''
    bootstrap.Modal.getOrCreateInstance('#projectLogsGraphSaveModal').hide()
    messageShow('Projects Graph was saved.', 'success')
  })
}

function projectLogsGraphFavouriteUpdate(graphSettings, setFavourite) {
  graphSettings.favourite = setFavourite ? 1 : 0
  asyncRequest('api/settings/' + graphSettings._id + '/save', graphSettings).then(json => {
    if (!json) {
      return
    }

    if (setFavourite) {
      document.querySelector('#graph_' + graphSettings._id + ' .projectsGraphSetFavouriteButton').classList.add('d-none')
      document.querySelector('#graph_' + graphSettings._id + ' .projectsGraphUnsetFavouriteButton').classList.remove('d-none')
      messageShow('Projects Graph was set as favourite.', 'success')
    } else {
      document.querySelector('#graph_' + graphSettings._id + ' .projectsGraphSetFavouriteButton').classList.remove('d-none')
      document.querySelector('#graph_' + graphSettings._id + ' .projectsGraphUnsetFavouriteButton').classList.add('d-none')
      messageShow('Projects Graph was unset as favourite.', 'success')
    }
  })
}

function projectLogsGraphDelete(graphSettings) {
  if (!confirm('Are you sure you want to delete ' + graphSettings['name'] + ' graph?')) {
    return
  }

  graphSettings.active = 0
  asyncRequest('api/settings/' + graphSettings._id + '/save', graphSettings).then(json => {
    if (!json) {
      return
    }

    document.getElementById('graph_' + graphSettings._id).remove()
    document.getElementById('graph_link_' + graphSettings._id).remove()
    messageShow('Projects Graph was deleted.', 'success')
  })
}

function projectsGraphCreate(projects, graphSettings) {
  if (typeof window.graphCounter === 'undefined') {
    window.graphCounter = 0
  }
  window.graphCounter++

  const total = projects.reduce((accumulator, project) => accumulator + project.count, 0)
  let strokeDashoffset = 0

  projects.sort((a, b) => b.count - a.count)

  for (let index = 0; index < projects.length; index++) {
    projects[index].colour = getColour(index, projects[index].handle)
    projects[index].percentage = projects[index].count / total
  }

  html = '<div id="graph_' + (graphSettings ? graphSettings._id : 'new_' + window.graphCounter) + '" class="pieChartWrapper d-flex">'
  html += '  <svg viewBox="0 0 20 20">'
  projects.forEach(project => {
    const strokeDasharrayValue = 31.4 * project.percentage
    html += '    <circle r="5" cx="10" cy="10" fill="transparent" stroke="' + project.colour + '" stroke-width="10" stroke-dasharray="' + strokeDasharrayValue + ' 31.4" stroke-dashoffset="-' + strokeDashoffset + '">'
    html += '      <title>' + project.count + ' (' + Math.round(project.percentage * 100) + '%) - ' + project.name + '</title>'
    html += '    </circle>'
    strokeDashoffset += strokeDasharrayValue
  })
  html += '  </svg>'
  html += '  <div class="d-flex flex-column justify-content-between">'
  html += '    <ul class="pieChartLegend">'
  projects.forEach(project => {
    html += '      <li><span style="background-color: ' + project.colour + '">&nbsp;</span>' + project.count + ' (' + Math.round(project.percentage * 100) + '%) - ' + project.name + '</li>'
  })
  html += '    </ul>'
  html += '    <div class="pieChartActions">'
  if (!graphSettings) {
    html += '      <a class="btn btn-outline-success float-end" href="#" onclick="projectLogsGraphSaveModalShow(\'graph_new_' + window.graphCounter + '\', \'' + projects.map(project => project.handle).join(',') + '\'); return false;" title="Save Graph"><i class="bi bi-floppy buttonIcon"></i></a>'
  } else {
    html += graphActionsHtml(graphSettings)
  }
  html += '    </div>'
  html += '  </div>'
  html += '</div>'

  document.querySelector('#projectLogsGraph').insertAdjacentHTML('beforeend', html)
}

function projectLogsGraphSaveModalShow(wrapperId, projectHandles) {
  document.getElementById('graphWrapperId').value = wrapperId
  document.getElementById('graphNameHandles').value = projectHandles
  bootstrap.Modal.getOrCreateInstance('#projectLogsGraphSaveModal').show()
}

function getColour(index, string) {
  // using Bootstrap colours https://getbootstrap.com/docs/5.2/utilities/colors/#variables
  const colours = ['#dc3545', '#198754', '#0d6efd', '#0dcaf0', '#d63384', '#ffc107', '#6610f2', '#fd7e14', '#6f42c1', '#20c997', '#adb5bd']

  if (colours[index]) {
    return colours[index]
  }

  if (typeof string !== 'undefined') {
    return stringToColour(string)
  }
  return stringToColour('colour' + Math.floor(Math.random() * 1000))
}


function stringToColour(string) {
  let hash = 0
  string.split('').forEach(char => hash = char.charCodeAt(0) + ((hash << 5) - hash))
  
  let colour = '#'
  for (let i = 0; i < 3; i++) {
    const value = (hash >> (i * 8)) & 0xff
    colour += value.toString(16).padStart(2, '0')
  }
  return colour
}

function escapeHtml(text) {
  const map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  }
  
  return text.replace(/[&<>"']/g, match => map[match])
}

function graphActionsHtml (graphSettings) {
  html = '      <a class="btn btn-outline-danger float-end" href="#" onclick="projectLogsGraphDelete(' + escapeHtml(JSON.stringify(graphSettings)) + '); return false;"><i class="bi bi-trash3 buttonIcon"></i></a>'
  html += '      <a class="projectsGraphUnsetFavouriteButton ' + (graphSettings.favourite === 0 ? 'd-none' : '') + ' btn btn-outline-secondary float-end me-2" href="#" onclick="projectLogsGraphFavouriteUpdate(' + escapeHtml(JSON.stringify(graphSettings)) + ', 0); return false;" title="Unset Graph as favourite to not be shown automatically."><i class="bi bi-star-fill buttonIcon"></i></a>'
  html += '      <a class="projectsGraphSetFavouriteButton ' + (graphSettings.favourite === 1 ? 'd-none' : '') + ' btn btn-outline-secondary float-end me-2" href="#" onclick="projectLogsGraphFavouriteUpdate(' + escapeHtml(JSON.stringify(graphSettings)) + ', 1); return false;" title="Set Graph as favourite to be shown automatically."><i class="bi bi-star buttonIcon"></i></a>'
  return html
}

function errorLogsDelete(logType, handle) {
  if (!confirm('Are you sure you want to delete the ' + logType + ' error log?')) {
    return
  }
  asyncRequest('api/error-logs/' + handle + '/' + logType + '/delete').then(json => {
    if (!json) {
      return
    }

    document.querySelector('#rowLogsInsertErrors_' + logType + '_' + handle).remove()
    if (document.querySelector('#tableLogsInsertErrors tbody').childElementCount === 0) {
      document.querySelector('#errorLogsInfoButton').remove()
      const fieldsImportModal = bootstrap.Modal.getInstance('#errorLogsInfoModal')
      fieldsImportModal.hide()
    }

    messageShow('Error logs cleared.', 'success')
  })
}

function apiKeyShow(keyId) {
  document.getElementById('tokenPlaceholder_' + keyId).classList.add('d-none')
  document.getElementById('tokenValue_' + keyId).classList.remove('d-none')
  setTimeout(() => {
    document.getElementById('tokenPlaceholder_' + keyId).classList.remove('d-none')
    document.getElementById('tokenValue_' + keyId).classList.add('d-none')
  }, 5000);
}

function apiKeyCreate(userId, columnActiveShow) {
  const apiKeyName = document.getElementById('apiKeyNameInput').value.trim()
  if (!apiKeyName) {
    document.getElementById('apiKeyCreateError').classList.remove('d-none')
    return
  }
  document.getElementById('apiKeyCreateError').classList.add('d-none')

  const apiKeyPost = { name: apiKeyName, user_id: userId }
  asyncRequest('api/key/create', apiKeyPost).then(json => {
    if (!json) {
      return
    }

    document.getElementById('apiKeyNameInput').value = ''
    bootstrap.Modal.getOrCreateInstance('#apiKeyCreateModal').hide()
    document.getElementById('noApiKeysMessage').remove()
    
    let html = '<tr id="apiKeyRow_' + json._id +  '">'
    html += '  <td>' + json.name + '</td>'
    if (columnActiveShow) {
      html += '  <td>Active</td>'
    }
    html += '  <td>'
    html += '    <span id="tokenPlaceholder_' + json._id + '">***</span>'
    html += '    <span id="tokenValue_' + json._id + '" class="d-none">' + json.token + '</span>'
    html += '  </td>'
    html += '  <td class="actionsCover">'
    html += '    <div class="actions">'
    html += '      <a href="#" class="btn btn-outline-secondary" title="View token" onclick="apiKeyShow(\'' + json._id + '\'); this.remove(); return false;"><i class="bi bi-eye buttonIcon"></i></a>'
    html += '      <a href="#" class="btn btn-outline-secondary" title="Copy to clipboard" onclick="navigator.clipboard.writeText(\'' + json.token + '\'); messageShow(\'Copied to clipboard.\', \'success\'); return false;"><i class="bi bi-copy buttonIcon"></i></a>'
    html += '      <a href="#" class="btn btn-outline-danger" title="Deactivate API Key" onclick="apiKeyDeactivate(\'' + json._id + '\'); return false;"><i class="bi bi-trash3 buttonIcon"></i></a>'
    html += '    </div>'
    html += 'Never'
    html += '  </td>'
    html += '</tr>'

    document.querySelector('table#apiKeysTable tbody').innerHTML += html

    messageShow('API Key created.', 'success')
  })
}

function apiKeyDeactivate(keyId) {
  if (!confirm('Are you sure you want to deactivate this API Key')) {
    return
  }

  asyncRequest('api/key/' + keyId + '/deactivate').then(json => {
    if (!json) {
      return
    }

    document.getElementById('apiKeyRow_' + keyId).remove()
    if (document.querySelectorAll('table#apiKeysTable tbody tr').length === 0) {
      document.getElementById('apiKeysTable').remove()
    }
    messageShow('API Key deactivated.', 'success')
  })
}

function placeholderReplace(inputId, placeholder, replacement) {
  const input = document.getElementById(inputId)
  const value = input.value ? input.value : ''
  input.value = value.replace(placeholder, replacement)
}

function memcachedKeyDelete(key) {
  if (!confirm('Are you sure you want to delete ' + key + '?')) {
    return
  }

  asyncRequest('api/rate-limits/delete', { key }).then(json => {
    if (!json) {
      return
    }
    messageShow(json.message, 'success')
  })
}

function ruleRowAdd() {
  if (typeof window.ruleRowCounter === 'undefined') {
    window.ruleRowCounter = 0
  }
  window.ruleRowCounter++
  const row = document.getElementById('rowRuleTemplate').content.getElementById('rowRuleNew').cloneNode(true)
  row.setAttribute('id', 'new_' + window.ruleRowCounter)
  const nodes = row.querySelectorAll('input, select')
  nodes.forEach(node => {
    node.setAttribute('name', node.getAttribute('name').replace('__replace__', 'new_' + window.ruleRowCounter))
  })
  const links = row.querySelectorAll('a')
  links.forEach(link => {
    link.setAttribute('onclick', link.getAttribute('onclick').replace('__replace__', 'new_' + window.ruleRowCounter))
  })
  document.getElementById('tableRules').getElementsByTagName('tbody')[0].appendChild(row)
}

function tagProjectIdChanged(select) {
  const ruleFieldSelects = document.querySelectorAll('select.ruleFieldSelect')
  ruleFieldSelects.forEach(element => element.options.length = 0)
  if (window.projectsFields[select.value]) {
    html = '<option>-- select --</option>'
    for (const key in window.projectsFields[select.value]) {
      if (Object.prototype.hasOwnProperty.call(window.projectsFields[select.value], key)) {
        html += '<option value="' + key + '">' + window.projectsFields[select.value][key] + '</option>'
      }
    }
    ruleFieldSelects.forEach(element => element.innerHTML = html)
  }
}

function projectsLogsSearch(projectHandles) {
  const formData = projectLogsFilterFormParse()
  if (formData === false) {
    return
  }
  const filter = projectLogsFilterCreate(formData)
  if (filter === false) {
    return
  }

  if (Object.keys(filter).length === 0) {
    messageShow('Cannot submit empty filter.', 'error')
    return
  }

  const filterUrl = projectLogsUrlFilterCreate(filter)

  const resultTable = document.getElementById('projectLogsSearchResult')
  if (resultTable.classList.contains('d-none')) {
    resultTable.classList.remove('d-none')
  }
  const resultTableBody = resultTable.getElementsByTagName('tbody')[0]
  resultTableBody.innerHTML = ''

  asyncRequest('api/logs/search?project_handles=' + projectHandles.join(','), filter).then(json => {
    if (!json) {
      return
    }
    
    messageShow(json.message, json.errors ? 'warning' : 'success', 'searchResult', json.errors ? -1 : 5000)

    if (!json.items) {
      return
    }

    if (json.items.length > 0) {
      json.items.forEach(item => {
        let html = '<tr>'
        html += '  <td' + (item.class ? ' class="' + item.class + '"' : '') + '>' + item.project_name + '</td>'
        html += '  <td' + (item.class ? ' class="' + item.class + '"' : '') + '>' + item.last + '</td>'
        html += '  <td class="actionsCover' + (item.class ? ' ' + item.class : '') +'">'
        html += '    <div class="actions">' 
        html += '      <a class="btn btn-outline-primary" href="' + window.correctUrl + 'projects/' + item.project_id + '/logs?' + filterUrl.join('&') + '" title="Show Logs with filter" target="_blank">';
        html += '        <i class="bi bi-list-columns buttonIcon"></i>';
        html += '      </a>';
        html += '      <a class="btn btn-outline-primary" href="' + window.correctUrl + 'api/logs/load?project_handle=' + item.project_handle + '&limit=1000&' + filterUrl.join('&') + '" title="Show raw data for last 1000 filtered logs" target="_blank">';
        html += '        <i class="bi bi-filetype-raw buttonIcon"></i>';
        html += '      </a>';
        html += '      <a class="btn btn-outline-primary" href="' + window.correctUrl + 'projects/' + item.project_id + '/logs?filter_logs=1&' + filterUrl.join('&') + '" title="Show only filtered Logs" target="_blank">';
        html += '        <i class="bi bi-search buttonIcon"></i>';
        html += '      </a>';
        html += '    </div>'
        html += item.count
        html += '  </td>'
        html += '</tr>'
  
        resultTableBody.insertAdjacentHTML('beforeend', html)
      });
    } else {
      resultTableBody.insertAdjacentHTML('beforeend', '<tr><td colspan="3" class="text-center">-- not found --</td></tr>')
    }
  })
}

function projectLogsFilterFormParse() {
  const formData = {}

  for (const element of document.forms.formProjectLogsSearch.elements) {
    if (element.localName !== 'input' || !['text', 'number', 'hidden'].includes(element.type) || element.disabled) {
      continue
    }
    const match = element.name.match(/^\[(.*)\]\[(filter|value|type|negative)\](\[(min|max)\])?$/)
    if (!match || !match[1] || !match[2]) {
      messageShow('Unknown format of filter name supplied: ' + element.name, 'error')
      return false
    }
    if (!formData[match[1]]) {
      formData[match[1]] = {}
    }
    if (match[4]) {
      if (!formData[match[1]][match[2]]) {
        formData[match[1]][match[2]] = {}
      }
      formData[match[1]][match[2]][match[4]] = element.value
    } else {
      formData[match[1]][match[2]] = element.value
    }
  }

  return formData
}

function projectLogsFilterCreate(formData) {
  for (const key in formData) {
    if (Object.prototype.hasOwnProperty.call(formData, key) && formData[key].value) {
      if (formData[key].type === 'int') {
        if (formData[key].filter === 'between') {
          formData[key].value.min = parseInt(formData[key].value.min)
          formData[key].value.max = parseInt(formData[key].value.max)
        } else {
          formData[key].value = parseInt(formData[key].value)
        }
        formData[key].type = 'number'
      } else if (formData[key].type === 'double') {
        if (formData[key].filter === 'between') {
          formData[key].value.min = parseFloat(formData[key].value.min)
          formData[key].value.max = parseFloat(formData[key].value.max)
        } else {
          formData[key].value = parseFloat(formData[key].value)
        }
        formData[key].type = 'number'
      }

      if (formData[key].filter === 'between') {
        if (formData[key].value.min === null || formData[key].value.max === null) {
          delete formData[key]
          continue
        }
        if (formData[key].value.min > formData[key].value.max) {
          messageShow('Filter for ' + key + 'cannot have min above max.', 'error');
          return false
        }
      }

      formData[key].negative = parseInt(formData[key].negative)
    } else {
      delete formData[key]
    }
  }

  return formData
}

function projectLogsUrlFilterCreate(formData, queryKey = 'filter') {
  const urlFilter = []
  for (const key in formData) {
    const filterType = formData[key].filter !== 'equals' ? '__' + (formData[key].negative ? 'not_' : '') + formData[key].filter : ''
    if (formData[key].filter === 'between') {
      urlFilter.push(encodeURIComponent(queryKey + '[' + key + filterType + '][min]') + '=' + encodeURIComponent(formData[key].value.min))
      urlFilter.push(encodeURIComponent(queryKey + '[' + key + filterType + '][max]') + '=' + encodeURIComponent(formData[key].value.max))
    } else {
      urlFilter.push(encodeURIComponent(queryKey + '[' + key + filterType + ']') + '=' + encodeURIComponent(formData[key].value))
    }
  }

  return urlFilter
}

function searchFilterClickedHandle(element, field, key, value, selector) {
  document.querySelector('[name="[' + field + '][' + key + ']"]').value = value
  element.closest('.hover-items').querySelector(selector)?.classList.remove('bg-secondary')
  element.classList.add('bg-secondary')
  return false
}

function searchFilterNumberChangeHandle(element, between = false) {
  const parentItem = element.closest('.filter-item')
  const filterNumberBetween = parentItem.querySelector('.filterNumberBetween')
  const filterNumberDefault = parentItem.querySelector('.filterNumberDefault')
  if (between) {
    if (filterNumberBetween.classList.contains('d-none')) {
      for (const input of filterNumberBetween.querySelectorAll('input[disabled]')) {
        input.disabled = false
      }
      filterNumberBetween.classList.remove('d-none')

      filterNumberDefault.querySelector('input').disabled = true
      filterNumberDefault.classList.add('d-none')
    }
  } else {
    if (filterNumberDefault.classList.contains('d-none')) {
      for (const input of filterNumberBetween.querySelectorAll('input')) {
        input.disabled = true
      }
      filterNumberBetween.classList.add('d-none')

      filterNumberDefault.querySelector('input').disabled = false
      filterNumberDefault.classList.remove('d-none')
    }
  }
}

function projectDashboardAssignmentSave(projectId) {
  const dashboardIds = []
  for (const option of document.getElementById('selectDashboardIds').selectedOptions) {
    dashboardIds.push(option.value)
  }

  asyncRequest('api/projects/' + projectId + '/dashboards/save', { dashboard_ids: dashboardIds }).then(json => {
    if (!json) {
      return
    }
    messageShow(json.message, 'success')
    bootstrap.Modal.getOrCreateInstance('#projectDashboardAssignmentModal').hide()

    const dashboardsAssignedToWrapper = document.getElementById('dashboardsAssignedToWrapper')
    if (json.dashboards.length > 0) {
      dashboardsAssignedToWrapper.innerHTML = ''
      json.dashboards.forEach(dashboard => {
        dashboardsAssignedToWrapper.insertAdjacentHTML('beforeend', '<a href="' + correctUrl + 'dashboards/' + dashboard._id + '/edit" class="btn btn-sm mb-2 me-1 btn-outline-secondary btnMinWidth">' + dashboard.name + '</a>' + "\n")
      });
    } else {
      dashboardsAssignedToWrapper.innerHTML = '<span class="text-warning">Project is not assign to any Dashboard</span>'
    }
  })
}

function projectUserPermissionsSave(projectId) {
  const usersPermissions = {}
  for (const element of document.querySelectorAll('#projectUserPermissionsModal input[type="radio"]:checked')) {
    usersPermissions[element.name] = element.value
  }

  asyncRequest('api/projects/' + projectId + '/users-permissions/save', { users_permissions: usersPermissions }).then(json => {
    if (!json) {
      return
    }
    messageShow(json.message, 'success')
    bootstrap.Modal.getOrCreateInstance('#projectUserPermissionsModal').hide()

    const usersPermissionsWrapper = document.getElementById('usersPermissionsWrapper')
    usersPermissionsWrapper.innerHTML = ''
    json.users.forEach(user => {
      if (user.link) {
        usersPermissionsWrapper.insertAdjacentHTML('beforeend', '<a href="' + correctUrl + 'users/' + user._id + '/edit" class="btn btn-sm mb-2 btn-outline-secondary btnMinWidth">' + user.name + ' | ' + user.permissions + '</a>' + "\n")
      } else {
        usersPermissionsWrapper.insertAdjacentHTML('beforeend', '<span class="btn btn-sm mb-2 btn-outline-secondary disabled btnMinWidth">' + user.name + ' | ' + user.permissions + '</span>' + "\n")
      }
    });
  })
}

function projectsLogsSearchCopy() {
  const formData = projectLogsFilterFormParse();
  if (formData === false) {
    return
  }
  const filter = projectLogsFilterCreate(formData);
  if (filter === false) {
    return
  }

  if (Object.keys(filter).length === 0) {
    messageShow('No filter set.', 'error');
    return;
  }

  const filterUrl = projectLogsUrlFilterCreate(filter, 'search')
  navigator.clipboard.writeText(window.correctUrl + 'projects?' + filterUrl.join('&'))
  messageShow('Copied to clipboard.', 'success');
}
