<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(__DIR__.'/inc/functions.php');

developerSet();

if (!is_file(__DIR__.'/inc/config.php')) {
  htmlError('Missing config file: /inc/config.php');
}

require_once(__DIR__.'/inc/config.php');

attemptRefreshToLoginIfFromExternalSite();

if (empty($mongoDbUri)) {
  htmlError('Missing Mongo DB URI in config file.');
}

if (!is_file(dirname(__DIR__).'/vendor/autoload.php')) {
  htmlError('Missing vendor.');
}

require_once(dirname(__DIR__).'/vendor/autoload.php');

$database = getDatabaseOrDie();

$page = !empty($_REQUEST['page']) ? basename($_REQUEST['page']) : 'dashboard';
if (!isUser()) {
  $page = 'login';
} elseif (isSuperuser() && $database->projects->countDocuments() === 0) {
  $page = 'project_edit';
  $_REQUEST['project_id'] = 'new';
  messageAdd('To start using Log Viewer create Project for logs.', 'info', 'initial_setup');
}

if (!permissionsCheck($page, getIdForPage($page))) {
  htmlError('Access denied', httpResponseCode: 403);
}

$filename = __DIR__.'/pages/'.$page.'.php';
if (file_exists($filename)) {
  include($filename);
} else {
  htmlError('Unknown page: '.$filename);
}
