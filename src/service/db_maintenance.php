<?php

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

serviceAccessOrDie();

$database = getDatabaseOrDie();
$timestampFilter = time() - strtotime('-14 days');

echo 'beginning deletion of deactivated users'.END_LINE;
$usersDeleted = $database->users->deleteMany(['active' => 0, 'deactivated_on' => ['$lt' => $timestampFilter]])->getDeletedCount();
echo 'deleted '.$usersDeleted.' deactivated users'.END_LINE;

echo 'beginning deletion of deactivated keys'.END_LINE;
$keysDeleted = $database->keys->deleteMany(['active' => 0, 'deactivated_on' => ['$lt' => $timestampFilter]])->getDeletedCount();
echo 'deleted '.$keysDeleted.' deactivated keys'.END_LINE;

echo 'beginning deletion of deactivated dashboards'.END_LINE;
$dashboards = cursorToArray($database->dashboards->find(['active' => 0, 'deactivated_on' => ['$lt' => $timestampFilter]]));
$dashboardIds = array_column($dashboards, '_id');

$usersModified = 0;
if (!empty($dashboardIds)) {
  $usersModified = $database->users->updateMany(['permissions.dashboards' => ['$in' => $dashboardIds]], ['$pull' => ['permissions.dashboards' => ['$in' => $dashboardIds]]])->getModifiedCount();
}

$dashboardsDeleted = $database->dashboards->deleteMany(['active' => 0, 'deactivated_on' => ['$lt' => $timestampFilter]])->getDeletedCount();
echo 'deleted '.$dashboardsDeleted.' deactivated dashboards, removed no longed valid permission from '.$usersModified.' users'.END_LINE;

echo 'beginning deactivation of expired tags'.END_LINE;
$tagsDeactivated = $database->tags->updateMany(['active_until' => ['$lt' => time()]], ['$set' => ['active' => 0, 'deactivated_on' => time()]])->getModifiedCount();
echo 'deactivated '.$tagsDeactivated.' expired tags'.END_LINE;

echo 'beginning deletion of deactivated tags'.END_LINE;
$tagsDeleted = $database->tags->deleteMany(['active' => 0, 'deactivated_on' => ['$lt' => $timestampFilter]])->getDeletedCount();
echo 'deleted '.$tagsDeleted.' deactivated tags'.END_LINE;
