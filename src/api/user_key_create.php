<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_user_key_create');

userOrTokenOrDie('errors_user_key_create');

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Missing data.', 'errors_user_key_create', 'missing data');
}

$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'errors_user_key_create', !empty($data) ? 'invalid json: '.substr($data, 0, 1000) : 'missing data');
}

$keyUserId = !empty($dataJson['user_id']) && is_string($dataJson['user_id']) ? sanitizeStringInput($dataJson['user_id']) : '';
if (empty($keyUserId)) {
  apiErrorResponse('Missing User for API Key.', 'errors_user_key_create', 'missing user id for api key');
}

if (!permissionsCheck('user_key_create', $keyUserId)) {
  apiErrorResponse('You cannot create API Keys for this User.', 'errors_user_key_create', 'access denied for supplied user id', 403);
}

$keyName = !empty($dataJson['name']) && is_string($dataJson['name']) ? sanitizeStringInput($dataJson['name']) : '';
if (empty($keyName)) {
  apiErrorResponse('Missing name for API Key.', 'errors_user_key_create', 'missing name for api key');
}

$token = userKeyTokenGenerate();
if (empty($token)) {
  apiErrorResponse('Could not generate unique token for API Key.', 'errors_user_key_create', 'could not generate unique token for api key');
}

$keyPost = array(
  'active' => 1,
  'name' => $keyName,
  'user_id' => $keyUserId,
  'token' => $token,
);

$resultKeyCreate = $database->keys->insertOne($keyPost);
if ($resultKeyCreate->getInsertedCount() === 0) {
  apiErrorResponse('Could not insert API Key to DB.', 'errors_user_key_create', 'could not insert api key to DB');
}
$keyPost['_id'] = (string)$resultKeyCreate->getInsertedId();

apiResponse('Successfully created API Key.', $keyPost);
