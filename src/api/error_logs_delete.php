<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_error_logs_delete');

userOrTokenOrDie('errors_error_logs_delete');

if (!permissionsCheck('error_logs_delete')) {
  apiErrorResponse('Access denied.', 'errors_error_logs_delete', 'access denied', 403);
}

$type = !empty($_REQUEST['type']) ? sanitizeStringInput($_REQUEST['type']) : '';
if (empty($type)) {
  apiErrorResponse('Missing type of error logs to delete.', 'errors_error_logs_delete', !empty($_REQUEST['type']) ? 'invalid error logs type: '.$_REQUEST['type'] : 'missing error logs type', 400);
}

if (!in_array($type, ['file', 'collection'])) {
  apiErrorResponse('Unknown error logs type '.$type.'.', 'errors_error_logs_delete', 'unknown error logs type: '.$type, 400);
}

$errorHandles = errorLogsHandles();
$handle = !empty($_REQUEST['handle']) ? sanitizeStringInput($_REQUEST['handle']) : '';
if (empty($handle)) {
  apiErrorResponse('Missing handle of error logs to delete.', 'errors_error_logs_delete', !empty($_REQUEST['handle']) ? 'invalid error logs handle: '.$_REQUEST['handle'] : 'missing error logs handle', 400);
}

if (!in_array($handle, $errorHandles)) {
  apiErrorResponse('Unknown error logs handle '.$handle.'.', 'errors_error_logs_delete', 'unknown error logs handle: '.$handle, 400);
}

$result = false;
if ($type === 'file') {
  $logFilename = dirname(__DIR__).'/logs/'.$handle.'.log';
  if (!is_file($logFilename)) {
    apiErrorResponse('File for error handle '.$handle.' does not exist.', 'errors_error_logs_delete', 'file for error handle '.$handle.' does not exist', 400);
  }
  $result = unlink($logFilename);
} else {
  $collectionList = $database->listCollections(['filter' => ['name' => $handle]]);
  $collectionList->rewind();
  if (!$collectionList->valid()) {
    apiErrorResponse('Collection for error handle '.$handle.' does not exist.', 'errors_error_logs_delete', 'collection for error handle '.$handle.' does not exist', 400);
  }
  $database->{$handle}->deleteMany([]);
  $result = true;
}

if ($result) {
  apiResponse('Successfully deleted error logs.');
}
apiErrorResponse('There was problem with deleting the error logs.', 'errors_error_logs_delete', 'could not delete error logs', 400);
