<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_logs_clear');

userOrTokenOrDie('errors_logs_clear');

$projectHandles = !empty($_REQUEST['project_handles']) ? array_unique(array_filter(array_map('sanitizeStringInput', explode(',', $_REQUEST['project_handles'])))) : '';
if (empty($projectHandles)) {
  apiErrorResponse('Missing Project handles.', 'errors_logs_clear', !empty($_REQUEST['project_handles']) ? 'invalid project handles: '.$_REQUEST['project_handles'] : 'missing project handles', 400);
}

$projects = in_array('__all', $projectHandles) ? $database->projects->find()->toArray() : $database->projects->find(['handle' => ['$in' => $projectHandles]])->toArray();
if (empty($projects)) {
  apiErrorResponse('Could not find Project with handle: '.implode(', ', $projectHandles).'.', 'errors_logs_clear', 'could not find Project with handle: '.implode(', ', $projectHandles), 400);
}

foreach ($projects as $project) {
  if (!permissionsCheck('logs_clear', (string)$project['_id'])) {
    apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_logs_clear', 'project '.$project['_id'].' access denied', 403);
  }
}

if (empty($_REQUEST['filter'])) {
  apiErrorResponse('Missing filter for clearing logs.', 'errors_logs_clear', 'missing filter for clearing logs', 400);
}

$projectsFields = [];

$logsCollections = array_map(fn ($project) => 'logs_'.$project['_id'], $projects);
$collectionsInfo = $database->listCollections(['filter' => ['name' => ['$in' => $logsCollections]]]);
$collectionsInfo->rewind();
foreach ($collectionsInfo as $collectionInfo) {
  $collectionOptions = $collectionInfo->getOptions();
  if (!empty($collectionOptions) && !empty($collectionOptions['validator'])) {
    $projectId = strtr($collectionInfo->getName(), ['logs_' => '']);
    if (!isset($projectsFields[$projectId])) {
      $projectsFields[$projectId] = [];
    }
    foreach ($collectionOptions['validator']['$jsonSchema']['properties'] as $key => $value) {
      $projectsFields[$projectId][] = ['name' => $key, 'type' => $value['bsonType'], 'required' => !empty($collectionOptions['validator']['$jsonSchema']['required']) && in_array($key, $collectionOptions['validator']['$jsonSchema']['required'])];
    }
  }
}

$modifiedCount = 0;
$errors = [];
foreach ($projects as $project) {
  $filter = mongoDBLogsFilter(requestFilterSettingsParse(!empty($projectsFields[(string)$project['_id']]) ? $projectsFields[(string)$project['_id']] : [], 'contains'));
  $logsCollection = $database->{'logs_'.$project['_id']};

  try {
    $modifiedCount += (int)$logsCollection->updateMany($filter, ['$set' => ['to_be_deleted' => true]], ['bypassDocumentValidation' => true])->getModifiedCount();
  } catch (Exception $exception) {
    $errors[] = $project['name'].' - '.$exception->getMessage();
    logErrorMessage('errors_logs_clear', $project['name'].' - '.$project['_id'].': '.$exception->getMessage());
  }
}

$message = 'Successfully set '.$modifiedCount.' logs as to be deleted.';
if (count($errors) > 0) {
  $message .= '<br /><strong>Errors</strong><br />'.implode('<br />', $errors);
}

apiResponse($message, ['count' => $modifiedCount, 'errors' => $errors], count($errors) > 0 ? 'error' : 'success');
