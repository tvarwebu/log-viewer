<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'project_dashboard_save');

userOrTokenOrDie('project_dashboard_save');

$projectId = !empty($_REQUEST['project_id']) ? sanitizeStringInput($_REQUEST['project_id']) : '';
if (empty($projectId)) {
  apiErrorResponse('Missing Project ID.', 'project_dashboard_save', !empty($_REQUEST['project_id']) ? 'invalid project id: '.$_REQUEST['project_id'] : 'missing project id');
}

$project = $database->projects->findOne(['_id' => new MongoDB\BSON\ObjectID($projectId)]);
if (empty($project)) {
  apiErrorResponse('Invalid Project ID.', 'project_dashboard_save', 'invalid project id: '.$_REQUEST['project_id']);
}
if (!permissionsCheck('project_save', (string)$project['_id'])) {
  apiErrorResponse('Project '.$project['_id'].' access denied.', 'project_dashboard_save', 'project '.$project['_id'].' access denied', 403);
}

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Missing data.', 'project_dashboard_save', 'missing data');
}
$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'project_dashboard_save', !empty($data) ? 'invalid json: '.substr($data, 0, 1000) : 'missing data');
}

if (!isset($dataJson['dashboard_ids'])) {
  apiErrorResponse('Missing Dashboard IDs.', 'project_dashboard_save', 'missing dashboard ids, keys: '.implode(', ', array_keys($dataJson)));
}
if (!is_array($dataJson['dashboard_ids'])) {
  apiErrorResponse('Invalid Dashboard IDs.', 'project_dashboard_save', 'invalid dashboard ids: '.print_r($dataJson['dashboard_ids'], true));
}

/** @var array<int, array{_id: string, name: string, active: int, project_ids: string[]}> $dashboards */
$dashboards = cursorToArray($database->dashboards->find(['project_ids' => $projectId]));
$dashboardIdsCurrent = array_map(fn ($dashboardCurrent) => (string)$dashboardCurrent['_id'], $dashboards);
$dashboardIds = !empty($dataJson['dashboard_ids']) ? $dataJson['dashboard_ids'] : [];

if (!empty($dashboardIds)) {
  /** @var array<int, array{_id: string, name: string, active: int, project_ids: string[]}> $dashboardsPost */
  $dashboardsPost = cursorToArray($database->dashboards->find(['_id' => ['$in' => array_map(fn ($dashboardId) => new MongoDB\BSON\ObjectID($dashboardId), $dashboardIds)]]));
  if (empty($dashboardsPost) || count($dashboardIds) !== count($dashboardsPost)) {
    apiErrorResponse('Invalid Dashboard IDs.', 'project_dashboard_save', 'invalid dashboard ids: '.implode(', ', $dataJson['dashboard_ids']));
  }
  foreach ($dashboardsPost as $dashboard) {
    if (!permissionsCheck('dashboard_save', (string)$dashboard['_id'])) {
      apiErrorResponse('Dashboard '.$dashboard['_id'].' access denied.', 'project_dashboard_save', 'dashboard '.$dashboard['_id'].' access denied', 403);
    }
    if (!in_array((string)$dashboard['_id'], $dashboardIdsCurrent)) {
      $dashboards[] = $dashboard;
    }
  }
}

$dashboardsUpdated = 0;
$dashboardsNew = [];
foreach ($dashboards as $dashboard) {
  if (in_array((string)$dashboard['_id'], $dashboardIds)) {
    $dashboard['project_ids'][] = $projectId;
    $dashboardsNew[] = $dashboard;
  } else {
    $dashboard['project_ids'] = array_filter($dashboard['project_ids'], fn ($projectIdCurrent) => $projectIdCurrent !== $projectId);
  }
  $resultDashboardUpdate = $database->dashboards->updateOne(['_id' => new MongoDB\BSON\ObjectID($dashboard['_id'])], ['$set' => ['project_ids' => array_values(array_unique($dashboard['project_ids']))]]);
  $dashboardsUpdated += $resultDashboardUpdate->getMatchedCount();
}

apiResponse('Successfully updated assignment for '.$dashboardsUpdated.' Dashboard.', ['dashboards' => $dashboardsNew]);
