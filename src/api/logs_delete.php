<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_logs_delete');

userOrTokenOrDie('errors_logs_delete');

$projectHandles = !empty($_REQUEST['project_handles']) ? array_unique(array_filter(array_map('sanitizeStringInput', explode(',', $_REQUEST['project_handles'])))) : '';
if (empty($projectHandles)) {
  apiErrorResponse('Missing Project handles.', 'errors_logs_delete', !empty($_REQUEST['project_handles']) ? 'invalid project handles: '.$_REQUEST['project_handles'] : 'missing project handles', 400);
}

$projects = in_array('__all', $projectHandles) ? $database->projects->find()->toArray() : $database->projects->find(['handle' => ['$in' => $projectHandles]])->toArray();
if (empty($projects)) {
  apiErrorResponse('Could not find Project with handle: '.implode(', ', $projectHandles).'.', 'errors_logs_delete', 'could not find Project with handle: '.implode(', ', $projectHandles), 400);
}

foreach ($projects as $project) {
  if (!permissionsCheck('logs_delete', (string)$project['_id'])) {
    apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_logs_delete', 'project '.$project['_id'].' access denied', 403);
  }
}

$countDeleted = 0;
foreach ($projects as $project) {
  $logsCollection = $database->{'logs_'.$project['_id']};
  $countDeleted += $logsCollection->deleteMany(['to_be_deleted' => true])->getDeletedCount();
}

apiResponse('Successfully deleted '.$countDeleted.' logs.', ['count' => $countDeleted]);
