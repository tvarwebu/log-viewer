<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

userOrTokenOrDie('errors_rate_limit_delete');

if (!permissionsCheck('rate_limit_delete')) {
  apiErrorResponse('Access denied.', 'errors_rate_limit_delete', 'access denied', 403);
}

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Missing data.', 'errors_rate_limit_delete', 'missing data');
}

$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'errors_rate_limit_delete', !empty($data) ? 'invalid json: '.substr($data, 0, 1000) : 'missing data');
}

$key = !empty($dataJson['key']) ? sanitizeStringInput($dataJson['key']) : '';
if (empty($key)) {
  apiErrorResponse('Missing Memcached key to delete.', 'errors_rate_limit_delete', !empty($dataJson['key']) ? 'invalid memcached key: '.$dataJson['key'] : 'missing memcached key', 400);
}

$result = memcachedDelete($key);
if ($result) {
  apiResponse('Successfully deleted rate limit.');
}
apiErrorResponse('There was problem with deleting the Rate Limit.', 'errors_rate_limit_delete', 'could not delete rate limit '.$key, 400);
