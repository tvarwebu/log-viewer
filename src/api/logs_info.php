<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_logs_info');

userOrTokenOrDie('errors_logs_info');

$projectHandles = !empty($_REQUEST['project_handles']) ? array_unique(array_filter(array_map('sanitizeStringInput', explode(',', $_REQUEST['project_handles'])))) : '';
if (empty($projectHandles)) {
  apiErrorResponse('Missing Project handles.', 'errors_logs_info', !empty($_REQUEST['project_handles']) ? 'invalid project handles: '.$_REQUEST['project_handles'] : 'missing project handles', 400);
}

$projects = in_array('__all', $projectHandles) ? $database->projects->find()->toArray() : $database->projects->find(['handle' => ['$in' => $projectHandles]])->toArray();
if (empty($projects)) {
  apiErrorResponse('Could not find Project with handle: '.implode(', ', $projectHandles).'.', 'errors_logs_info', 'could not find Project with handle: '.implode(', ', $projectHandles), 400);
}

foreach ($projects as $project) {
  if (!permissionsCheck('logs_info', (string)$project['_id'])) {
    apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_logs_info', 'project '.$project['_id'].' access denied', 403);
  }
}

$logsInfo = [];

foreach ($projects as $project) {
  $logs = $database->{'logs_'.$project['_id']};
  $logInfo = array(
    'id' => (string)$project['_id'],
    'name' => $project['name'],
    'handle' => $project['handle'],
    'count' => $logs->count(),
  );

  if (!empty($_REQUEST['count_per_day'])) {
    $logInfo['count_per_day'] = [];
    foreach ($logs->find() as $log) {
      $logDate = gmdate('Y-m-d', (int)($log['timestamp'] / 1000));
      if (!isset($logInfo['count_per_day'][$logDate])) {
        $logInfo['count_per_day'][$logDate] = 0;
      }
      $logInfo['count_per_day'][$logDate]++;
    }

    ksort($logInfo['count_per_day']);
    $logInfo['count_per_day_min'] = !empty($logInfo['count_per_day']) ? min($logInfo['count_per_day']) : 0;
    $logInfo['count_per_day_max'] = !empty($logInfo['count_per_day']) ? max($logInfo['count_per_day']) : 0;
  }

  $logLast = $logs->findOne([], ['sort' => ['_id' => -1], 'limit' => 1]);
  $logInfo['last'] = !empty($logLast) ? gmdate('Y-m-d H:i:s', $logLast['_id']->getTimestamp()) : null;

  $logsInfo[] = $logInfo;
}

apiResponse('Successfully retrieved Logs info.', ['items' => $logsInfo]);
