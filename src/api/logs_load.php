<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_logs_load');

userOrTokenOrDie('errors_logs_load');

$data = file_get_contents('php://input');
$dataJson = !empty($data) ? json_decode($data, true) : [];
if (!is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'errors_logs_load', 'invalid json: '.($data === false ? 'could not get contents of php://input' : substr($data, 0, 1000)));
}

$projectHandle = !empty($_REQUEST['project_handle']) ? sanitizeStringInput((string)$_REQUEST['project_handle']) : '';
if (empty($projectHandle)) {
  apiErrorResponse('Missing Project handle.', 'errors_logs_load', !empty($_REQUEST['project_handle']) ? 'invalid project handle: '.$_REQUEST['project_handle'] : 'missing project handle', 400);
}

$project = $database->projects->findOne(['handle' => $projectHandle]);
if (empty($project)) {
  apiErrorResponse('Could not find Project with handle: '.$projectHandle.'.', 'errors_logs_load', 'could not find Project with handle: '.$projectHandle, 400);
}

if (!permissionsCheck('project_logs', (string)$project['_id'])) {
  apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_logs_load', 'project '.$project['_id'].' access denied', 403);
}

$filterBase = !empty($dataJson) ? $dataJson : requestFilterSettingsParse(logsFields((string)$project['_id']), 'contains');
$logsFilter = mongoDBLogsFilter($filterBase);
if (empty($_REQUEST['to_be_deleted_show'])) {
  $logsFilter['to_be_deleted'] = ['$ne' => true];
}
$logsFilter['__ignored'] = ['$ne' => true];

$options = [];
if (!empty($_REQUEST['limit'])) {
  $options['limit'] = (int)$_REQUEST['limit'];
}
if (!empty($_REQUEST['skip'])) {
  $options['skip'] = (int)$_REQUEST['skip'];
}

$logs = cursorToArray($database->{'logs_'.$project['_id']}->find($logsFilter, $options));

apiResponse('Successfully loaded Logs.', ['count' => count($logs), 'items' => $logs]);
