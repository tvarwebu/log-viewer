<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_logs_search');

userOrTokenOrDie('errors_logs_search');

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Missing data.', 'errors_logs_search', 'missing data');
}

$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'errors_logs_search', !empty($data) ? 'invalid json: '.substr($data, 0, 1000) : 'missing data');
}

$projectHandles = !empty($_REQUEST['project_handles']) ? array_unique(array_filter(array_map('sanitizeStringInput', explode(',', $_REQUEST['project_handles'])))) : '';
if (empty($projectHandles)) {
  apiErrorResponse('Missing Project handles.', 'errors_logs_search', !empty($_REQUEST['project_handles']) ? 'invalid project handles: '.$_REQUEST['project_handles'] : 'missing project handles', 400);
}

$projects = in_array('__all', $projectHandles) ? $database->projects->find()->toArray() : $database->projects->find(['handle' => ['$in' => $projectHandles]])->toArray();
if (empty($projects)) {
  apiErrorResponse('Could not find Project with handle: '.implode(', ', $projectHandles).'.', 'errors_logs_search', 'could not find Project with handle: '.implode(', ', $projectHandles), 400);
}

$logsCollections = [];
foreach ($projects as $project) {
  if (!permissionsCheck('logs_search', (string)$project['_id'])) {
    apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_logs_search', 'project '.$project['_id'].' access denied', 403);
  }
  $logsCollections[] = 'logs_'.$project['_id'];
}

$filter = mongoDBLogsFilter($dataJson);
$filter['to_be_deleted'] = ['$ne' => true];
$filter['__ignored'] = ['$ne' => true];

$logsInfo = [];
$errors = [];

foreach ($projects as $project) {
  try {
    $logCollection = $database->{'logs_'.$project['_id']};

    $logsCount = $logCollection->countDocuments($filter);
    if ($logsCount > 0) {
      $logLast = $logCollection->findOne(['__ignored' => ['$ne' => true]], ['sort' => ['_id' => -1], 'limit' => 1]);
      $logLastTimestamp = $logLast['_id']->getTimestamp();
      $class = '';
      if (time() - $logLastTimestamp < 60 * 60) {
        $class = 'text-danger';
      } elseif (time() - $logLastTimestamp < 60 * 60 * 24) {
        $class = 'text-warning';
      }

      $logsInfo[] = array(
        'project_id' => (string)$project['_id'],
        'project_name' => $project['name'],
        'project_handle' => $project['handle'],
        'count' => $logsCount,
        'last' => gmdate('Y-m-d H:i:s', $logLastTimestamp),
        'class' => $class,
      );
    }
  } catch (Exception $exception) {
    $errors[] = $project['name'].' - '.$exception->getMessage();
    logErrorMessage('errors_logs_search', $project['name'].' - '.$project['_id'].': '.$exception->getMessage());
  }
}

$responseData = ['items' => $logsInfo];
$message = 'Successfully finished Logs search.';
if (count($errors) > 0) {
  $responseData['errors'] = $errors;
  $message .= '<br /><strong>Errors</strong><br />'.implode('<br />', $errors);
}

apiResponse($message, $responseData);
