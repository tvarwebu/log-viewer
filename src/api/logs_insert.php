<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_logs_insert');

userOrTokenOrDie('errors_logs_insert');

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Could not read data from php input.', 'errors_logs_insert', 'could not read data from php input');
}

$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'errors_logs_insert', 'supplied json is '.(!empty($data) ? 'invalid: '.substr($data, 0, 1000) : 'empty'));
}

if (empty($dataJson['project_handle'])) {
  apiErrorResponse('Missing Project handle.', 'errors_logs_insert', 'missing project handle');
}

$project = $database->projects->findOne(['handle' => $dataJson['project_handle']]);
if (empty($project)) {
  logInsertData($data, $dataJson);
  apiErrorResponse('Invalid Project handle.', 'errors_logs_insert', 'invalid project handle: '.$dataJson['project_handle']);
}

if (!permissionsCheck('logs_insert', (string)$project['_id'])) {
  apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_logs_insert', 'project '.$project['_id'].' access denied', 403);
}

$tag = logInsertTagFind((string)$project['_id'], $dataJson);
if (!empty($tag) && $tag['action'] === 'ignore') {
  $dataJson['__tag_id'] = $tag['_id'];
  $dataJson['__ignored'] = true;
}

$collection = $database->{'logs_'.$project['_id']};

try {
  $result = $collection->insertOne($dataJson);
} catch (\MongoDB\Driver\Exception\BulkWriteException $exception) {
  logInsertData($data, $dataJson);
  $errors = array_map(function ($error) { return $error->getInfo(); }, $exception->getWriteResult()->getWriteErrors());
  apiResponse($exception->getMessage(), $errors, 'error', 'errors_logs_insert', $exception->getMessage().': '.implode(', ', array_map(function ($error) { return json_encode($error); }, $errors)), 400);
} catch (\Throwable $exception) {
  logInsertData($data, $dataJson);
  apiErrorResponse($exception->getMessage(), 'errors_logs_insert', $exception->getMessage());
}

logInsertData($data, $dataJson, $result);

if ($result->getInsertedCount() === 0) {
  apiErrorResponse('Could not insert log into MongoDB collection.', 'errors_logs_insert', 'could not insert log into logs_'.$project['_id'], 500);
}

apiResponse('Successfully inserted log into MongoDB collection');
