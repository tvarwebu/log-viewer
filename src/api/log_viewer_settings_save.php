<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_log_viewer_settings_save');

userOrTokenOrDie('errors_log_viewer_settings_save');

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Missing data.', 'errors_log_viewer_settings_save', 'missing data');
}

$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'errors_log_viewer_settings_save', !empty($data) ? 'invalid json: '.substr($data, 0, 1000) : 'missing data');
}

if (empty($dataJson['handle']) || !in_array($dataJson['handle'], ['projectLogsGraph', 'logsRowGraph'])) {
  apiErrorResponse('Data handle is invalid.', 'errors_log_viewer_settings_save', !empty($dataJson['handle']) ? 'invalid data handle: '.$dataJson['handle'] : 'missing data handle');
}

if (!isset($dataJson['settings'])) {
  apiErrorResponse('Missing data to save.', 'errors_log_viewer_settings_save', 'missing settings key, keys: '.implode(', ', array_keys($dataJson)));
}

$active = 1;
if (isset($dataJson['active'])) {
  $active = $dataJson['active'];
}

$dataToSave = array(
  'handle' => $dataJson['handle'],
  'active' => $active,
  'favourite' => isset($dataJson['favourite']) ? (int)$dataJson['favourite'] : 0,
  'settings' => sanitizeArray((array)$dataJson['settings']),
);

if ($dataToSave['handle'] === 'projectLogsGraph') {
  if (empty($dataToSave['settings']['project_handles']) || !is_array($dataToSave['settings']['project_handles'])) {
    apiErrorResponse('Missing project handles for Graph.', 'errors_log_viewer_settings_save', 'missing project handles for graph');
  }
  if (count($dataToSave['settings']['project_handles']) < 2) {
    apiErrorResponse('At least two Projects must be selected for graph.', 'errors_log_viewer_settings_save', 'not enough project handles supplied for graph: '.implode(', ', $dataToSave['settings']['project_handles']));
  }

  $projects = $database->projects->find(['handle' => ['$in' => $dataToSave['settings']['project_handles']]])->toArray();
  if (empty($projects) || count($projects) !== count($dataToSave['settings']['project_handles'])) {
    apiErrorResponse('Invalid project handles supplied for Graph.', 'errors_log_viewer_settings_save', 'invalid project handles supplied for graph: '.implode(', ', $dataToSave['settings']['project_handles']));
  }

  foreach ($projects as $project) {
    if (!permissionsCheck('log_viewer_settings_save', (string)$project['_id'])) {
      apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_log_viewer_settings_save', 'project '.$project['_id'].' access denied', 403);
    }
  }
} else {
  if (empty($dataToSave['settings']['project_handle']) || !is_string($dataToSave['settings']['project_handle'])) {
    apiErrorResponse('Missing project handle for Graph.', 'errors_log_viewer_settings_save', 'missing project handle for graph');
  }

  $project = $database->projects->findOne(['handle' => $dataToSave['settings']['project_handle']]);
  if (empty($project)) {
    apiErrorResponse('Invalid Project handle.', 'errors_log_viewer_settings_save', 'invalid project handle: '.$dataToSave['settings']['project_handle']);
  }

  if (!permissionsCheck('log_viewer_settings_save', (string)$project['_id'])) {
    apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_log_viewer_settings_save', 'project '.$project['_id'].' access denied', 403);
  }
}

$logViewerSettingsId = !empty($_REQUEST['log_viewer_settings_id']) && $_REQUEST['log_viewer_settings_id'] !== 'new' ? sanitizeStringInput($_REQUEST['log_viewer_settings_id']) : null;
if (!is_null($logViewerSettingsId)) {
  $mongoDbId = new MongoDB\BSON\ObjectID($logViewerSettingsId);
  $settings = $database->log_viewer_settings->findOne(['_id' => $mongoDbId]);
  if (empty($settings)) {
    apiErrorResponse('Could not find Log Viewer Setting with ID: '.$logViewerSettingsId.'.', 'errors_log_viewer_settings_save', 'could not find Log Viewer Setting with ID: '.$logViewerSettingsId);
  }

  $updateResult = $database->log_viewer_settings->updateOne(['_id' => $mongoDbId], ['$set' => $dataToSave]);
  if ($updateResult->getMatchedCount() === 0) {
    apiErrorResponse('Could not update Log Viewer Setting with ID: '.$logViewerSettingsId.'.', 'errors_log_viewer_settings_save', 'could not update Log Viewer Setting with ID: '.$logViewerSettingsId);
  }
} else {
  $insertResult = $database->log_viewer_settings->insertOne($dataToSave);
  if ($insertResult->getInsertedCount() === 0) {
    apiErrorResponse('Could not insert Log Viewer Setting to DB.', 'errors_log_viewer_settings_save', 'could not insert Log Viewer Setting to DB');
  }
  $logViewerSettingsId = (string)$insertResult->getInsertedId();
}

apiResponse('Successfully updated Log Viewer settings', ['id' => $logViewerSettingsId]);
