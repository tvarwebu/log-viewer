<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_parser_settings_save');

userOrTokenOrDie('errors_parser_settings_save');

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Missing data.', 'errors_parser_settings_save', 'missing data');
}

$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'errors_parser_settings_save', !empty($data) ? 'invalid json: '.substr($data, 0, 1000) : 'missing data');
}

if (!isset($dataJson['project_handle'])) {
  apiErrorResponse('Missing Project handle.', 'errors_parser_settings_save', 'missing project handle, keys: '.implode(', ', array_keys($dataJson)));
}
$project = $database->projects->findOne(['handle' => $dataJson['project_handle']]);
if (empty($project)) {
  apiErrorResponse('Invalid Project handle.', 'errors_parser_settings_save', 'invalid project handle: '.$dataJson['project_handle']);
}

if (!permissionsCheck('parser_settings_save', (string)$project['_id'])) {
  apiErrorResponse('Project '.$project['_id'].' access denied.', 'errors_parser_settings_save', 'project '.$project['_id'].' access denied', 403);
}

if (empty($dataJson['handle']) || !in_array($dataJson['handle'], ['parserKeys', 'parserKeysTypes', 'options', 'filters'])) {
  apiErrorResponse('Data handle is invalid.', 'errors_parser_settings_save', !empty($dataJson['handle']) ? 'invalid data handle: '.$dataJson['handle'] : 'missing data handle');
}

if (!isset($dataJson['settings'])) {
  apiErrorResponse('Missing data to save.', 'errors_parser_settings_save', 'missing settings key, keys: '.implode(', ', array_keys($dataJson)));
}

$key = sanitizeStringInput($dataJson['key']);
if (strlen($key) === 0) {
  apiErrorResponse('Invalid value supplied for data Key.', 'errors_parser_settings_save', 'invalid value supplied for data key, value: '.$dataJson['key']);
}

$active = 1;
if (isset($dataJson['active'])) {
  $active = $dataJson['active'];
} elseif (isset($dataJson['settings']['active'])) {
  $active = $dataJson['settings']['active'];
}
if ($dataJson['handle'] !== 'filters') {
  unset($dataJson['settings']['_id'], $dataJson['settings']['active']);
}

$dataToSave = array(
  'project_handle' => $dataJson['project_handle'],
  'handle' => $dataJson['handle'],
  'key' => $key,
  'active' => $active,
  'favourite' => isset($dataJson['favourite']) ? (int)$dataJson['favourite'] : 0,
  'settings' => sanitizeArray((array)$dataJson['settings']),
);

$parserSettingsId = !empty($_REQUEST['parser_settings_id']) && $_REQUEST['parser_settings_id'] !== 'new' ? sanitizeStringInput($_REQUEST['parser_settings_id']) : null;
if (!is_null($parserSettingsId)) {
  $mongoDbId = new MongoDB\BSON\ObjectID($parserSettingsId);
  $settings = $database->parser_settings->findOne(['_id' => $mongoDbId]);
  if (empty($settings)) {
    apiErrorResponse('Could not find Parser Setting with ID: '.$parserSettingsId.'.', 'errors_parser_settings_save', 'could not find parser setting with id: '.$parserSettingsId);
  }

  $updateResult = $database->parser_settings->updateOne(['_id' => $mongoDbId], ['$set' => $dataToSave]);
  if ($updateResult->getMatchedCount() === 0) {
    apiErrorResponse('Could not update Parser Setting with ID: '.$parserSettingsId.'.', 'errors_parser_settings_save', 'could not update parser setting with id: '.$parserSettingsId);
  }
} else {
  $insertResult = $database->parser_settings->insertOne($dataToSave);
  if ($insertResult->getInsertedCount() === 0) {
    apiErrorResponse('Could not insert Parser Setting to DB.', 'errors_parser_settings_save', 'could not insert parser setting to db');
  }
  $parserSettingsId = (string)$insertResult->getInsertedId();
}

apiResponse('Successfully updated Parser settings', ['id' => $parserSettingsId]);
