<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_tag_save');

userOrTokenOrDie('errors_tag_save');

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Missing data.', 'errors_tag_save', 'missing data');
}

$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'errors_tag_save', !empty($data) ? 'invalid json: '.substr($data, 0, 1000) : 'missing data');
}

try {
  $tagPost = tagPostValidateAndSanitize($dataJson);
} catch (\LogViewer\Exception\ExceptionWithData $ex) {
  apiErrorResponse($ex->getMessage(), 'errors_tag_save', $ex->getLogMessage(), !empty($ex->getCode()) ? $ex->getCode() : 400);
} catch (\Throwable $th) {
  apiErrorResponse('Unexpected exception throw, contact support.', 'errors_tag_save', $th->getMessage(), 500);
}

$tagId = !empty($_REQUEST['tag_id']) && $_REQUEST['tag_id'] !== 'new' ? sanitizeStringInput($_REQUEST['tag_id']) : null;
if (!is_null($tagId)) {
  $mongoDbId = new MongoDB\BSON\ObjectID($tagId);
  $tag = $database->tags->findOne(['_id' => $mongoDbId]);
  if (empty($settings)) {
    apiErrorResponse('Could not find Tag with ID: '.$tagId.'.', 'errors_tag_save', 'could not find tag with id: '.$tagId);
  }

  $updateResult = $database->tags->updateOne(['_id' => $mongoDbId], ['$set' => $tagPost]);
  if ($updateResult->getMatchedCount() === 0) {
    apiErrorResponse('Could not update Tag with ID: '.$tagId.'.', 'errors_tag_save', 'could not update tag with id: '.$tagId);
  }
} else {
  $insertResult = $database->tags->insertOne($tagPost);
  if ($insertResult->getInsertedCount() === 0) {
    apiErrorResponse('Could not insert Tag to DB.', 'errors_tag_save', 'could not insert tag to db');
  }
  $tagId = (string)$insertResult->getInsertedId();
}

$tagPost['_id'] = $tagId;

$message = 'Successfully '.($_REQUEST['tag_id'] !== 'new' ? 'updated' : 'created').' Tag.';

if (!empty($_REQUEST['save_and_process'])) {
  $message .= ' Marked '.tagProcess($tagPost).' logs.';
}

apiResponse($message, ['id' => $tagId]);
