<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'errors_user_key_deactivate');

userOrTokenOrDie('errors_user_key_deactivate');

$keyId = !empty($_REQUEST['key_id']) ? sanitizeStringInput($_REQUEST['key_id']) : '';
if (empty($keyId)) {
  apiErrorResponse('Missing API Key ID.', 'errors_user_key_deactivate', 'missing api key id');
}

if (!permissionsCheck('user_key_deactivate', $keyId)) {
  apiErrorResponse('Key '.$keyId.' access denied.', 'errors_user_key_deactivate', 'key '.$keyId.' access denied', 403);
}

$key = $database->keys->findOne(['_id' => new MongoDB\BSON\ObjectID($keyId)]);
if (empty($key)) {
  apiErrorResponse('Supplied unknown ID for API Key.', 'errors_user_key_deactivate', 'Supplied unknown id for api key');
} elseif ($key['active'] === 0) {
  apiResponse('API Key is already deactivated.');
}

$resultKeyUpdate = $database->keys->updateOne(['_id' => new MongoDB\BSON\ObjectID($keyId)], ['$set' => ['active' => 0, 'deactivated_on' => time()]]);
if ($resultKeyUpdate->getModifiedCount() === 0) {
  apiErrorResponse('Could not deactivated API Key.', 'errors_user_key_deactivate', 'could not deactivated api key');
}

apiResponse('Successfully deactivated API Key.');
