<?php

if (!isset($_SESSION)) {
  session_start();
}

require_once(dirname(__DIR__).'/inc/config.php');
require_once(dirname(__DIR__).'/inc/functions.php');

$database = getDatabaseOrDie(true, 'project_users_permissions_save');

userOrTokenOrDie('project_users_permissions_save');

$projectId = !empty($_REQUEST['project_id']) ? sanitizeStringInput($_REQUEST['project_id']) : '';
if (empty($projectId)) {
  apiErrorResponse('Missing Project ID.', 'project_users_permissions_save', !empty($_REQUEST['project_id']) ? 'invalid project id: '.$_REQUEST['project_id'] : 'missing project id');
}

$project = $database->projects->findOne(['_id' => new MongoDB\BSON\ObjectID($projectId)]);
if (empty($project)) {
  apiErrorResponse('Invalid Project ID.', 'project_users_permissions_save', 'invalid project id: '.$_REQUEST['project_id']);
}
if (!permissionsCheck('project_save', (string)$project['_id'])) {
  apiErrorResponse('Project '.$project['_id'].' access denied.', 'project_users_permissions_save', 'project '.$project['_id'].' access denied', 403);
}

$data = file_get_contents('php://input');
if ($data === false) {
  apiErrorResponse('Missing data.', 'project_users_permissions_save', 'missing data');
}
$dataJson = json_decode($data, true);
if (empty($dataJson) || !is_array($dataJson)) {
  apiErrorResponse('Supplied JSON is invalid.', 'project_users_permissions_save', !empty($data) ? 'invalid json: '.substr($data, 0, 1000) : 'missing data');
}

if (!isset($dataJson['users_permissions'])) {
  apiErrorResponse('Missing users permissions.', 'project_users_permissions_save', 'missing users permissions, keys: '.implode(', ', array_keys($dataJson)));
}
if (!is_array($dataJson['users_permissions'])) {
  apiErrorResponse('Invalid users permissions.', 'project_users_permissions_save', 'invalid users permissions: '.print_r($dataJson['users_permissions'], true));
}

$permissionsUnknown = array_diff(array_unique(array_values($dataJson['users_permissions'])), ['admin', 'maintainer', 'standard', 'none']);
if (!empty($permissionsUnknown)) {
  apiErrorResponse('Unknown users permissions supplied: '.implode(', ', $permissionsUnknown).'.', 'project_users_permissions_save', 'unknown users permissions supplied: '.implode(', ', $permissionsUnknown));
}

$filter = array(
  '$or' => array(
    ['type' => 'superuser'],
    ['_id' => ['$in' => array_map(fn ($userId) => new MongoDB\BSON\ObjectID($userId), array_keys($dataJson['users_permissions']))]]
  )
);

/** @var array<int, array{_id: string, forename: string, surname: string, active: int, type: string, permissions: array{dashboards: string[], projects: array<string, string>, additional: string[]}}> $users */
$users = cursorToArray($database->users->find($filter, ['sort' => ['forename' => 1, 'surname' => 1]]));

$usersUpdated = 0;
$userButtonsData = [];
foreach ($users as $user) {
  if ($user['type'] === 'superuser') {
    $userButtonsData[] = array(
      '_id' => (string)$user['_id'],
      'name' => $user['forename'].' '.$user['surname'],
      'permissions' => 'Superuser',
      'link' => (string)$user['_id'] === $_SESSION['LVuser']['_id'],
    );
    continue;
  }

  if (!permissionsCheck('user_save', $user['_id'])) {
    apiErrorResponse('User '.$user['_id'].' access denied.', 'project_users_permissions_save', 'user '.$user['_id'].' access denied', 403);
  }

  if ($dataJson['users_permissions'][$user['_id']] === 'none') {
    unset($user['permissions']['projects'][$projectId]);
  } else {
    $user['permissions']['projects'][$projectId] = $dataJson['users_permissions'][$user['_id']];
    $userButtonsData[] = array(
      '_id' => $user['_id'],
      'name' => $user['forename'].' '.$user['surname'],
      'permissions' => ucfirst($dataJson['users_permissions'][$user['_id']]),
      'link' => isSuperuser() || $user['_id'] === $_SESSION['LVuser']['_id'],
    );
  }
  $resultUserUpdate = $database->users->updateOne(['_id' => new MongoDB\BSON\ObjectID($user['_id'])], ['$set' => ['permissions' => $user['permissions']]]);
  $usersUpdated += $resultUserUpdate->getMatchedCount();
}

apiResponse('Successfully updated permissions for '.$usersUpdated.' users.', ['users' => $userButtonsData]);
