# Log Parser

A Vue 3 application for viewing logs.

## Setup

 1. Install dependencies by running `npm i`
 2. Set up config `vite.config.js`, use provided .dist config as base
 3. Build the Parser using `npm run build`

Once above steps are done the project should be fully functioning.

## Development

Follow the steps 1 and 2 in [Setup](#Setup) then run `npm run dev`. Development server will start based on the `server.port` variable in `client/vite.config.js`.

## Usage

The App expects `logsData` defined on the window that contains array of the logs that will be displayed. The logs can be filtered using the *Filters* tab. You can also change how the logs are displayed using the *Keys* tab and choose which data are shown for the *Group* (individual lines), *Item* (show when Group is clicked) and *Detail* (show when Item is clicked).
