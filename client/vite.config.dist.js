import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import * as path from 'path'
import fs from 'fs'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    { name: 'post-build-commands', closeBundle: () => { postBuildCommands() } }
  ],
  server: {
    strictPort: true,
    port: 8084 // port for the Log Parser app dev server, has to match the port specified in inc/config.php
  },
  build: {
    outDir: path.resolve(__dirname, '../src/js/vuejs/tmp'),
    emptyOutDir: true,
    sourcemap: true,
    assetsDir: '',
    rollupOptions: {
      input: path.resolve(__dirname, 'src/main.js'),
      output: {
        entryFileNames: 'app.js',
        assetFileNames: '[ext]/app.[ext]'
      }
    },
    chunkSizeWarningLimit: 4000
  }
})

function postBuildCommands () {
  if (process.env.NODE_ENV === 'development') {
    return
  }

  const mainDir = '../src/js/vuejs/'
  if (!fs.existsSync(path.resolve(__dirname, mainDir, 'tmp/app.js'))) {
    console.error('\x1b[31mapp.js was not created')
    return
  }
  if (!fs.existsSync(path.resolve(__dirname, mainDir, 'tmp/css/app.css'))) {
    console.error('\x1b[31mapp.css was not created')
    return
  }

  fs.renameSync(path.resolve(__dirname, mainDir, 'tmp/app.js'), path.resolve(__dirname, mainDir, 'app.js'))

  const mapKeepFile = path.resolve(__dirname, mainDir, 'app.js.map.keep')
  const mapFilePath = path.resolve(__dirname, mainDir, 'app.js.map')
  if (fs.existsSync(mapKeepFile)) {
    const stat = fs.statSync(mapFilePath)
    const time = stat.mtime.getTime()
    fs.renameSync(mapFilePath, path.resolve(__dirname, mainDir, 'app.js.map@' + parseInt(time / 1000)))
    fs.rmSync(mapKeepFile)
  }
  fs.renameSync(path.resolve(__dirname, mainDir, 'tmp/app.js.map'), mapFilePath)

  if (!fs.existsSync(path.resolve(__dirname, mainDir, 'css'))) {
    fs.mkdirSync(path.resolve(__dirname, mainDir, 'css'))
  }
  fs.renameSync(path.resolve(__dirname, mainDir, 'tmp/css/app.css'), path.resolve(__dirname, mainDir, 'css/app.css'))

  fs.rmSync(path.resolve(__dirname, mainDir, 'tmp'), { recursive: true, force: true })
}
