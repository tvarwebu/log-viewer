# Log Viewer

A web interface for storing and viewing logs in MongoDB

## Setup

 1. Set up [MongoDB](https://www.mongodb.com/docs/manual/installation/)
 2. Install dependencies
    - Run `composer i` in root of project
    - Run `npm i` in /client
 3. Set up configs `/src/inc/config.php` and `/client/vite.config.js`, use provided .dist configs as base
 4. Create folder `/src/logs` for storing logs
 5. Build the Parser using `npm run build` in /client

Once above steps are done the project should be fully functioning.

## Development

Follow the steps 1 - 3 in [Setup](#setup) then run `npm run dev` in /client. Development server will start based on the `server.port` variable in `client/vite.config.js` this variable should match `$vueDevServerPort` in `/src/inc/config.php`. Add `developer=1` to any URL to enable the developer mode eg. `http://localhost/log-viewer?developer=1`. To turn off developer mode add `developer=0` to any URL.

## Usage

### Initial Setup

To start using **Log Viewer** you first must create `superuser` account, Project and Key for api. Additionally you should consider setting up other Users and Dashboards.

To create the `superuser` account navigate to your **Log Viewer** instance, where you will be prompted to create the account.

Once the account is create you will be redirected to the Project creation page where you should create the Project for which you want to store logs. You can read about how to set up Projects in [Project Edit](#project-edit).

Finally you will have to create Key for api that will insert logs. You can read about how to set up API Keys in [Keys](#keys).

Once all this is set up you can start using the **Log Viewer**.

Additionally you can also add more [Users](#users) or create [Dashboards](#dashboards) for better projects overview.

### <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-medical" viewBox="0 0 16 16"><path d="M7.5 5.5a.5.5 0 0 0-1 0v.634l-.549-.317a.5.5 0 1 0-.5.866L6 7l-.549.317a.5.5 0 1 0 .5.866l.549-.317V8.5a.5.5 0 1 0 1 0v-.634l.549.317a.5.5 0 1 0 .5-.866L8 7l.549-.317a.5.5 0 1 0-.5-.866l-.549.317zm-2 4.5a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1zm0 2a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1z"/><path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2M9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/></svg> Projects

The `/log-viewer/projects` page serves as overview of all of your Logs for individual Projects.

#### Project Edit

To add new Project navigate to the `/log-viewer/projects/new/edit` page by clicking the plus <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16"><path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4"/></svg> button. Project must have **Name** and **Handle**. When creating the **Handle** consider that it is used when working with logs through API and has to be send with the Logs data. Additionally Log Fields can be defined for validation of the logs, note that any additional fields that are not set in the Fields setting will still be inserted into the database.

#### Marking older logs

To mark logs older then specific date to be deleted click the clock <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-clock-history" viewBox="0 0 16 16"><path d="M8.515 1.019A7 7 0 0 0 8 1V0a8 8 0 0 1 .589.022zm2.004.45a7 7 0 0 0-.985-.299l.219-.976q.576.129 1.126.342zm1.37.71a7 7 0 0 0-.439-.27l.493-.87a8 8 0 0 1 .979.654l-.615.789a7 7 0 0 0-.418-.302zm1.834 1.79a7 7 0 0 0-.653-.796l.724-.69q.406.429.747.91zm.744 1.352a7 7 0 0 0-.214-.468l.893-.45a8 8 0 0 1 .45 1.088l-.95.313a7 7 0 0 0-.179-.483m.53 2.507a7 7 0 0 0-.1-1.025l.985-.17q.1.58.116 1.17zm-.131 1.538q.05-.254.081-.51l.993.123a8 8 0 0 1-.23 1.155l-.964-.267q.069-.247.12-.501m-.952 2.379q.276-.436.486-.908l.914.405q-.24.54-.555 1.038zm-.964 1.205q.183-.183.35-.378l.758.653a8 8 0 0 1-.401.432z"/><path d="M8 1a7 7 0 1 0 4.95 11.95l.707.707A8.001 8.001 0 1 1 8 0z"/><path d="M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5"/></svg> button. You will be shown modal where you can set to which date should logs be marked as deleted. This action will mark Logs in all projects that are older then the date set.

#### Deleting logs

You can delete logs that were mark for deletion either per Project or for all Projects. To delete marked logs from all project click the trashcan <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash3" viewBox="0 0 16 16"><path d="M6.5 1h3a.5.5 0 0 1 .5.5v1H6v-1a.5.5 0 0 1 .5-.5M11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3A1.5 1.5 0 0 0 5 1.5v1H1.5a.5.5 0 0 0 0 1h.538l.853 10.66A2 2 0 0 0 4.885 16h6.23a2 2 0 0 0 1.994-1.84l.853-10.66h.538a.5.5 0 0 0 0-1zm1.958 1-.846 10.58a1 1 0 0 1-.997.92h-6.23a1 1 0 0 1-.997-.92L3.042 3.5zm-7.487 1a.5.5 0 0 1 .528.47l.5 8.5a.5.5 0 0 1-.998.06L5 5.03a.5.5 0 0 1 .47-.53Zm5.058 0a.5.5 0 0 1 .47.53l-.5 8.5a.5.5 0 1 1-.998-.06l.5-8.5a.5.5 0 0 1 .528-.47M8 4.5a.5.5 0 0 1 .5.5v8.5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5"/></svg> button in the top right menu. To delete marked logs only for specific Project hover over the Project row and click the same button. Note that these buttons are shown only if there are some marked logs.

Only the `superuser` or user with the `admin` permissions can delete logs. Note that in the case of the `admin` user the top right menu button will delete logs for all projects the user has `admin` role for.

#### Graphs

There are several Graphs that you can set up to have better overview of your Logs.

To create Pie chart to see logs distribution between individual Projects click the graph <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-graph-up-arrow" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M0 0h1v15h15v1H0zm10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4.9l-3.613 4.417a.5.5 0 0 1-.74.037L7.06 6.767l-3.656 5.027a.5.5 0 0 1-.808-.588l4-5.5a.5.5 0 0 1 .758-.06l2.609 2.61L13.445 4H10.5a.5.5 0 0 1-.5-.5"/></svg> button. You will be shown modal where you can select which Project you want to use for the Graph. Once the Graph is create you can save the Graph which will add it as a link into the Graph modal. Saved Graphs can be also set as *Favourite* which will show them by default on the page load.

To show Graph for individual rows click the graphs <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-graph-up-arrow" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M0 0h1v15h15v1H0zm10 3.5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V4.9l-3.613 4.417a.5.5 0 0 1-.74.037L7.06 6.767l-3.656 5.027a.5.5 0 0 1-.808-.588l4-5.5a.5.5 0 0 1 .758-.06l2.609 2.61L13.445 4H10.5a.5.5 0 0 1-.5-.5"/></svg> button show when hovering over the individual table row. Once show this Graph can also be set as favourite.

### <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16"><path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1zm-7.978-1L7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002-.014.002zM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4m3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0M6.936 9.28a6 6 0 0 0-1.23-.247A7 7 0 0 0 5 9c-4 0-5 3-5 4q0 1 1 1h4.216A2.24 2.24 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816M4.92 10A5.5 5.5 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275ZM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0m3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4"/></svg> Users

The `/log-viewer/users` page is used for creating and managing users. Only `superuser` can create new users. New user can be created as `standard` or `superuser`. `superuser` can edit different `standard` users but cannot edit different `superuser`.

When creating new `standard` user you will have to set up permissions for individual Projects and Dashboards. Dashboard is not technically required for user to function but it is recommended.

User can have on of following permissions for Project: 
 - admin: user can view, clear, delete logs and additionally they can edit the project settings and insert logs,
 - maintainer: user can view and clear logs,
 - standard: user can view logs,
 - none: no access.

User cannot be given permission for Dashboard that contains only Projects for which user does not have any permission.

Additionally user can be given permission to see and clear the logs for errors during Log insert.

### <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-key" viewBox="0 0 16 16"><path d="M0 8a4 4 0 0 1 7.465-2H14a.5.5 0 0 1 .354.146l1.5 1.5a.5.5 0 0 1 0 .708l-1.5 1.5a.5.5 0 0 1-.708 0L13 9.207l-.646.647a.5.5 0 0 1-.708 0L11 9.207l-.646.647a.5.5 0 0 1-.708 0L9 9.207l-.646.647A.5.5 0 0 1 8 10h-.535A4 4 0 0 1 0 8m4-3a3 3 0 1 0 2.712 4.285A.5.5 0 0 1 7.163 9h.63l.853-.854a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.646-.647a.5.5 0 0 1 .708 0l.646.647.793-.793-1-1h-6.63a.5.5 0 0 1-.451-.285A3 3 0 0 0 4 5"/><path d="M4 8a1 1 0 1 1-2 0 1 1 0 0 1 2 0"/></svg> Keys

To create API Keys navigate to user you want to use and at the bottom of the page click the plus <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-plus" viewBox="0 0 16 16"><path d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4"/></svg> button next to the API Keys heading (or inside the table if there are no Keys set up). Modal will be shown with input for the name of the API Key. Set the name and click the button create, token will be created for your Key. You can view this token on the User edit page by clicking the eye <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16"><path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8M1.173 8a13 13 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5s3.879 1.168 5.168 2.457A13 13 0 0 1 14.828 8q-.086.13-.195.288c-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5s-3.879-1.168-5.168-2.457A13 13 0 0 1 1.172 8z"/><path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5M4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0"/></svg> button show when hovering over the table row.

Note the user that will be used when inserting logs should have the Admin right for the specific projects.

### <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-speedometer2" viewBox="0 0 16 16"><path d="M8 4a.5.5 0 0 1 .5.5V6a.5.5 0 0 1-1 0V4.5A.5.5 0 0 1 8 4M3.732 5.732a.5.5 0 0 1 .707 0l.915.914a.5.5 0 1 1-.708.708l-.914-.915a.5.5 0 0 1 0-.707M2 10a.5.5 0 0 1 .5-.5h1.586a.5.5 0 0 1 0 1H2.5A.5.5 0 0 1 2 10m9.5 0a.5.5 0 0 1 .5-.5h1.5a.5.5 0 0 1 0 1H12a.5.5 0 0 1-.5-.5m.754-4.246a.39.39 0 0 0-.527-.02L7.547 9.31a.91.91 0 1 0 1.302 1.258l3.434-4.297a.39.39 0 0 0-.029-.518z"/><path fill-rule="evenodd" d="M0 10a8 8 0 1 1 15.547 2.661c-.442 1.253-1.845 1.602-2.932 1.25C11.309 13.488 9.475 13 8 13c-1.474 0-3.31.488-4.615.911-1.087.352-2.49.003-2.932-1.25A8 8 0 0 1 0 10m8-7a7 7 0 0 0-6.603 9.329c.203.575.923.876 1.68.63C4.397 12.533 6.358 12 8 12s3.604.532 4.923.96c.757.245 1.477-.056 1.68-.631A7 7 0 0 0 8 3"/></svg> Dashboards

`/log-viewer/dashboards` group individual Projects. To see your Dashboards click the **Log Viewer** logo in the top left corner of simply navigate to `/log-viewer`. Dashboards can be customized with specific color.

### Inserting Logs

Logs are inserted by making POST request to the `api/logs/insert` endpoint. For log to be inserted the request must contain:
 - authorization token from the Key (to learn more see [Keys](#keys)) send in HTTP headers in the `Authorization: Bearer <token>` format,
 - valid JSON as body,
 - `project_handle` key in the JSON body with handle for one of the defined Projects,
 - JSON data must past validation defined in Project (type and required), additional keys can be supplied and will be inserted.

### Log insert errors

When Log cannot be inserted error will be saved. These errors then can be reviewed using the newly shown button warning <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16"><path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.15.15 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.2.2 0 0 1-.054.06.1.1 0 0 1-.066.017H1.146a.1.1 0 0 1-.066-.017.2.2 0 0 1-.054-.06.18.18 0 0 1 .002-.183L7.884 2.073a.15.15 0 0 1 .054-.057m1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767z"/><path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/></svg> in the top right submenu.

You can control how and when these logs are created using following config variables: 
 - $insertDataLog
   - leave empty to not log data when inserting logs (default),
   - `simple` for single line log with only date, access token, data length,
   - `full` to store full request data,
 - $logToFile - log to `src/logs/errors_logs_insert.log` file instead of collection in the DB (default is `false`).

### Log Parser

Log Parser is Vue 3 app used to display the logs in easily readable and manageable format. The logs can be filtered using the **Filters** tab. You can also change how the logs are displayed using the **Keys** tab and choose which data are shown for the **Group** (individual lines), **Item** (show when Group is clicked) and **Detail** (show when Item is clicked).
